### Saved Raw Data Title format ###
# RunID+TestDate + p/sFEB+ SCA ID + Wedge Serial + Quad info +Layer + Gain + PT + polarity + VMMtoscope mapping
# File Format: run_00002_17052019_pFEB_1045_20MNIWSAP00001_QS1P_L1_3_50_P_0_1_2_3.dat
# File Format: run_00002_17052019_pFEB_1045_00000000000000_QS0T_L0_3_50_N_2_3_4.dat (On test Bench)
# runid+_+nowdate+_+board_type_N+_+scaid+_+monthly+_+Size_ChamberNo+Position+_+Layer+_+vmm_gain_temp_t+_+vmm_pt_temp+_+board_type_N_t+_+vmmmaping+".dat"

from __future__ import absolute_import
from __future__ import unicode_literals

import os
import psutil
import glob
import time

from Tkinter import *
from Cham_DECODER_funct import *
from line_replacer import *
from BoardID_replacer import *
from DECODER_SCAID_funct import *
#from tekscope_utils_4034 import *
from tekscope_utils import *
from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket
from Noise_process_funct import *
from VMMconfig_replacer import *
from datetime import datetime
from roc_analog_reg_cfg import *
from roc_digital_reg_cfg import *
from sendttc import *

#Use for running shell script
import subprocess

xm=XmlModifier()
sp=SendPacket()
spa=SendPacket_Ana()
spd=SendPacket_Dig()
spttc=SendPacket_TTC()

vmm_pt_temp = 50
vmm_gain_temp = 1.0

nowdate = datetime.today().strftime('%d%m%Y')

def show_entry_fields():
   print("First Name: %s\nLast Name: %s" % (e1.get(), e2.get()))

master = Tk()
master.title("sTGC Wedge Noise Auto Measurement")

#=====================================================MINIDAQ & SCOPE IP====================================================================
Label(master, text="MINIDAQ IP: 192.168.0.16",font=20, fg="red").grid(row=0, column=0, rowspan=1, sticky=E)
Label(master, text="OSCILLIOSCOPE IP: 192.168.0.100",font=20, fg="red").grid(row=0, column=3, rowspan=1, sticky=E)

#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=1)

#=====================================================Selecting scope model================================================================ 

v_SCOPE = IntVar()
v_SCOPE.set(1)
languages = [("MSO56"), ("DPO4034"), ("WR8104")]

def ShowChoice_scope():
    global scope_type_N
    if v_SCOPE.get() == 0:
        scope_type_N = "MSO56"
    elif v_SCOPE.get() == 1:
        scope_type_N = "DPO4034"
    elif v_SCOPE.get() == 2:
        scope_type_N = "WR8104"
    print(scope_type_N)

Label(master, text="""Choose the oscillscope you are working with:""", justify = LEFT, padx = 20,font=20).grid(row=1, column=0)
r = 0
for val, language in enumerate(languages):
    Radiobutton(master, text=language, padx = 20, variable=v_SCOPE, command=ShowChoice_scope, value=val,font=20).grid(row=2, column=r)
    r = r + 1

#=====================================================Selecting MINIDAQ boardID============================================================= 
var_BoardID = IntVar()
var_BoardID.set(3)

languages = [("J1"), ("J2"), ("J3"), ("J4"), ("J5"), ("J6"), ("J7"), ("J8")]

def ShowChoice_BoardID():
    print(var_BoardID.get())
    if var_BoardID.get() == 0:
        BoardID_replacer_board_type_0()
    elif var_BoardID.get() == 1:
        BoardID_replacer_board_type_1()
    elif var_BoardID.get() == 2:
        BoardID_replacer_board_type_2()
    elif var_BoardID.get() == 3:
        BoardID_replacer_board_type_3()
    elif var_BoardID.get() == 4:
        BoardID_replacer_board_type_4()
    elif var_BoardID.get() == 5:
        BoardID_replacer_board_type_5()
    elif var_BoardID.get() == 6:
        BoardID_replacer_board_type_6()
    elif var_BoardID.get() == 7:
        BoardID_replacer_board_type_7()

Label(master, text="""Choose which MINISAS is connected to MINIDAQ:""", justify = LEFT, padx = 20,font=20).grid(row=3, column=0)

c_BID = 0
for val, language in enumerate(languages):
    Radiobutton(master, text=language, padx = 20, variable=var_BoardID, command=ShowChoice_BoardID, value=val,font=20).grid(row=4, column=c_BID)
    c_BID = c_BID + 1

#=====================================================Selecting scope model================================================================ 

v_LOC = IntVar()
v_LOC.set(0)
languages = [("CLEANROOM"), ("GASROOM"), ("FENCEAREA"), ("OTHER")]

def ShowChoice_LOC():
    global location_type
    global location_type_T
    if v_LOC.get() == 0:
        location_type = "CLEANROOM"
        location_type_T = "C"
    elif v_LOC.get() == 1:
        location_type = "GASROOM"
        location_type_T = "G"
    elif v_LOC.get() == 2:
        location_type = "FENCEAREA"
        location_type_T = "F"
    elif v_LOC.get() == 3:
        location_type = "OTHER"
        location_type_T = "O"

    print(location_type)

Label(master, text="""Choose the wedge location:""", justify = LEFT, padx = 20,font=20).grid(row=5, column=0)
r = 0
for val, language in enumerate(languages):
    Radiobutton(master, text=language, padx = 20, variable=v_LOC, command=ShowChoice_LOC, value=val,font=20).grid(row=6, column=r)
    r = r + 1

#=====================================================Insert row for title===========================================================
#Message(master, text = "=========================Shifter Input=========================", width=1000, font=24, pady=24, fg="blue").grid(row=7, column=0, columnspan=100)
Message(master, text = " ").grid(row=7)

#======================================================Shifter input ==================================================
'''
def show_shifter(): 
	global shifter1
	global shifter2
	global shifter3
	shifter1 = e_shifter1.get()
	shifter2 = e_shifter2.get()
	shifter3 = e_shifter3.get()
	print("Shifter1: %s\nShifter2: %s\nShifter3: %s" % (shifter1, shifter2, shifter3))
'''

Label(master, text="""Shifter1:""", justify = LEFT, padx = 20, font=20).grid(row=8, column=0)
Label(master, text="""Shifter2:""", justify = LEFT, padx = 20, font=20).grid(row=8, column=1)
Label(master, text="""Shifter3:""", justify = LEFT, padx = 20, font=20).grid(row=8, column=2)

e_shifter1 = Entry(master)
e_shifter1.grid(row=9, column=0)
e_shifter2 = Entry(master)
e_shifter2.grid(row=9, column=1)
e_shifter3 = Entry(master)
e_shifter3.grid(row=9, column=2)


#Button(master, text='Show Shifters Name', command=show_shifter, font=20).grid(row=9, column=3, pady=4)

#=====================================================Insert row for title===========================================================
Message(master, text = "=========================FEB/Wedge Serial & Location Input=========================", width=1000, font=24, pady=24, fg="blue").grid(row=15, column=0, columnspan=100)

#======================================================pFEB/sFEB serial input==================================================

def show_FEB_serial(): 
	global FEB_serial
	FEB_serial = e_FEB_serial.get()
	print("FEB_serial: %s" % (FEB_serial))


Label(master, text="""*FEB_serial*:""", justify = LEFT, padx = 20, font=20).grid(row=16, column=0)
Label(master, text="""FEB MTF:""", justify = LEFT, padx = 20, font=20).grid(row=17, column=0)

e_FEB_serial = Entry(master,highlightbackground="red")
e_FEB_serial.grid(row=16, column=1)
e_FEB_MTF = Entry(master)
e_FEB_MTF.grid(row=17, column=1)

#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=18)

#====================================================Chamber Serial Decoder============================================

fields = ('*Chamber Serial*', 'Large/Small', 'Side A/C', 'Pivot/Confirm', 'Serial Number')

def id_decode(entries):
    global Position
    global monthly
    global S_L_Wedge
    global small_or_large
    global A_C_Wedge
    global P_C_Wedge
    global Wedge_Serial_Number
    global A_or_C
    monthly = (str(entries['*Chamber Serial*'].get()))
    cham_code = cham_decode(monthly)
    S_L_Wedge = cham_code[0]
    A_C_Wedge = cham_code[1]
    P_C_Wedge = cham_code[2]
    Wedge_Serial_Number = cham_code[3]
    entries['Large/Small'].delete(0,END)
    entries['Large/Small'].insert(0, cham_code[0] )
    entries['Side A/C'].delete(0,END)
    entries['Side A/C'].insert(0, cham_code[1] )
    entries['Pivot/Confirm'].delete(0,END)
    entries['Pivot/Confirm'].insert(0, cham_code[2] )
    entries['Serial Number'].delete(0,END)
    entries['Serial Number'].insert(0, cham_code[3] )
    print("Chamber Serial: %s" % str(monthly))

    if cham_code[2] == "Pivot":
        Position = "P"
    elif cham_code[2] == "Confirm":
        Position = "C"
    elif cham_code[2] == "Bench":
        Position = "T"

    if S_L_Wedge == "QS":
        small_or_large = "Small"
    elif S_L_Wedge == "QL":
        small_or_large = "Large"
    else:
        small_or_large = "N/A"

    if A_C_Wedge == "Side A":
        A_or_C = "A"
    elif A_C_Wedge == "Side C":
        A_or_C = "C"
    else:
        A_or_C = "N/A"

def makeform(root, fields):
   entries = {}
   line_count = 0
   for field in fields:
      lab = Label(master, width=22, text=field+": ",font=20)
      ent = Entry(master)
      lab.grid(row=line_count+19, column=0)
      ent.grid(row=line_count+19, column=1)
      entries[field] = ent
      line_count = line_count + 1
   return entries

ents = makeform(master, fields)  
b1 = Button(master, text='cham_decode', command=(lambda e=ents: id_decode(e)),font=20)
b1.grid(row=23, column=2)

#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=25)

#======================================================Quad/Small/Large/Pivot/Confirm/Pad/Strip/Layer input==================================================

def show_Wedge_LOC(): 
	global Wedge_LOC
	global Size_ChamberNo_Position
	global board_type_N
	global board_type_N_t
	global Layer
	global Layer_T
	code_wedge_list = []
	Wedge_LOC = e_Wedge_LOC.get()
	code_wedge_list = Wedge_LOC.split("-")
	if len(code_wedge_list) == 3:
		Size_ChamberNo_Position = code_wedge_list[0]
		if code_wedge_list[1] == "Pad":
			line_replacer_board_type_0()
			board_type_N = "pFEB"
			board_type_N_t = "P"
		elif code_wedge_list[1] == "Strip":
			line_replacer_board_type_1()
			board_type_N = "sFEB"
			board_type_N_t = "P"
		print("Measurement location: %s" % (Wedge_LOC))
		if code_wedge_list[2] == "Layer1":
			Layer = "L1"
			Layer_T = "1"
		elif code_wedge_list[2] == "Layer2":
			Layer = "L2"
			Layer_T = "2"
		elif code_wedge_list[2] == "Layer3":
			Layer = "L3"
			Layer_T = "3"
		elif code_wedge_list[2] == "Layer4":
			Layer = "L4"
			Layer_T = "4"
		else:
			Layer = "L0"
			Layer_T = "0"
	else:
		Size_ChamberNo_Position = "QS0T"
		board_type_N = "pFEB"
		board_type_N_t = "P"
		Layer = "L0"
		Layer_T = "0"

Label(master, text="""*Measurement location*:""", justify = LEFT, padx = 20, font=20).grid(row=26, column=0)

e_Wedge_LOC = Entry(master,highlightbackground="red")
e_Wedge_LOC.grid(row=26, column=1)

Button(master, text='Meas LOC decode', command=show_Wedge_LOC,font=20).grid(row=26, column=2)

#=====================================================Choose pFEB/sFEB===========================================================
'''
var_board = IntVar()
var_board.set(1)
languages = [("pFEB",1), ("sFEB",2)]

def ShowChoice_board():
    global board_type_N
    global board_type_N_t
    print(var_board.get())
    if var_board.get() == 0:
        line_replacer_board_type_0()
        board_type_N = "pFEB"
        board_type_N_t = "P"
    elif var_board.get() == 1:
        line_replacer_board_type_1()
        board_type_N = "sFEB"
        board_type_N_t = "N"

Label(master, text="""Choose which board you are working with:""", justify = LEFT, padx = 20,font=20).grid(row=6, column=0)

c_B = 0
for val, language in enumerate(languages):
    Radiobutton(master, text=language, padx = 20, variable=var_board, command=ShowChoice_board, value=val,font=20).grid(row=7, column=c_B)
    c_B = c_B + 1
'''
#=====================================================Insert empty row===========================================================
Message(master, text = "=============================MINIDAQ INIT CONFIG=============================", width=1000, font=24, pady=24, fg="blue").grid(row=30, column=0, columnspan=100)


#=====================================================INIT SCA && Readback SCAID===========================================================

def paraprocess_SCA():
    subprocess.call("./test_SCA.sh")

def SCAID_insert():
    global scaid
    e3.insert(0, SCAID_DECODER() )
    os.remove('test.dat')
    scaid = e3.get()

Button(master, text='INIT SCA', command=paraprocess_SCA,font=20).grid(row=31, column=0)
Button(master, text='READ SCA ID', command=SCAID_insert,font=20).grid(row=31, column=1)

Label(master, text="SCAID:",font=20).grid(row=31, column=2, rowspan=1, sticky=E)

e3 = Entry(master)
e3.grid(row=31, column=3)

#=====================================================INIT ROC===========================================================

def paraprocess_ROC():
	spa.send('roc','CfgASIC',target)
	time.sleep(2)
	spd.send('roc','CfgASIC',target)
	print "ROC configuation done"

Button(master, text='INIT ROC(Wait for 5 seconds)', command=paraprocess_ROC,font=20).grid(row=32, column=0)

#=====================================================INIT TTC===========================================================
"""
def paraprocess_TTC():
	spttc.send()
	print "TTC configuation done"

Button(master, text='INIT TTC', command=paraprocess_TTC,font=20).grid(row=17, column=2)
"""
#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=33)

#=====================================================CHOOSE Chamber Type========================================================

'''
var_CHM = IntVar()
var_CHM.set(1)
languages = [("Bench"), ("QL1"), ("QL2"), ("QL3"), ("QS1"), ("QS2"), ("QS3")]

def ShowChoice_CHM():
    global Size_ChamberNo
    print(var_CHM.get())
    if var_CHM.get() == 0:
        Size_ChamberNo = "QS0"
    elif var_CHM.get() == 1:
        Size_ChamberNo = "QL1"
    elif var_CHM.get() == 2:
        Size_ChamberNo = "QL2"
    elif var_CHM.get() == 3:
        Size_ChamberNo = "QL3"
    elif var_CHM.get() == 4:
        Size_ChamberNo = "QS1"
    elif var_CHM.get() == 5:
        Size_ChamberNo = "QS2"
    elif var_CHM.get() == 6:
        Size_ChamberNo = "QS3"

Label(master, text="""Choose which environment on test:""", justify = LEFT, padx = 20,font=20).grid(row=12, column=0)
c = 0
for val, language in enumerate(languages):
    Radiobutton(master, text=language, padx = 20, variable=var_CHM, command=ShowChoice_CHM, value=val,font=20).grid(row=13, column=c)
    c = c + 1
'''
#=====================================================CHOOSE Layer========================================================
'''
var_LAY = IntVar()
var_LAY.set(1)
languages = [("Layer",1), ("Layer",2), ("Layer",3), ("Layer",4)]

def ShowChoice_LAY():
    global Layer
    print(var_LAY.get())
    if var_LAY.get() == 0:
        Layer = "L1"
    elif var_LAY.get() == 1:
        Layer = "L2"
    elif var_LAY.get() == 2:
        Layer = "L3"
    elif var_LAY.get() == 3:
        Layer = "L4"

Label(master, text="""Choose which layer on test:""", justify = LEFT, padx = 20,font=20).grid(row=14, column=0)
c = 0
for val, language in enumerate(languages):
    Radiobutton(master, text=language, padx = 20, variable=var_LAY, command=ShowChoice_LAY, value=val,font=20).grid(row=15, column=c)
    c = c + 1
'''

#======================================================GAIN Select==================================================
var_GAIN = IntVar()
var_GAIN.set(1)
languages = [("VMM_GAIN_1mV/fC"), ("VMM_GAIN_3mV/fC")]

def ShowChoice_VMM_GAIN():
	global vmm_gain_temp
	global vmm_gain_temp_t
	if var_GAIN.get() == 0:
		vmm_gain_temp = 1.0
		vmm_gain_temp_t = "1"
	elif var_GAIN.get() == 1:
		vmm_gain_temp = 3.0
		vmm_gain_temp_t = "3"

Label(master, text="""VMM Gain:""", justify = LEFT, padx = 20,font=20).grid(row=34, column=0)

c_VT = 1
for val, language in enumerate(languages):
	Radiobutton(master, text=language, padx = 20, variable=var_GAIN, command=ShowChoice_VMM_GAIN, value=val,font=20).grid(row=34, column=c_VT)
	c_VT = c_VT + 1

#======================================================PT Select==================================================
var_PT = IntVar()
var_PT.set(1)
languages = [("VMM_PT_25ns"), ("VMM_PT_50ns")]

def ShowChoice_VMM_PT():
	global vmm_pt_temp
	global vmm_pt_temp_N
	if var_PT.get() == 0:
		vmm_pt_temp = 25
		vmm_pt_temp_N = "25"
	elif var_PT.get() == 1:
		vmm_pt_temp = 50
		vmm_pt_temp_N = "50"

Label(master, text="""VMM PT:""", justify = LEFT, padx = 20,font=20).grid(row=35, column=0)

c_PT = 1
for val, language in enumerate(languages):
	Radiobutton(master, text=language, padx = 20, variable=var_PT, command=ShowChoice_VMM_PT, value=val,font=21).grid(row=35, column=c_PT)
	c_PT = c_PT + 1

#======================================================polarity Select==================================================
var_POLAR = IntVar()
var_POLAR.set(0)
languages = [("Positive(Pad/Strip)"), ("Negative(Wire)")]

def ShowChoice_VMM_POLAR():
	global vmm_POLAR_temp
	global vmm_POLAR_temp_N
	if var_POLAR.get() == 0:
		vmm_POLAR_temp = 0
		vmm_POLAR_temp_N = "P"
	elif var_POLAR.get() == 1:
		vmm_POLAR_temp = 1
		vmm_POLAR_temp_N = "N"

Label(master, text="""VMM Polarity:""", justify = LEFT, padx = 20,font=20).grid(row=36, column=0)

c_POLAR = 1
for val, language in enumerate(languages):
	Radiobutton(master, text=language, padx = 20, variable=var_POLAR, command=ShowChoice_VMM_POLAR, value=val,font=22).grid(row=36, column=c_POLAR)
	c_POLAR = c_POLAR + 1
	

#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=37)

#======================================================Cpi and Rp input ==================================================
'''
def show_Cpi_Rp(): 
	global Cpi_Pad
	global Rp_Strip
	Cpi_Pad = e_Cpi.get()
	Rp_Strip = e_Rp.get()
	print("Cpi in pF(for pFEB): %s\nRp in kOhm(for sFEB): %s" % (Cpi_Pad, Rp_Strip))
'''

Label(master, text="""Cpi in pF(for pFEB):""", justify = LEFT, padx = 20, font=20).grid(row=38, column=0)
Label(master, text="""Rp in kOhm(for sFEB):""", justify = LEFT, padx = 20, font=20).grid(row=38, column=1)

e_Cpi = Entry(master)
e_Cpi.grid(row=39, column=0)
e_Rp = Entry(master)
e_Rp.grid(row=39, column=1)

#Button(master, text='Show Cpi/Rp', command=show_Cpi_Rp, font=20).grid(row=39, column=2, pady=4)


#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=40)

#=====================================================Mapping input ==================================================

Label(master, text="""Choose which VMM is connected to scope:\n('N' means not available)\n('-1' means not connected to FEB)""", justify = LEFT, padx = 20, font=20).grid(row=41, column=0)

class Dropdown_N(Frame):
	def __init__(self, parent=None, picks=[], side=LEFT, anchor=W):
		Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = StringVar(master)
			tkvar_map.set('N')
			popupMenu = OptionMenu(self, tkvar_map, 'N','-1','0','1','2','3','4','5','6','7')
			popupMenu.pack(side=side, anchor=anchor, expand=YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)

map1 = Dropdown_N(master, ['CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8'])
map1.grid(row=41, column=1)

def allstates_map(): 
	global total_scanned_channel
	global scope_channel_string
	global scope_channel_string_no
	global scope_channel_string_N

	list_vmmmap = list(map1.state())
	#list_vmmmap = [i for i in list_vmmmap if not 'N' in i]
	print(list_vmmmap)

	list_scope_channel_no = []
 	list_scope_channel = []
	for x in range(0,8):
		print x
		if list_vmmmap[x] != 'N':
			list_scope_channel.append(int(list_vmmmap[x]))
			list_scope_channel_no.append(x+1)


	total_scanned_channel = len(list_scope_channel_no)
	print total_scanned_channel
	print list_scope_channel_no
	print list_scope_channel

	if total_scanned_channel == 1:
		scope_channel_string = "{}".format(list_scope_channel[0])
		scope_channel_string_no = "{}".format(list_scope_channel_no[0])
	elif total_scanned_channel == 2:
		scope_channel_string = "{},{}".format(list_scope_channel[0], list_scope_channel[1])
		scope_channel_string_no = "{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1])
	elif total_scanned_channel == 3:
		scope_channel_string = "{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2])
		scope_channel_string_no = "{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2])
	elif total_scanned_channel == 4:
		scope_channel_string = "{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3])
 		scope_channel_string_no = "{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3])
	elif total_scanned_channel == 5:
		scope_channel_string = "{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4])
		scope_channel_string_no = "{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4])
	elif total_scanned_channel == 6:
		scope_channel_string = "{},{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4], list_scope_channel[5])
		scope_channel_string_no = "{},{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4], list_scope_channel_no[5])
	elif total_scanned_channel == 7:
		scope_channel_string = "{},{},{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4], list_scope_channel[5], list_scope_channel[6])
		scope_channel_string_no = "{},{},{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4], list_scope_channel_no[5], list_scope_channel_no[6])
	elif total_scanned_channel == 8:
		scope_channel_string = "{},{},{},{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4], list_scope_channel[5], list_scope_channel[6], list_scope_channel[7])
		scope_channel_string_no = "{},{},{},{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4], list_scope_channel_no[5], list_scope_channel_no[6], list_scope_channel_no[7])

	print scope_channel_string
	print scope_channel_string_no

	list_scope_channel_N = []
	for x in range(0,len(list_scope_channel)):
		if (list_scope_channel[x] != -1):
			list_scope_channel_N.append(list_scope_channel[x])

	print list_scope_channel_N

	total_scanned_channel_N = len(list_scope_channel_N)

	if total_scanned_channel_N == 1:
		scope_channel_string_N = "{}".format(list_scope_channel_N[0])
	elif total_scanned_channel_N == 2:
		scope_channel_string_N = "{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1])
	elif total_scanned_channel_N == 3:
		scope_channel_string_N = "{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2])
	elif total_scanned_channel_N == 4:
		scope_channel_string_N = "{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3])
	elif total_scanned_channel_N == 5:
		scope_channel_string_N = "{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4])
	elif total_scanned_channel_N == 6:
		scope_channel_string_N = "{}_{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4], list_scope_channel_N[5])
	elif total_scanned_channel_N == 7:
		scope_channel_string_N = "{}_{}_{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4], list_scope_channel_N[5], list_scope_channel_N[6])
	elif total_scanned_channel_N == 8:
		scope_channel_string_N = "{}_{}_{}_{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4], list_scope_channel_N[5], list_scope_channel_N[6], list_scope_channel_N[7])

	print scope_channel_string_N

Button(master, text='Gen VMMID', command=allstates_map, font=20).grid(row=41, column=2, pady=4)


#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=42)

#======================================================Comment(s) input ==================================================
'''
def show_shifter(): 
	global shifter1
	global shifter2
	global shifter3
	shifter1 = e_shifter1.get()
	shifter2 = e_shifter2.get()
	shifter3 = e_shifter3.get()
	print("Shifter1: %s\nShifter2: %s\nShifter3: %s" % (shifter1, shifter2, shifter3))
'''

Label(master, text="""Comment:""", justify = LEFT, padx = 20, font=20).grid(row=43, column=0)

e_comment = Entry(master)
e_comment.grid(row=43, column=1, ipadx=500, columnspan=100, sticky=W)

#=====================================================Insert empty row===========================================================
Message(master, text = " ").grid(row=44)

#======================================================file name monitoring==================================================
def show_file_name():
	global vmm_pt_temp_N
	global vmm_gain_temp_t
	global runid
	global board_type_N
	global scaid
	global monthly
	#global Size_ChamberNo
	#global Position
	global Size_ChamberNo_Position
	global Layer
	global vmm_POLAR_temp_N
	global scope_channel_string_N
	global file_name
	global FEB_MTF
	global FEB_serial
	global location_type_T
	#print Size_ChamberNo

	FEB_serial = e_FEB_serial.get()
	FEB_MTF = e_FEB_MTF.get()
	print("FEB_serial: %s" % (FEB_serial))

	#if (runid != "") & (nowdate != "") & (board_type_N != "") & (scaid != "") & (monthly != "") & (Size_ChamberNo_Position != "") & (Layer != "")& (vmm_gain_temp_t != "") & (vmm_pt_temp_N != "") & (vmm_POLAR_temp_N != "") & (scope_channel_string_N != "") & (location_type_T != ""):
	file_name="run_"+runid+"_"+nowdate+"_"+board_type_N+"_"+scaid+"_"+monthly+"_"+Size_ChamberNo_Position+"_"+Layer+"_"+vmm_gain_temp_t+"_"+vmm_pt_temp_N+"_"+vmm_POLAR_temp_N+"_VMM_"+scope_channel_string_N+"_"+location_type_T+".dat"


	print file_name

Button(master, text='Show file name', command=show_file_name, font=20).grid(row=45, column=1, pady=4) 

#======================================================Run ID scan and generation ==================================================

def runID_gen():
	global runid
	datfiles = []
	runid_temp = []
	for file in glob("*.dat"):
		datfiles.append(file)
	print datfiles
	if len(datfiles) == 0:
		f=open('run_00000_99999999_tFEB_00000_00000000000000_L0_0_00_T_VMM_0.dat','a')
		f.write("**This is a dummy file for further file generation**\n")
		f.close()
		runid_temp = [0]
	else:
		for x in range(0,len(datfiles)):
			data_temp = datfiles[x].split("_")
			print data_temp[1]
			runid_temp.append(int(data_temp[1]))
	print runid_temp
	print max(runid_temp)
	runid = str(int(max(runid_temp)) + 1)
	print runid

Button(master, text='Generate RUNID', command=runID_gen, font=20).grid(row=45, column=0, pady=4)

#======================================================Noise Measurment Program==================================================

def main_noise_measurement_4():
	global vmm_pt_temp
	global vmm_gain_temp
	global runid
	global board_type_N
	global scaid
	global monthly
	#global Size_ChamberNo
	global Position
	global Layer
	global vmm_POLAR_temp
	global scope_channel_string_N
	global file_name
	global S_L_Wedge
	global A_C_Wedge
	global P_C_Wedge
	global Wedge_Serial_Number
	global scope_type_N
	global FEB_serial
	global vmm_POLAR_temp_N
	global FEB_MTF
	global vmm_gain_temp_t
	global vmm_pt_temp_N
	print vmm_pt_temp
	print vmm_gain_temp
	global Size_ChamberNo_Position
	global total_scanned_channel
	global scope_channel_string
	global scope_channel_string_no
	global location_type
	global small_or_large
	global A_or_C
	global Layer_T
	print Layer

	Size_ChamberNo_Position_list = list(Size_ChamberNo_Position)
	QType = Size_ChamberNo_Position_list[0] + Size_ChamberNo_Position_list[1]
	QIndex = Size_ChamberNo_Position_list[2]

	comment_tab = e_comment.get()
	shifter1 = e_shifter1.get()
	shifter2 = e_shifter2.get()
	shifter3 = e_shifter3.get()
	Cpi_Pad = e_Cpi.get()
	Rp_Strip = e_Rp.get()

	start=time.time()

	#== setup scope
	scope_N=scope('192.168.0.100')
	scope_chan=1
	setup_scope(scope_N,scope_channel=1)
	setup_scope(scope_N,scope_channel=2)
	setup_scope(scope_N,scope_channel=3)
	setup_scope(scope_N,scope_channel=4)
	DATAFOLDERDIR='./data/'
	f=open(file_name,'a')

	f.write("************************\n")
	f.write("**Measurement Unix Time:"+str(int(time.time()))+"**\n")
	f.write("**Measurement Date:"+nowdate+"**\n")

	if (shifter1 != "") & (shifter2 == "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+""+"**\n")
		f.write("**Measurement Shifter3:"+""+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+""+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 != ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+shifter3+"**\n")
	else:
		print("No of channels")
		f.write("**Measurement Shifter1:"+""+"**\n")
		f.write("**Measurement Shifter2:"+""+"**\n")
		f.write("**Measurement Shifter3:"+""+"**\n")

	if location_type != "":
		f.write("**Measurement Location:"+location_type+"**\n")
	else:
		showerror("Error", "No measurment location inserted")

	f.write("**FEB Serial Number:"+FEB_serial+"**\n")
	f.write("**FEB MTF:"+FEB_MTF+"**\n")	
	f.write("**FEB Type:"+board_type_N+"**\n")

	if (Rp_Strip == "") & (Cpi_Pad != ""):
		f.write("**Cpi if pFEB(pF):"+Cpi_Pad+"**\n")
	elif (Rp_Strip != "") & (Cpi_Pad == ""):
		f.write("**Rpull-up if sFEB(kOhm):"+Rp_Strip+"**\n")
	else:
		print("Neither Cpi if pFEB(pF) nor Rpull-up if sFEB(kOhm) input provide. please check")

	f.write("**VMM Gain(mV/fC):"+vmm_gain_temp_t+"**\n")
	f.write("**VMM Peaking Time (ns):"+vmm_pt_temp_N+"**\n")
	f.write("**VMM SFM:"+"1"+"**\n")

	f.write("**Wedge MTF:"+monthly+"**\n")
	f.write("**Wedge Size:"+small_or_large+"**\n")
	f.write("**Wedge for ATLAS Side A or C:"+A_or_C+"**\n")
	f.write("**Wedge Type:"+P_C_Wedge+"**\n")
	f.write("**Wedge Serial Number:"+Wedge_Serial_Number+"**\n")
	f.write("**Quadruplet Type:"+QType+"**\n")
	f.write("**Quadruplet Index:"+QIndex+"**\n")
	f.write("**Quadruplet Layer:"+Layer_T+"**\n")

	f.write("**GFZ Adapter Board Type:"+"**\n")
	f.write("**Scope Type:"+ scope_type_N+"**\n")
	f.write("**Scope Channels Connected:"+str(total_scanned_channel)+"**\n")
	f.write("**Scope Channel Recorded:"+scope_channel_string_no+"**\n")
	f.write("**FEB VMM Number Mapped:"+scope_channel_string+"**\n")
	f.write("************************\n\n\n\n")
	
	if comment_tab == "":
		f.write("############COMMENT############\n")
		f.write("###\n")
		f.write("##############################\n\n\n\n")
	elif comment_tab != "":
		f.write("############COMMENT############\n")
		f.write("###"+comment_tab+"\n")
		f.write("##############################\n\n\n\n")
	else:
		print("No comment inputed, please check")				

	f.close()

	noise_level_scan_4(scope_N,file_name,vmm_pt=vmm_pt_temp,vmm_gain=vmm_gain_temp,vmm_polarity=vmm_POLAR_temp)

	stop=time.time()
	total_run_time=stop-start
	print "Noise Measurement Run finished in "+str(total_run_time)+" seconds"

def main_noise_measurement_6():
	global vmm_pt_temp
	global vmm_gain_temp
	global runid
	global board_type_N
	global scaid
	global monthly
	#global Size_ChamberNo
	global Position
	global Layer
	global vmm_POLAR_temp
	global scope_channel_string_N
	global file_name
	global S_L_Wedge
	global A_C_Wedge
	global P_C_Wedge
	global Wedge_Serial_Number
	global scope_type_N
	global FEB_serial
	global vmm_POLAR_temp_N
	global FEB_MTF
	global vmm_gain_temp_t
	global vmm_pt_temp_N
	print vmm_pt_temp
	print vmm_gain_temp
	global Size_ChamberNo_Position
	global Cpi_Pad
	global Rp_Strip
	global shifter1
	global shifter2
	global shifter3
	global total_scanned_channel
	global scope_channel_string
	global scope_channel_string_no
	global location_type
	global small_or_large
	global Layer_T
	print Layer
	start=time.time()

	#== setup scope
	scope_N=scope('192.168.0.100')
	scope_chan=1
	setup_scope(scope_N,scope_channel=1)
	setup_scope(scope_N,scope_channel=2)
	setup_scope(scope_N,scope_channel=3)
	setup_scope(scope_N,scope_channel=4)
	setup_scope(scope_N,scope_channel=5)
	setup_scope(scope_N,scope_channel=6)
	DATAFOLDERDIR='./data/'
	f=open(file_name,'a')


	f.write("************************\n")
	f.write("**Measurement Date:"+nowdate+"**\n")

	if (shifter1 != "") & (shifter2 == "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+" "+"**\n")
		f.write("**Measurement Shifter3:"+" "+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+" "+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 != ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+shifter3+"**\n")
	else:
		print("No of channels")
		f.write("**Measurement Shifter1:"+" "+"**\n")
		f.write("**Measurement Shifter2:"+" "+"**\n")
		f.write("**Measurement Shifter3:"+" "+"**\n")

	f.write("**Measurement Location:"+location_type+"**\n")
	f.write("**FEB Serial Number:"+FEB_serial+"**\n")
	f.write("**FEB MTF:"+FEB_MTF+"**\n")	
	f.write("**FEB Type:"+board_type_N+"**\n")

	if (Rp_Strip == "") & (Cpi_Pad != ""):
		f.write("**Cpi if pFEB(pF):"+Cpi_Pad+"**\n")
	elif (Rp_Strip != "") & (Cpi_Pad == ""):
		f.write("**Rpull-up if sFEB(kOhm):"+Rp_Strip+"**\n")
	else:
		print("Neither Cpi if pFEB(pF) nor Rpull-up if sFEB(kOhm) inputed, please check")

	f.write("**VMM Gain(mV/fC):"+vmm_gain_temp_t+"**\n")
	f.write("**VMM Peaking Time(ns):"+vmm_pt_temp_N+"**\n")
	f.write("**VMM SFM:"+"1"+"**\n")

	f.write("**Wedge MTF:"+monthly+"**\n")
	f.write("**Wedge Size:"+small_or_large+"**\n")
	f.write("**Wedge for Side ATLAS A or C:"+A_C_Wedge+"**\n")
	f.write("**Wedge Type:"+P_C_Wedge+"**\n")
	f.write("**Wedge Serial Number:"+Wedge_Serial_Number+"**\n")
	f.write("**Quadruplet Type:"+Size_ChamberNo_Position+"**\n")
	f.write("**Quadruplet Layer:"+Layer_T+"**\n")

	f.write("**GFZ Adapter Board Type:"+' '+"**\n")
	f.write("**Scope Type:"+ scope_type_N+" **\n")
	f.write("**Number of Scope Channel(s) Connected:"+str(total_scanned_channel)+"**\n")
	f.write("**Scope Channel(s) Recorded:"+scope_channel_string_no+"**\n")
	f.write("**FEB VMM Number Mapped:"+scope_channel_string+"**\n")
	f.write("************************\n\n\n\n")

	f.write("############COMMENT############\n")
	f.write("###\n")
	f.write("##############################\n\n\n\n")

	f.close()

	noise_level_scan_6(scope_N,file_name,vmm_pt=vmm_pt_temp,vmm_gain=vmm_gain_temp,vmm_polarity=vmm_POLAR_temp)

	stop=time.time()
	total_run_time=stop-start
	print "Noise Measurement Run finished in "+str(total_run_time)+" seconds"

#======================================================Base Button Creation==================================================

Button(master, text='Scan Noise(4 Channels)', command=main_noise_measurement_4, font=20).grid(row=45, column=2)
Button(master, text='Scan Noise(6 Channels)', command=main_noise_measurement_6, font=20).grid(row=45, column=3)

#======================================================Data Plotting==================================================
Button(master, text='Quit', command=master.quit, font=20).grid(row=45, column=4)

mainloop( )
