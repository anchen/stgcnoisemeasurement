#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import unicode_literals
import os

from xmlmodifier_N import *


# Format [pFEB/sFEB]_VMM[x]_Q[S/L][1/2/3][P/C][1/2/3/4].xml
# sp = 0 negative, sp = 1 positive
polarity = 1
#Masked Channel_1
starting_CH_1 = 0
finishing_CH_1 = 63
#Unmasked Channel
starting_CH_2 = 0
finishing_CH_2 = 63
#Masked Channel_2
starting_CH_3 = 42
finishing_CH_3 = 63

xm=XmlModifier()
"""
#Masked Channel_1
for i in range (starting_CH_1,finishing_CH_1+1):
	modlist=[['global1','sp',polarity]]
	modlist.append(['ch'+str(i),'sc',0])
	modlist.append(['ch'+str(i),'sl',1])
	modlist.append(['ch'+str(i),'st',0])
	modlist.append(['ch'+str(i),'sth',0])
	modlist.append(['ch'+str(i),'sm',0])
	modlist.append(['ch'+str(i),'smx',1])
	modlist.append(['ch'+str(i),'sd',0])
	xm.vmm_mod('vmm.xml',modlist)
"""
#Unmasked Channel
for i in range (starting_CH_2,finishing_CH_2+1):
	modlist=[['global1','sp',polarity]]
	modlist.append(['ch'+str(i),'sc',0])
	modlist.append(['ch'+str(i),'sl',0])
	modlist.append(['ch'+str(i),'st',1])
	modlist.append(['ch'+str(i),'sth',0])
	modlist.append(['ch'+str(i),'sm',0])
	modlist.append(['ch'+str(i),'smx',0])
	modlist.append(['ch'+str(i),'sd',0])
	xm.vmm_mod('vmm.xml',modlist)
"""
#Masked Channel_2
for i in range (starting_CH_3,finishing_CH_3+1):
	modlist=[['global1','sp',polarity]]
	modlist.append(['ch'+str(i),'sc',0])
	modlist.append(['ch'+str(i),'sl',1])
	modlist.append(['ch'+str(i),'st',0])
	modlist.append(['ch'+str(i),'sth',0])
	modlist.append(['ch'+str(i),'sm',0])
	modlist.append(['ch'+str(i),'smx',1])
	modlist.append(['ch'+str(i),'sd',0])
	xm.vmm_mod('vmm.xml',modlist)
"""


