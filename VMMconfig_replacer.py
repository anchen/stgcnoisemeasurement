# This file is used to modify vmm.xml pointing file for MINISAS connection between MINIDAQ & FEB in "target_info.py" by tk GUI

import re

def VMMConfig_replacer(vmm_config_file):
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        #text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 0', text)
        text = re.sub('\t\t\tvmm_configfile="vmm.xml"', '\t\t\tvmm_configfile='+vmm_config_file, text)
	#text = re.sub("vmm.xml", "vmmb.xml", text)'
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_255_1():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 255', '\t\t\tself.asic_id = 1', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_1_2():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 1', '\t\t\tself.asic_id = 2', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_2_4():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 2', '\t\t\tself.asic_id = 4', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_4_8():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 4', '\t\t\tself.asic_id = 8', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_8_16():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 8', '\t\t\tself.asic_id = 16', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_16_32():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 16', '\t\t\tself.asic_id = 32', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_32_64():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 32', '\t\t\tself.asic_id = 64', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_64_128():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 64', '\t\t\tself.asic_id = 128', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_128_255():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 128', '\t\t\tself.asic_id = 255', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def asic_id_replacer_4_255():
    with open("target_info.py", 'r+') as f:		
        text = f.read()
        text = re.sub('\t\t\tself.asic_id = 4', '\t\t\tself.asic_id = 255', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()
