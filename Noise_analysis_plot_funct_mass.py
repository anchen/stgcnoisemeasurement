import time
import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

def Noise_plot_sFEB_4(file1, file2, reverse_reg, VMM_MASK_MAP, file_name_result, Wedge_Size, Wedge_Side, Wedge_Quad, Layer):

#	ARRAY_Data_1 = np.loadtxt(file2,dtype='double', comments="*")
#	Total_No_1 = len(ARRAY_Data_1)
#	print Total_No_1
#	ARRAY_Data_2 = np.loadtxt(file1,dtype='double', comments="*")
#	Total_No_2 = len(ARRAY_Data_2)
#	print Total_No_2

	ARRAY_Data_1 = []
	ARRAY_Data_1_N = []	
	with open(file2) as fd:
		for line in fd:
			if line.startswith('*') or line.startswith('#'):
				continue
			ARRAY_Data_1.append(map(str, line.strip().split('	')))
	#print ARRAY_Data_1

	for z in range(0,len(ARRAY_Data_1)):
		if ARRAY_Data_1[z] != ['']:
			ARRAY_Data_1_N.append(ARRAY_Data_1[z])

	#print ARRAY_Data_1_N
	Total_No_1 = len(ARRAY_Data_1_N)
	#print Total_No_1

	ARRAY_Data_2 = []
	ARRAY_Data_2_N = []
	with open(file1) as fd:
		for line in fd:
			if line.startswith('*') or line.startswith('#'):
				continue
			ARRAY_Data_2.append(map(str, line.strip().split('	')))
	#print ARRAY_Data_2

	for z in range(0,len(ARRAY_Data_2)):
		if ARRAY_Data_2[z] != ['']:
			ARRAY_Data_2_N.append(ARRAY_Data_2[z])

	#print ARRAY_Data_2_N
	Total_No_1 = len(ARRAY_Data_2_N)
	#print Total_No_1

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	NOISE_DATA_0 = []
	NOISE_DATA_1 = []
	NOISE_DATA_2 = []
	NOISE_DATA_3 = []
	NOISE_DATA_4 = []
	NOISE_DATA_5 = []
	NOISE_DATA_6 = []
	NOISE_DATA_7 = []
	NOISE_PAD = []
	NOISE_STRIP = []
	NOISE_WIRE = []
	counter = []
	
	#sweep though all the list, group channel's data
	
	for y in range(0,64):
		NOISE_DATA_0.append(float(ARRAY_Data_1_N[y][1]))
		y += 1

	for y in range(0,64):
		NOISE_DATA_1.append(float(ARRAY_Data_1_N[y][2]))
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_2.append(float(ARRAY_Data_1_N[y][3]))
		y += 1

	for y in range(0,64):
		NOISE_DATA_3.append(float(ARRAY_Data_1_N[y][4]))
		y += 1

	for y in range(0,64):
		NOISE_DATA_4.append(float(ARRAY_Data_2_N[y][1]))
		y += 1

	for y in range(0,64):
		NOISE_DATA_5.append(float(ARRAY_Data_2_N[y][2]))
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_6.append(float(ARRAY_Data_2_N[y][3]))
		y += 1

	for y in range(0,64):
		NOISE_DATA_7.append(float(ARRAY_Data_2_N[y][4]))
		y += 1


	for x in range(0,len(VMM_MASK_MAP)):
		if VMM_MASK_MAP[x][0] == 0:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_0[y_temp])
			counter.append(y_temp)
		elif VMM_MASK_MAP[x][0] == 1:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_1[y_temp])
			counter.append(y_temp+64)
		elif VMM_MASK_MAP[x][0] == 2:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_2[y_temp])
			counter.append(y_temp+128)
		elif VMM_MASK_MAP[x][0] == 3:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_3[y_temp])
			counter.append(y_temp+192)
		elif VMM_MASK_MAP[x][0] == 4:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_4[y_temp])
			counter.append(y_temp+256)
		elif VMM_MASK_MAP[x][0] == 5:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_5[y_temp])
			counter.append(y_temp+320)
		elif VMM_MASK_MAP[x][0] == 6:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_6[y_temp])
			counter.append(y_temp+384)
		elif VMM_MASK_MAP[x][0] == 7:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_7[y_temp])
			counter.append(y_temp+448)

	#print NOISE_DATA_0
	#print NOISE_DATA_1
	#print NOISE_DATA_2
	#print NOISE_DATA_3
	#print NOISE_DATA_4
	#print NOISE_DATA_5
	#print NOISE_DATA_6
	#print NOISE_DATA_7

	#Read all channel under spacer
	ARRAY_SPACER = []
	ARRAY_SPACER_PLOT_x = []
	ARRAY_SPACER_PLOT_y = []

	ARRAY_SPACER = np.loadtxt('/home/nsw/stgcnoisemeasurement/strip_under_wire_support.txt',dtype='string', comments="=")
	print ARRAY_SPACER

	if Wedge_Side == 'Confirm':
		if Layer == '1':
			Layer_TEMP = '4'
		elif Layer == '2':
			Layer_TEMP = '3'
		elif Layer == '3':
			Layer_TEMP = '2'
		elif Layer == '4':
			Layer_TEMP = '1'
	elif Wedge_Side == 'Pivot':
		Layer_TEMP = Layer
		

	for y in range(0,len(ARRAY_SPACER)):
		if Wedge_Size == ARRAY_SPACER[y][0]:
			if Wedge_Side == ARRAY_SPACER[y][1]:
				if Wedge_Quad == ARRAY_SPACER[y][2]:
					if Layer_TEMP == ARRAY_SPACER[y][3]:
						ARRAY_SPACER_PLOT_x.append((int(ARRAY_SPACER[y][4])*64)+int(ARRAY_SPACER[y][5]))

	print ARRAY_SPACER_PLOT_x

	#For sFEB strip, add VMM0 to VMM7 into series(TBD: Need to know if it is going reversingly)
	#Plot data on graphs

	if reverse_reg == 1:
		NOISE_STRIP.reverse()
		print('list reversed')
	else:
		print('no reverse')

	lower_limit = 0.9
	higher_limit = 2.1
	lower_limit_arr_x = []
	higher_limit_arr_x = []
	lower_limit_arr_y = []
	higher_limit_arr_y = []


	for y in range(0,len(NOISE_STRIP)):
		if NOISE_STRIP[y] > higher_limit:
			higher_limit_arr_x.append(counter[y])
			higher_limit_arr_y.append(NOISE_STRIP[y])
		elif NOISE_STRIP[y] < lower_limit:
			lower_limit_arr_x.append(counter[y])
			lower_limit_arr_y.append(NOISE_STRIP[y])
		y += 1

	print lower_limit_arr_x
	print len(lower_limit_arr_x)
	print higher_limit_arr_x

	lower_limit_arr_x_T = []
	higher_limit_arr_x_T = []
	
	if len(lower_limit_arr_x) != 0:
		for y in range(0,len(lower_limit_arr_x)):
			num_temp = lower_limit_arr_x[y]
			lower_limit_arr_x_Q = num_temp // 64
			lower_limit_arr_x_R = num_temp % 64
			lower_limit_arr_x_T.append([lower_limit_arr_x_Q,lower_limit_arr_x_R])

	if len(higher_limit_arr_x) != 0:
		for y in range(0,len(higher_limit_arr_x)):
			higher_limit_arr_x_Q = int(higher_limit_arr_x[y]) // 64
			higher_limit_arr_x_R = int(higher_limit_arr_x[y]) % 64
			higher_limit_arr_x_T.append([higher_limit_arr_x_Q,higher_limit_arr_x_R])

	print lower_limit_arr_x_T 
	print higher_limit_arr_x_T
	
	data_temp = file1.split("_")
	data_temp_2 = file2.split("_")
	noise_title = data_temp[3]+"_"+data_temp[1]+"_"+data_temp_2[1]+"_"+data_temp[4]+"_"+data_temp[5]+"_"+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]
	noise_title_name = noise_title+".png"
	'''
	f=open(file_name_result,'a')
	if len(higher_limit_arr_x_T) != 0:
		f.write("===High Noise Channel in "+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+" with upper bound "+str(higher_limit)+" mV ===\n")
		for x in range(0, len(higher_limit_arr_x_T)):
			f.write(str(higher_limit_arr_x_T[x][0])+"\t"+str(higher_limit_arr_x_T[x][1])+"\n")
	if len(lower_limit_arr_x_T) != 0:
		f.write("===Low Noise Channel in "+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+" with lower bound "+str(lower_limit)+" mV ===\n")
		for x in range(0, len(lower_limit_arr_x_T)):
			f.write(str(lower_limit_arr_x_T[x][0])+"\t"+str(lower_limit_arr_x_T[x][1])+"\n")
	f.close()
	'''
	plt.plot(counter, NOISE_STRIP,'bx-',markersize = 4)
	plt.plot(lower_limit_arr_x, lower_limit_arr_y,'ro',markersize = 8)
	plt.plot(higher_limit_arr_x, higher_limit_arr_y,'ro',markersize = 8)
	for xy in zip(higher_limit_arr_x, higher_limit_arr_y):
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=5)
	for xy in zip(lower_limit_arr_x, lower_limit_arr_y):
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=5)
	#plt.plot(counter, NOISE_DATA_2,'bx',markersize = 5)
	#plt.plot(counter, NOISE_DATA_3,'gx',markersize = 5)
	#plt.plot(counter, NOISE_DATA_4,'mx',markersize = 5)
	#plt.legend('VMMA',loc='upper right')
	#plt.title('Noise Plot')
	plt.title(noise_title)
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.legend(loc='upper right')
	plt.ylim(0,3)
	plt.xlim(0,520)
	plt.axhline(lower_limit, color='gray', linestyle='--',linewidth=0.5)
	plt.axhline(higher_limit, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(63.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(127.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(191.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(255.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(319.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(383.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(447.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(512, color='gray', linestyle='--',linewidth=0.5)

	for z in range(0,len(ARRAY_SPACER_PLOT_x)):
		ARRAY_SPACER_x_TEMP = ARRAY_SPACER_PLOT_x[z]
		plt.axvline(ARRAY_SPACER_x_TEMP, color='purple', linestyle='--',linewidth=0.5)

	plt.savefig(noise_title_name, dpi=600)
	time.sleep(1)
	plt.clf()


def Noise_plot_pFEB(file1, reverse_reg, VMM_MASK_MAP, file_name_result):
	#ARRAY_Data_1 = np.loadtxt(file1,dtype='double', comments=['#','*'])
	ARRAY_Data_1 = []
	ARRAY_Data_1_N = []	
	with open(file1) as fd:
		for line in fd:
			if line.startswith('*') or line.startswith('#'):
				continue
			ARRAY_Data_1.append(map(str, line.strip().split('	')))
	print ARRAY_Data_1

	for z in range(0,len(ARRAY_Data_1)):
		if ARRAY_Data_1[z] != ['']:
			ARRAY_Data_1_N.append(ARRAY_Data_1[z])

	print ARRAY_Data_1_N
	Total_No_1 = len(ARRAY_Data_1_N)
	print Total_No_1

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	NOISE_DATA_1 = []
	NOISE_DATA_2 = []
	NOISE_PAD_1 = []
	NOISE_PAD_2 = []
	counter_1 = []
	counter_2 = []
	
	#sweep though all the list, group channel's data


	for y in range(0,64):
		NOISE_DATA_1.append(float(ARRAY_Data_1_N[y][2]))
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_2.append(float(ARRAY_Data_1_N[y][3]))
		y += 1


	print NOISE_DATA_1
	print NOISE_DATA_2

	for x in range(0,len(VMM_MASK_MAP)):
		if VMM_MASK_MAP[x][0] == 1:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_PAD_1.append(NOISE_DATA_1[y_temp])
			counter_1.append(y_temp)
		elif VMM_MASK_MAP[x][0] == 2:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_PAD_2.append(NOISE_DATA_2[y_temp])
			counter_2.append(y_temp+64)			
	print NOISE_PAD_1
	print NOISE_PAD_2

	#For pFEB PAD, add VMM1 to VMM2 into series(TBD: Need to know if it is going reversingly)
	#Plot data on graphs

	if reverse_reg == 1:
		NOISE_PAD_1.reverse()
		print('list reversed')
	else:
		print('no reverse')

	lower_limit = 1.3
	higher_limit = 10.5
	lower_limit_arr_x = []
	higher_limit_arr_x = []
	lower_limit_arr_y = []
	higher_limit_arr_y = []

	for y in range(0,len(NOISE_PAD_1)):
		if NOISE_PAD_1[y] > higher_limit:
			higher_limit_arr_x.append(counter_1[y])
			higher_limit_arr_y.append(NOISE_PAD_1[y])
		elif NOISE_PAD_1[y] < lower_limit:
			lower_limit_arr_x.append(counter_1[y])
			lower_limit_arr_y.append(NOISE_PAD_1[y])
		y += 1

	for y in range(0,len(NOISE_PAD_2)):
		if NOISE_PAD_2[y] > higher_limit:
			higher_limit_arr_x.append(counter_2[y])
			higher_limit_arr_y.append(NOISE_PAD_2[y])
		elif NOISE_PAD_2[y] < lower_limit:
			lower_limit_arr_x.append(counter_2[y])
			lower_limit_arr_y.append(NOISE_PAD_2[y])
		y += 1

	print counter_1
	print counter_2

	data_temp = file1.split("_")
	noise_title = data_temp[3]+"_"+data_temp[1]+"_"+data_temp[4]+"_"+data_temp[5]+"_"+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]
	print data_temp
	noise_title_name = noise_title+".png"

	'''
	f=open(file_name_result,'a')
	if len(higher_limit_arr_x) != 0:
		f.write("===High Noise Channel in "+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+" with upper bound "+str(higher_limit)+" mV ===\n")
		for x in range(0, len(higher_limit_arr_x)):
			f.write(str(higher_limit_arr_x[x])+"\t"+str(higher_limit_arr_y[x])+"\n")
	if len(lower_limit_arr_x) != 0:
		f.write("===Low Noise Channel in "+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+" with lower bound "+str(lower_limit)+" mV ===\n")
		for x in range(0, len(lower_limit_arr_x)):
			f.write(str(lower_limit_arr_x[x])+"\t"+str(lower_limit_arr_y[x])+"\n")
	f.close()
	'''

	plt.plot(counter_1, NOISE_PAD_1,'bx-',markersize = 4, label = 'VMMB')
	plt.plot(counter_2, NOISE_PAD_2,'rx-',markersize = 4, label = 'VMMC')
	plt.plot(lower_limit_arr_x, lower_limit_arr_y,'ro',markersize = 8)
	plt.plot(higher_limit_arr_x, higher_limit_arr_y,'ro',markersize = 8)
	for xy in zip(higher_limit_arr_x, higher_limit_arr_y):
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=5)
	for xy in zip(lower_limit_arr_x, lower_limit_arr_y):
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=5)
	plt.title(noise_title)
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.legend(loc='upper right')
	plt.ylim(0,9)
	plt.xlim(0,128)
	plt.axhline(lower_limit, color='gray', linestyle='--',linewidth=0.5)
	plt.axhline(higher_limit, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(63.5, color='gray', linestyle='--',linewidth=0.5)
	plt.savefig(noise_title_name, dpi=600)
	time.sleep(1)
	plt.clf()


def Noise_plot_sFEB_6(file1, reverse_reg, VMM_MASK_MAP, file_name_result):

	ARRAY_Data_1 = np.loadtxt(file1,dtype='double', comments="*")
	Total_No_1 = len(ARRAY_Data_1)
	print Total_No_1

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	NOISE_DATA_2 = []
	NOISE_DATA_3 = []
	NOISE_DATA_4 = []
	NOISE_DATA_5 = []
	NOISE_DATA_6 = []
	NOISE_DATA_7 = []
	NOISE_PAD = []
	NOISE_STRIP = []
	NOISE_WIRE = []
	counter = []
	
	#sweep though all the list, group channel's data
	
	for y in range(0,64):
		NOISE_DATA_2.append(ARRAY_Data_1[y][1])
		y += 1

	for y in range(0,64):
		NOISE_DATA_3.append(ARRAY_Data_1[y][2])
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_4.append(ARRAY_Data_1[y][3])
		y += 1

	for y in range(0,64):
		NOISE_DATA_5.append(ARRAY_Data_1[y][4])
		y += 1

	for y in range(0,64):
		NOISE_DATA_6.append(ARRAY_Data_1[y][5])
		y += 1

	for y in range(0,64):
		NOISE_DATA_7.append(ARRAY_Data_1[y][6])
		y += 1


	for x in range(0,len(VMM_MASK_MAP)):
		if VMM_MASK_MAP[x][0] == 0:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_0[y_temp])
			counter.append(y_temp)
		elif VMM_MASK_MAP[x][0] == 1:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_1[y_temp])
			counter.append(y_temp+64)
		elif VMM_MASK_MAP[x][0] == 2:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_2[y_temp])
			counter.append(y_temp+128)
		elif VMM_MASK_MAP[x][0] == 3:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_3[y_temp])
			counter.append(y_temp+192)
		elif VMM_MASK_MAP[x][0] == 4:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_4[y_temp])
			counter.append(y_temp+256)
		elif VMM_MASK_MAP[x][0] == 5:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_5[y_temp])
			counter.append(y_temp+320)
		elif VMM_MASK_MAP[x][0] == 6:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_6[y_temp])
			counter.append(y_temp+384)
		elif VMM_MASK_MAP[x][0] == 7:
			y_temp = int(VMM_MASK_MAP[x][1])
			NOISE_STRIP.append(NOISE_DATA_7[y_temp])
			counter.append(y_temp+448)

	print NOISE_DATA_2
	print NOISE_DATA_3
	print NOISE_DATA_4
	print NOISE_DATA_5
	print NOISE_DATA_6
	print NOISE_DATA_7

	#For sFEB strip, add VMM0 to VMM7 into series(TBD: Need to know if it is going reversingly)
	#Plot data on graphs

	if reverse_reg == 1:
		NOISE_STRIP.reverse()
		print('list reversed')
	else:
		print('no reverse')

	lower_limit = 0.9
	higher_limit = 1.9
	lower_limit_arr_x = []
	higher_limit_arr_x = []
	lower_limit_arr_y = []
	higher_limit_arr_y = []


	for y in range(0,len(NOISE_STRIP)):
		if NOISE_STRIP[y] > higher_limit:
			higher_limit_arr_x.append(counter[y])
			higher_limit_arr_y.append(NOISE_STRIP[y])
		elif NOISE_STRIP[y] < lower_limit:
			lower_limit_arr_x.append(counter[y])
			lower_limit_arr_y.append(NOISE_STRIP[y])
		y += 1

	print lower_limit_arr_x
	print len(lower_limit_arr_x)
	print higher_limit_arr_x

	lower_limit_arr_x_T = []
	higher_limit_arr_x_T = []
	
	if len(lower_limit_arr_x) != 0:
		for y in range(0,len(lower_limit_arr_x)):
			num_temp = lower_limit_arr_x[y]
			lower_limit_arr_x_Q = num_temp // 64
			lower_limit_arr_x_R = num_temp % 64
			lower_limit_arr_x_T.append([lower_limit_arr_x_Q,lower_limit_arr_x_R])

	if len(higher_limit_arr_x) != 0:
		for y in range(0,len(higher_limit_arr_x)):
			higher_limit_arr_x_Q = int(higher_limit_arr_x[y]) // 64
			higher_limit_arr_x_R = int(higher_limit_arr_x[y]) % 64
			higher_limit_arr_x_T.append([higher_limit_arr_x_Q,higher_limit_arr_x_R])

	print lower_limit_arr_x_T 
	print higher_limit_arr_x_T
	
	data_temp = file1.split("_")
	noise_title = data_temp[3]+"_"+data_temp[1]+"_"+data_temp[4]+"_"+data_temp[5]+"_"+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]
	noise_title_name = noise_title+".png"
	'''
	f=open(file_name_result,'a')
	if len(higher_limit_arr_x_T) != 0:
		f.write("===High Noise Channel in "+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+" with upper bound "+str(higher_limit)+" mV ===\n")
		for x in range(0, len(higher_limit_arr_x_T)):
			f.write(str(higher_limit_arr_x_T[x][0])+"\t"+str(higher_limit_arr_x_T[x][1])+"\n")
	if len(lower_limit_arr_x_T) != 0:
		f.write("===Low Noise Channel in "+data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+" with lower bound "+str(lower_limit)+" mV ===\n")
		for x in range(0, len(lower_limit_arr_x_T)):
			f.write(str(lower_limit_arr_x_T[x][0])+"\t"+str(lower_limit_arr_x_T[x][1])+"\n")
	f.close()
	'''
	plt.plot(counter, NOISE_STRIP,'bx-',markersize = 4)
	plt.plot(lower_limit_arr_x, lower_limit_arr_y,'ro',markersize = 8)
	plt.plot(higher_limit_arr_x, higher_limit_arr_y,'ro',markersize = 8)
	for xy in zip(higher_limit_arr_x, higher_limit_arr_y):
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=5)
	for xy in zip(lower_limit_arr_x, lower_limit_arr_y):
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=5)
	plt.title(noise_title)
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.legend(loc='upper right')
	plt.ylim(0,3)
	plt.xlim(0,520)
	plt.axhline(lower_limit, color='gray', linestyle='--',linewidth=0.5)
	plt.axhline(higher_limit, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(63.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(127.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(191.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(255.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(319.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(383.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(447.5, color='gray', linestyle='--',linewidth=0.5)
	plt.axvline(512, color='gray', linestyle='--',linewidth=0.5)
	plt.savefig(noise_title_name, dpi=600)
	time.sleep(1)
	plt.clf()


