'''
try:
  import tkinter as tk
  import tkinter.ttk as ttk
  import tkinter.scrolledtext as tkst
  from tkinter import filedialog
  import PIL
except ImportError:
  import Tkinter as tk
  import ttk
  import ScrolledText as tkst
  import tkFileDialog as filedialog
  import PIL
'''
import Tkinter as tk
import ttk
import ScrolledText as tkst
import tkFileDialog as filedialog
import PIL
import sys
from glob import glob
from tkfilebrowser import askopendirname, askopenfilenames, asksaveasfilename
from Noise_analysis_plot_funct_mass import *
from search_mapping_funct import *
import tkMessageBox
import os

global logtext


# User selection tool for importing raw data files
def c_open_file_old_raw():
  global flist
  #flist = list(filedialog.askopenfilenames(parent=root, initialdir='/Users/andychen/Documents/ATLAS/gitlab/stgcnoisemeasurement/', filetypes=[("DAT", "*.dat"), ("PNG", "*.png"), ("JPEG", "*.jpg"), ("All files", "*")]))
  flist = list(filedialog.askopenfilenames(parent=root, filetypes=[("DAT", "*.dat"), ("PNG", "*.png"), ("JPEG", "*.jpg"), ("All files", "*")]))
  print(flist)
  
  logtext.insert(tk.END,"Files selected: "+str(len(flist))+"\n")
  for x in range((len(flist))):
    logtext.insert(tk.END,flist[x])
    logtext.insert(tk.END,"\n")

  logtext.insert(tk.END,"\n")
  print(target_dir)


# User selection tool for selecting target directory for plots
def c_open_target_dir():
  global target_dir
  target_dir = filedialog.askdirectory()
  logtext.insert(tk.END,"Target directory: "+target_dir+"\n\n")
  print(target_dir)
  

# Reverses the plotting order of channels
def reverse_chn_count():
  global reverse_reg
  reverse_reg = var_r.get()
  print (var_r.get())


# Parses raw data files for sFEB file pairs with four (4) VMMs each and processes noise RMS measurements into plot with all VMM channels
def combine_file_sFEB_4():
  global flist
  global target_dir
  global reverse_reg
  global VMM_MASK_MAP

  try:
    target_dir
  except NameError:
    target_dir = os.getcwd()
    print(target_dir)

  try:
    flist
  except NameError:
    logtext.insert(tk.END,"No files selected.\n\n")
  else:
    rep = flist[:]
    print(len(rep))
    if (len(rep)<=1):
      logtext.insert(tk.END,"Too few files for sFEB plot construction. Needs at least two files.\n\n")
    else:
      # parsing for compatible data file pairs
      address_temp = []
      run_match = []
      scan_list = []

      for x in range(len(rep)-1,-1,-1):
        data_temp_0 = rep[x].split("_")
        if (len(data_temp_0) == 17):
          if (data_temp_0[3] == 'sFEB'):
            address_temp.append(data_temp_0)
          else:
            logtext.insert(tk.END,"Skipping incompatible file: "+rep[x]+"\n")
            del rep[x]
        else:
          logtext.insert(tk.END,"Skipping incompatible file: "+rep[x]+"\n")
          del rep[x]

      print address_temp

      for y in range(0,len(address_temp)):
        for z in range(y+1,len(address_temp)):
          if (address_temp[y][2:11] == address_temp[z][2:11]):
            run_match.append([address_temp[y][1],address_temp[z][1]])

      print run_match
      logtext.insert(tk.END,"Number of matching data file pairs: "+str(len(run_match))+"\n")

      # Mapping all the file back in the list
      for x in range(len(run_match)-1,-1,-1):
        run1_indx = (zip(*address_temp)[1]).index(run_match[x][0])
        run2_indx = (zip(*address_temp)[1]).index(run_match[x][1])
        if (address_temp[run1_indx][12:16] == ['0','1','2','3']) & (address_temp[run2_indx][12:16] == ['4','5','6','7']):
          file1_temp = rep[run2_indx]
          file2_temp = rep[run1_indx]
          scan_list.append([file1_temp,file2_temp])
        elif (address_temp[run2_indx][12:16] == ['0','1','2','3']) & (address_temp[run1_indx][12:16] == ['4','5','6','7']):
          file1_temp = rep[run1_indx]
          file2_temp = rep[run2_indx]
          scan_list.append([file1_temp,file2_temp])
        else:
          logtext.insert(tk.END,"Incompatible file pair: ("+str(run_match[x][0])+", "+str(run_match[x][1])+")\n")
          del run_match[x]

      print scan_list

      file_name_result = "Result_Noise_"+address_temp[0][5]+"_sFEB.dat"
      
      for x in range(0,len(scan_list)):
        data_temp_0 = scan_list[x][0].split("_")
        Quad_Side = data_temp_0[6]
        Layer = data_temp_0[7][1] # only needs number
        PADSTRIP = "strip"
        reverse_reg = var_r.get()
        print ("Quad_Side: "+Quad_Side)
        print ("Layer: "+Layer)
        print ("Det_Type: "+PADSTRIP)
        print (reverse_reg)
        VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
        
        if list(Quad_Side)[1] == 'S':
          Wedge_Size = 'Small'
        elif list(Quad_Side)[1] == 'L':
          Wedge_Size = 'Large'
        else:
          logtext.insert(tk.END,"File pair wedge size neither small nor large, please check: ("+str(run_match[x][0])+", "+str(run_match[x][1])+")\n")

        if list(Quad_Side)[3] == 'P':
          Wedge_Side = 'Pivot'
        elif list(Quad_Side)[3] == 'C':
          Wedge_Side = 'Confirm'
        else:
          logtext.insert(tk.END,"File pair wedge size neither pivot nor confirm, please check: ("+str(run_match[x][0])+", "+str(run_match[x][1])+")\n")

        Wedge_Quad = list(Quad_Side)[2]

        plotfname = Noise_plot_sFEB_4(scan_list[x][1], scan_list[x][0], reverse_reg, VMM_MASK_MAP, file_name_result, target_dir, Wedge_Size, Wedge_Side, Wedge_Quad, Layer)

        logtext.insert(tk.END,plotfname+"\n")

      logtext.insert(tk.END,"Plots created: "+str(len(scan_list))+"\n")
      logtext.insert(tk.END,"Output directory: "+target_dir+"\n\n")


# Parses one (1) raw data file for sFEB with six (6) VMMs and processes noise RMS measurements into plot with all VMM channels
def combine_file_sFEB_6():
  global flist
  global target_dir
  global reverse_reg
  global VMM_MASK_MAP

  try:
    target_dir
  except NameError:
    target_dir = os.getcwd()
    print(target_dir)

  try:
    flist
  except NameError:
    logtext.insert(tk.END,"No files selected.\n\n")
  else:
    rep = flist[:]
    print(len(rep))

    if (len(rep)<=0):
      logtext.insert(tk.END,"Too few files for sFEB plot construction. Needs at least one file.\n\n")
    else:
      for x in range(len(rep)-1,-1,-1):
        data_temp_0 = rep[x].split("_")
        if (len(data_temp_0) == 15):
          if (data_temp_0[3] != 'pFEB'):
            logtext.insert(tk.END,"Skipping incompatible file: "+rep[x]+"\n")
            del rep[x]
        else:
          logtext.insert(tk.END,"Skipping incompatible file: "+rep[x]+"\n")
          del rep[x]

      for x in range(0,len(rep)):
        data_temp_0 = rep[x].split("_")
        file_name_result = "Result_Noise_"+data_temp_0[5]+"_sFEB.dat"
        Quad_Side = data_temp_0[6]
        Layer = data_temp_0[7][1] # only needs number
        PADSTRIP = "strip"
        reverse_reg = var_r.get()
        print ("Quad_Side:"+Quad_Side)
        print ("Layer:"+Layer)
        print ("Det_Type:"+PADSTRIP)
        print (reverse_reg)
        VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
        plotfname = Noise_plot_sFEB_6(rep[x], reverse_reg, VMM_MASK_MAP, file_name_result, target_dir)

        logtext.insert(tk.END,plotfname+" created.\n")

      logtext.insert(tk.END,"Plots created: "+str(len(rep))+"\n")
      logtext.insert(tk.END,"Output directory: "+target_dir+"\n\n")



# Parses one (1) raw data file for pFEB with two (2) VMMs and processes noise RMS measurements into plot with all VMM channels
def combine_file_pFEB():
  global flist
  global target_dir
  global VMM0_S
  global VMM0_E
  global VMM1_S
  global VMM1_E
  global VMM2_S
  global VMM2_E
  global reverse_reg
  global VMM_MASK_MAP

  try:
    target_dir
  except NameError:
    target_dir = os.getcwd()
    print(target_dir)

  try:
    flist
  except NameError:
    logtext.insert(tk.END,"No files selected.\n\n")
  else:
    rep = flist[:]
    print(len(rep))
    if (len(rep)<=0):
      logtext.insert(tk.END,"Too few files for pFEB plot construction. Needs at least one (1) file.\n\n")
    else:
      for x in range(len(rep)-1,-1,-1):
        data_temp_0 = rep[x].split("_")
        if (len(data_temp_0) == 15):
          if (data_temp_0[3] != 'pFEB'):
            logtext.insert(tk.END,"Skipping incompatible file: "+rep[x]+"\n")
            del rep[x]
        else:
          logtext.insert(tk.END,"Skipping incompatible file: "+rep[x]+"\n")
          del rep[x]

      for x in range(0,len(rep)):
        data_temp_0 = rep[x].split("_")
        file_name_result = "Result_Noise_"+data_temp_0[5]+"_pFEB.dat"
        Quad_Side = data_temp_0[6]
        Layer = data_temp_0[7][1] # only needs number
        PADSTRIP = "pad"
        reverse_reg = var_r.get()
        print ("Quad_Side:"+Quad_Side)
        print ("Layer:"+Layer)
        print ("Det_Type:"+PADSTRIP)
        print (reverse_reg)
        VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
        plotfname = Noise_plot_pFEB(rep[x], reverse_reg, VMM_MASK_MAP, file_name_result, target_dir)

        logtext.insert(tk.END,plotfname+" created.\n")

      logtext.insert(tk.END,"Plots created: "+str(len(rep))+"\n")
      logtext.insert(tk.END,"Output directory: "+target_dir+"\n\n")


# ==== MAIN SCRIPT ====

root = tk.Tk()

root.title("sTGC Wedge Noise Measurement Plot")

style = ttk.Style(root)
style.theme_use("clam")

# == GUI title ==
tk.Label(root, text='sTGC Wedge Noise Measurement Plot', font=20).grid(row=0, column=0)
ttk.Separator(root,orient=tk.HORIZONTAL).grid(row=1, columnspan=1, sticky='ew')

# == Data file selection ==
tk.Message(root, text = "").grid(row=10) # spacer
tk.Label(root, text='1. Open Raw Files for Noise RMS Measurements', font=20).grid(row=11, column=0, padx=4, pady=4, sticky='ew')
tk.Button(root, text="Open Files", command=c_open_file_old_raw, font=20).grid(row=12, column=0, pady=4)

tk.Message(root, text = "").grid(row=15) # spacer
tk.Label(root, text="2. Select Target Directory", font=20).grid(row=16, column=0, padx=4, pady=4, sticky='ew')
tk.Button(root, text="Set Target Directory", command=c_open_target_dir, font=20).grid(row=17, column=0, pady=4)


# == Plot creation options ==
tk.Message(root, text = "").grid(row=20) # spacer
tk.Label(root, text="3. Select Plot Option", font=20).grid(row=21, column=0, padx=4, pady=4, sticky='ew')

# sFEB plotting button (4 channels per dat file)
tk.Button(root, text='Combine and plot data for sFEB (For 4 channels)', command=combine_file_sFEB_4, font=20).grid(row=22, column=0, pady=4)

# sFEB plotting button (6 channels per dat file)
tk.Button(root, text='Combine and plot data for sFEB (For 6 channels)', command=combine_file_sFEB_6, font=20).grid(row=23, column=0, pady=4)

# pFEB plotting button (4 channels per dat file)
tk.Button(root, text='Plot noise data for pFEB', command=combine_file_pFEB, font=20).grid(row=24, column=0, pady=4)

# Add reverse option (for strips that counts in desending order)
var_r = tk.IntVar()
tk.Checkbutton(root, text="Reverse Channel Order *WIP*", variable=var_r, font=20).grid(row=25, column=0, pady=4) # only reverses channels of first dat file


# == Log output textbox ==
tk.Message(root, text = "").grid(row=90) # spacer
ttk.Separator(root,orient=tk.HORIZONTAL).grid(row=91, columnspan=1, sticky='ew')
tk.Label(root, text="GUI Log:", font=20).grid(row=92, column=0, padx=4, pady=4, sticky='ew')
logtext = tkst.ScrolledText(root, height=20, bd=2, highlightbackground="black")
logtext.grid(row=93, column=0, padx=4, pady=4, sticky='ew')

# == GUI exit ==
tk.Button(root, text='Exit', command=root.quit,font=20).grid(row=100, column=0)

root.mainloop()





