import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

def missing_elements(L):
    start, end = L[0], L[-1]
    return sorted(set(range(start, end + 1)).difference(L))

def mapping_output(Quad_Side,Layer,Det_Type):
	global VMM0_ST
	global VMM0_EN
	ARRAY_Data = np.loadtxt('mapping.txt',dtype='str')
	Total_No = len(ARRAY_Data)
	
	ARRAY_REG = []
	REG_TEMP = []
	
	#Quad_Side = 'QS1P'
	#Layer = '1'
	#Det_Type = 'strip'
	
	Quad_Side_SEG = list(Quad_Side)
	if Quad_Side[3] == 'P':
		if Layer == '1':
			Gas_Gap = '1'
		elif Layer == '2':
			Gas_Gap = '2'
		elif Layer == '3':
			Gas_Gap = '3'
		elif Layer == '4':
			Gas_Gap = '4'
	elif Quad_Side[3] == 'C':
		if Layer == '1':
			Gas_Gap = '4'
		elif Layer == '2':
			Gas_Gap = '3'
		elif Layer == '3':
			Gas_Gap = '2'
		elif Layer == '4':
			Gas_Gap = '1'

	print ("Gas Gap:"+Gas_Gap)

	for x in range(0,Total_No):
		if (ARRAY_Data[x][0] == Quad_Side) & (ARRAY_Data[x][1] == Gas_Gap) & (ARRAY_Data[x][2] == Det_Type):
			VMM_CH = int(ARRAY_Data[x][3]) - 1
			REG_TEMP = [VMM_CH,ARRAY_Data[x][4]]
			ARRAY_REG.append(REG_TEMP)
			REG_TEMP = []
	
	print ARRAY_REG
	return ARRAY_REG

