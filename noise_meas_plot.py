try:
    import tkinter as tk
    import tkinter.ttk as ttk
    from tkinter import filedialog
    import PIL
except ImportError:
    import Tkinter as tk
    import ttk
    import tkFileDialog as filedialog
    import PIL
import sys
from glob import glob
from tkfilebrowser import askopendirname, askopenfilenames, asksaveasfilename
#from Noise_analysis_plot_funct import *
from Noise_analysis_plot_funct_mass import *
from search_mapping_funct import *
import tkMessageBox

root = tk.Tk()

root.title("sTGC Wedge Noise Measurement Plot")

style = ttk.Style(root)
style.theme_use("clam")


def c_open_file_old_raw():
    global rep
    rep = filedialog.askopenfilenames(parent=root, initialdir='~/stgcnoisemeasurement', initialfile='tmp',
    filetypes=[("DAT", "*.dat"), ("PNG", "*.png"), ("JPEG", "*.jpg"), ("All files", "*")])
    print(rep)
    #filetext = tk.Text(root)
    #filetext.grid(row=3, column=0, padx=4, pady=4, sticky='ew')
    filetext.insert(tk.END,rep)
    filetext.insert(tk.END,"\n\n")
    

def c_open_file_old_spacer():
    global rep_spacer
    rep = filedialog.askopenfilenames(parent=root, initialdir='/home/nsw/stgc_noise_measurement', initialfile='tmp',
    filetypes=[("DAT", "*.dat"), ("PNG", "*.png"), ("JPEG", "*.jpg"), ("All files", "*")])
    print(rep_spacer)
'''
def c_open_file():
    rep = askopenfilenames(parent=root, initialdir='/', initialfile='tmp',
    filetypes=[("DAT", "*.dat"), ("Pictures", "*.png|*.jpg|*.JPG"), ("All files", "*")])
    print(rep)
'''
tk.Label(root, text='Open Raw Files For Noise Measurement Plot',font=20).grid(row=0, column=0, padx=4, pady=4, sticky='ew')
#tk.Label(root, text='tkfilebrowser dialogs').grid(row=0, column=1, padx=4, pady=4, sticky='ew')
tk.Button(root, text="Open files", command=c_open_file_old_raw,font=20).grid(row=1, column=0, padx=4, pady=4, sticky='ew')
global filetext
filetext = tk.Text(root)
filetext.grid(row=3, column=0, padx=4, pady=4, sticky='ew')
#tk.Label(root, text='Open Files For Spacer',font=20).grid(row=0, column=1, padx=4, pady=4, sticky='ew')
#tk.Button(root, text="Open files", command=c_open_file_old_spacer,font=20).grid(row=1, column=1, padx=4, pady=4, sticky='ew')
#tk.Button(root, text="Open folder", command=c_open_dir_old).grid(row=2, column=0, padx=4, pady=4, sticky='ew')
#tk.Button(root, text="Save file", command=c_save_old).grid(row=3, column=0, padx=4, pady=4, sticky='ew')
#tk.Button(root, text="Open files", command=c_open_file).grid(row=1, column=1, padx=4, pady=4, sticky='ew')
#tk.Button(root, text="Open folder", command=c_open_dir).grid(row=2, column=1, padx=4, pady=4, sticky='ew')
#tk.Button(root, text="Save file", command=c_save).grid(row=3, column=1, padx=4, pady=4, sticky='ew')

#=====================================================Insert empty row===========================================================
tk.Message(root, text = " ").grid(row=4)

'''
#=====================================================CHOOSE Chamber Type========================================================
# Just use to store on database

var_CHM = tk.IntVar()
var_CHM.set(1)
languages = [("Bench"), ("QL1"), ("QL2"), ("QL3"), ("QS1"), ("QS2"), ("QS3")]

def ShowChoice_CHM():
    global Size_ChamberNo
    print(var_CHM.get())
    if var_CHM.get() == 0:
        Size_ChamberNo = "QS0"
    elif var_CHM.get() == 1:
        Size_ChamberNo = "QL1"
    elif var_CHM.get() == 2:
        Size_ChamberNo = "QL2"
    elif var_CHM.get() == 3:
        Size_ChamberNo = "QL3"
    elif var_CHM.get() == 4:
        Size_ChamberNo = "QS1"
    elif var_CHM.get() == 5:
        Size_ChamberNo = "QS2"
    elif var_CHM.get() == 6:
        Size_ChamberNo = "QS3"

tk.Label(root, text="""Choose which environment on test:""", padx = 20).grid(row=5, column=0)
c = 0
for val, language in enumerate(languages):
    tk.Radiobutton(root, text=language, padx = 20, variable=var_CHM, command=ShowChoice_CHM, value=val).grid(row=6, column=c)
    c = c + 1

#=====================================================CHOOSE Layer========================================================

var_LAY = tk.IntVar()
var_LAY.set(1)
languages = [("Layer",1), ("Layer",2), ("Layer",3), ("Layer",4)]

def ShowChoice_LAY():
    global Layer
    print(var_LAY.get())
    if var_LAY.get() == 0:
        Layer = "1"
    elif var_LAY.get() == 1:
        Layer = "2"
    elif var_LAY.get() == 2:
        Layer = "3"
    elif var_LAY.get() == 3:
        Layer = "4"

tk.Label(root, text="""Choose which layer on test:""", padx = 20).grid(row=9, column=0)
c = 0
for val, language in enumerate(languages):
    tk.Radiobutton(root, text=language, padx = 20, variable=var_LAY, command=ShowChoice_LAY, value=val).grid(row=10, column=c)
    c = c + 1

#=====================================================CHOOSE Pivot/Confirm========================================================

var_PC = tk.IntVar()
var_PC.set(1)
languages = [("Pivot"), ("Confirm")]

def ShowChoice_PC():
    global PivCon
    print(var_PC.get())
    if var_PC.get() == 0:
        PivCon = "P"
    elif var_PC.get() == 1:
        PivCon = "C"

tk.Label(root, text="""Choose Pivot/Confirm:""", padx = 20).grid(row=7, column=0)
c = 0
for val, language in enumerate(languages):
    tk.Radiobutton(root, text=language, padx = 20, variable=var_PC, command=ShowChoice_PC, value=val).grid(row=8, column=c)
    c = c + 1

#=====================================================CHOOSE Strip/Pad========================================================

var_SP = tk.IntVar()
var_SP.set(1)
languages = [("Pad"), ("Strip")]

def ShowChoice_SP():
    global PADSTRIP
    print(var_SP.get())
    if var_SP.get() == 0:
        PADSTRIP = "pad"
    elif var_SP.get() == 1:
        PADSTRIP = "strip"

tk.Label(root, text="""Choose Strip/Pad:""", padx = 20).grid(row=11, column=0)
c = 0
for val, language in enumerate(languages):
    tk.Radiobutton(root, text=language, padx = 20, variable=var_SP, command=ShowChoice_SP, value=val).grid(row=12, column=c)
    c = c + 1
'''
#=====================================================Insert empty row===========================================================
tk.Message(root, text = " ").grid(row=13)

#=====================================================Add reverse option(for strips that counts in desending order)===========================================================

def reverse_chn_count():
	global reverse_reg
	reverse_reg = var_r.get()
	print (var_r.get())

var_r = tk.IntVar()
tk.Checkbutton(root, text="Reverse CHN", variable=var_r,font=20).grid(row=25, column=0, pady=4)

#======================================================Show mapping type==================================================
'''
def cominbine_mapping():
	global Size_ChamberNo
	global Layer
	global PivCon
	global PADSTRIP
	global VMM_MASK_MAP
	data_temp_0 = rep[0].split("_")
	if data_temp_0[3] == "pFEB":
			Quad_Side = data_temp_0[6]
			Layer_temp = list(data_temp_0[7])
			Layer = Layer_temp[1]
			PADSTRIP = "pad"
			#Quad_Side = Size_ChamberNo+PivCon
			print ("Quad_Side:"+Quad_Side)
			print ("Layer:"+Layer)
			print ("Det_Type:"+PADSTRIP)
			VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
			print VMM_MASK_MAP
	elif data_temp_0[3] == "sFEB":
		data_temp_1 = rep[1].split("_")
		if (data_temp_0[3] == data_temp_1[3]) & (data_temp_0[4] == data_temp_1[4]) & (data_temp_0[5] == data_temp_1[5]) & (data_temp_0[6] == data_temp_1[6]) & (data_temp_0[7] == data_temp_1[7]) & (data_temp_0[8] == data_temp_1[8]):
			Quad_Side = data_temp_0[6]
			Layer_temp = list(data_temp_0[7])
			Layer = Layer_temp[1]
			PADSTRIP = "strip"
			#Quad_Side = Size_ChamberNo+PivCon
			print ("Quad_Side:"+Quad_Side)
			print ("Layer:"+Layer)
			print ("Det_Type:"+PADSTRIP)
			VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
			print VMM_MASK_MAP

tk.Button(root, text='Combine mapping data', command=cominbine_mapping).grid(row=24, column=1, pady=4)
'''

#======================================================Combine Two raw data files and process, taking VMM not connected channel into consideration==================================================
def cominbine_file_sFEB_4():
	global rep
	global reverse_reg
	global VMM_MASK_MAP
	address_temp = []
	run_match = []
	del_number = []
	for x in range(1,len(rep)):
		data_temp_0 = rep[x].split("_")
		address_temp.append(data_temp_0)

	print address_temp

	for y in range(0,len(address_temp)):
		for z in range(0,len(address_temp)):
			if (address_temp[y][2] == address_temp[z][2])\
				& (address_temp[y][3] == address_temp[z][3])\
				& (address_temp[y][4] == address_temp[z][4])\
				& (address_temp[y][5] == address_temp[z][5])\
				& (address_temp[y][6] == address_temp[z][6])\
				& (address_temp[y][7] == address_temp[z][7])\
				& (address_temp[y][8] == address_temp[z][8])\
				& (address_temp[y][9] == address_temp[z][9])\
				& (address_temp[y][10] == address_temp[z][10]):
				run_match.append([address_temp[y][1],address_temp[z][1]])

	for x in range(0,len(run_match)):
		if (run_match[x][0] == run_match[x][1]):
			del_number.append(x)

	print run_match

	for x in range((len(del_number)-1),-1,-1):
		print x
		del run_match[del_number[x]]

	del_number = []
	
	for x in range(0,len(run_match)):
		for y in range(x+1,len(run_match)):
			if (run_match[x][0] == run_match[y][1]) & (run_match[x][1] == run_match[y][0]):
				del_number.append(y)

	del_number.sort()

	for x in range((len(del_number)-1),-1,-1):
		print x
		del run_match[del_number[x]]
	print run_match

	scanlist = []
	#Mapping all the file back in the list
	for x in range(0, len(run_match)):
		for y in range(0, len(address_temp)):
			if (run_match[x][0] == address_temp[y][1]) & (int(address_temp[y][12]) == 0) & (int(address_temp[y][13]) == 1) & (int(address_temp[y][14]) == 2) & (address_temp[y][15] == '3'):
				file1_temp = rep[y+1]
			elif (run_match[x][0] == address_temp[y][1]) & (int(address_temp[y][12]) == 4) & (int(address_temp[y][13]) == 5) & (int(address_temp[y][14]) == 6) & (address_temp[y][15] == '7'):
				file2_temp = rep[y+1]
			if (run_match[x][1] == address_temp[y][1]) & (int(address_temp[y][12]) == 0) & (int(address_temp[y][13]) == 1) & (int(address_temp[y][14]) == 2) & (address_temp[y][15] == '3'):
				file1_temp = rep[y+1]
			elif (run_match[x][1] == address_temp[y][1]) & (int(address_temp[y][12]) == 4) & (int(address_temp[y][13]) == 5) & (int(address_temp[y][14]) == 6) & (address_temp[y][15] == '7'):
				file2_temp = rep[y+1]
		scanlist.append([file1_temp,file2_temp])
	print scanlist
	
	file_name_result = "Result_Noise_"+address_temp[0][5]+"_sFEB.dat"
	
	for x in range(0,len(scanlist)):
		data_temp_0 = scanlist[x][0].split("_")
		data_temp_1 = scanlist[x][1].split("_")
		if (data_temp_0[3] == "sFEB") & (data_temp_1[3] == "sFEB"):
			Quad_Side = data_temp_0[6]
			Layer_temp = list(data_temp_0[7])
			Layer = Layer_temp[1]
			PADSTRIP = "strip"
			print ("Quad_Side:"+Quad_Side)
			print ("Layer:"+Layer)
			print ("Det_Type:"+PADSTRIP)
			VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
			file1 = scanlist[x][1]
			file2 = scanlist[x][0]
			reverse_reg = var_r.get()
			print (reverse_reg)
			
			if list(Quad_Side)[1] == 'S':
				Wedge_Size = 'Small'
			elif list(Quad_Side)[1] == 'L':
				Wedge_Size = 'Large'
			else:
				tkMessageBox.showwarning("Warning","The wedge size is neither small or large, please check")

			if list(Quad_Side)[3] == 'P':
				Wedge_Side = 'Pivot'
			elif list(Quad_Side)[3] == 'C':
				Wedge_Side = 'Confirm'
			else:
				tkMessageBox.showwarning("Warning","The wedge size is neither pivot or confirm, please check")

			Wedge_Quad = list(Quad_Side)[2]

			Noise_plot_sFEB_4(file1, file2, reverse_reg, VMM_MASK_MAP, file_name_result, Wedge_Size, Wedge_Side, Wedge_Quad, Layer)

tk.Button(root, text='Combine and plot data for sFEB(For 4 channels)', command=cominbine_file_sFEB_4,font=20).grid(row=26, column=0, pady=4)

#======================================================For sFEB with 6 channels' measurement, taking VMM not connected channel into consideration==================================================
def cominbine_file_sFEB_6():
	global rep
	global reverse_reg
	global VMM_MASK_MAP
	for x in range(1,len(rep)):
		data_temp_0 = rep[x].split("_")
		file_name_result = "Result_Noise_"+data_temp_0[5]+"_sFEB.dat"
		if data_temp_0[3] == "sFEB":
			Quad_Side = data_temp_0[6]
			Layer_temp = list(data_temp_0[7])
			Layer = Layer_temp[1]
			PADSTRIP = "strip"
			print ("Quad_Side:"+Quad_Side)
			print ("Layer:"+Layer)
			print ("Det_Type:"+PADSTRIP)
			VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
			file1 = rep[x]
			reverse_reg = var_r.get()
			print (reverse_reg)
			Noise_plot_sFEB_6(file1, reverse_reg, VMM_MASK_MAP, file_name_result)

tk.Button(root, text='Combine and plot data for sFEB(For 6 channels)', command=cominbine_file_sFEB_6,font=20).grid(row=26, column=1, pady=4)

#======================================================Combine Two raw data files and process, taking VMM not connected channel into consideration==================================================
def cominbine_file_pFEB():
	global rep
	global VMM0_S
	global VMM0_E
	global VMM1_S
	global VMM1_E
	global VMM2_S
	global VMM2_E
	global reverse_reg
	global VMM_MASK_MAP
	for x in range(1,len(rep)):
		data_temp_0 = rep[x].split("_")
		file_name_result = "Result_Noise_"+data_temp_0[5]+"_pFEB.dat"
		if data_temp_0[3] == "pFEB":
			Quad_Side = data_temp_0[6]
			Layer_temp = list(data_temp_0[7])
			Layer = Layer_temp[1]
			PADSTRIP = "pad"
			print ("Quad_Side:"+Quad_Side)
			print ("Layer:"+Layer)
			print ("Det_Type:"+PADSTRIP)
			VMM_MASK_MAP = mapping_output(Quad_Side,Layer,PADSTRIP)
			file1 = rep[x]
			reverse_reg = var_r.get()
			print (reverse_reg)
			#Noise_plot_pFEB(file1, VMM1_S, VMM1_E, VMM2_S, VMM2_E, reverse_reg)
			Noise_plot_pFEB(file1, reverse_reg, VMM_MASK_MAP, file_name_result)

	'''
	file1 = rep[0]
	reverse_reg = var_r.get()
	print (reverse_reg)
	#Noise_plot_pFEB(file1, VMM1_S, VMM1_E, VMM2_S, VMM2_E, reverse_reg)
	Noise_plot_pFEB(file1, reverse_reg, VMM_MASK_MAP)
	'''

tk.Button(root, text='Combine and plot data for pFEB', command=cominbine_file_pFEB,font=20).grid(row=26, column=2, pady=4)

tk.Button(root, text='Quit', command=root.quit,font=20).grid(row=45, column=3)


root.mainloop()
