import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

'''
if len(sys.argv)!=2:
  print("Usage: python TEST.py targetDir")
  sys.exit(0)

filelist = glob( sys.argv[1] + "/*.txt")
'''
def Noise_plot_sFEB(file1, file2, VMM0_S, VMM0_E, VMM1_S, VMM1_E, VMM2_S, VMM2_E, VMM3_S, VMM3_E, VMM4_S, VMM4_E, VMM5_S, VMM5_E, VMM6_S, VMM6_E, VMM7_S, VMM7_E, reverse_reg):
	#ARRAY_Data = np.loadtxt('0_pFEB009_VMM3_xxx.dat',dtype='double')
	ARRAY_Data_1 = np.loadtxt(file1,dtype='double')
	Total_No_1 = len(ARRAY_Data_1)
	print Total_No_1
	ARRAY_Data_2 = np.loadtxt(file2,dtype='double')
	Total_No_2 = len(ARRAY_Data_2)
	print Total_No_2

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	NOISE_DATA_0 = []
	NOISE_DATA_1 = []
	NOISE_DATA_2 = []
	NOISE_DATA_3 = []
	NOISE_DATA_4 = []
	NOISE_DATA_5 = []
	NOISE_DATA_6 = []
	NOISE_DATA_7 = []
	NOISE_PAD = []
	NOISE_STRIP = []
	NOISE_WIRE = []
	
	#sweep though all the list, group channel's data
	
	for y in range(0,64):
		NOISE_DATA_0.append(ARRAY_Data_1[y][1])
		y += 1

	for y in range(0,64):
		NOISE_DATA_1.append(ARRAY_Data_1[y][2])
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_2.append(ARRAY_Data_1[y][3])
		y += 1

	for y in range(0,64):
		NOISE_DATA_3.append(ARRAY_Data_1[y][4])
		y += 1

	for y in range(0,64):
		NOISE_DATA_4.append(ARRAY_Data_2[y][1])
		y += 1

	for y in range(0,64):
		NOISE_DATA_5.append(ARRAY_Data_2[y][2])
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_6.append(ARRAY_Data_2[y][3])
		y += 1

	for y in range(0,64):
		NOISE_DATA_7.append(ARRAY_Data_2[y][4])
		y += 1

	print NOISE_DATA_0
	print NOISE_DATA_1
	print NOISE_DATA_2
	print NOISE_DATA_3
	print NOISE_DATA_4
	print NOISE_DATA_5
	print NOISE_DATA_6
	print NOISE_DATA_7
	print VMM0_S
	print VMM0_E



	#For sFEB strip, add VMM0 to VMM7 into series(TBD: Need to know if it is going reversingly)
	'''
	VMM0_ST = int(VMM0_S) - 1
	VMM0_EN = int(VMM0_E) + 1	
	VMM1_ST = int(VMM1_S) - 1
	VMM1_EN = int(VMM1_E) + 1
	VMM2_ST = int(VMM2_S) - 1
	VMM2_EN = int(VMM2_E) + 1
	VMM3_ST = int(VMM3_S) - 1
	VMM3_EN = int(VMM3_E) + 1
	VMM4_ST = int(VMM4_S) - 1
	VMM4_EN = int(VMM4_E) + 1
	VMM5_ST = int(VMM5_S) - 1
	VMM5_EN = int(VMM5_E) + 1
	VMM6_ST = int(VMM6_S) - 1
	VMM6_EN = int(VMM6_E) + 1
	VMM7_ST = int(VMM7_S) - 1
	VMM7_EN = int(VMM7_E) + 1
	'''

	if (VMM0_S == 'N') & (VMM0_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_0[y])
	else:
		VMM0_ST = int(VMM0_S) - 1
		VMM0_EN = int(VMM0_E) + 1
		for y in range(0,VMM0_ST):
			NOISE_STRIP.append(NOISE_DATA_0[y])
		for y in range(VMM0_EN,64):
			NOISE_STRIP.append(NOISE_DATA_0[y])

	if (VMM1_S == 'N') & (VMM1_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_1[y])
	else:
		VMM1_ST = int(VMM1_S) - 1
		VMM1_EN = int(VMM1_E) + 1
		for y in range(0,VMM1_ST):
			NOISE_STRIP.append(NOISE_DATA_1[y])
		for y in range(VMM1_EN,64):
			NOISE_STRIP.append(NOISE_DATA_1[y])

	if (VMM2_S == 'N') & (VMM2_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_2[y])
	else:
		VMM2_ST = int(VMM2_S) - 1
		VMM2_EN = int(VMM2_E) + 1
		for y in range(0,VMM2_ST):
			NOISE_STRIP.append(NOISE_DATA_2[y])
		for y in range(VMM2_EN,64):
			NOISE_STRIP.append(NOISE_DATA_2[y])

	if (VMM3_S == 'N') & (VMM3_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_3[y])
	else:
		VMM3_ST = int(VMM3_S) - 1
		VMM3_EN = int(VMM3_E) + 1
		for y in range(0,VMM3_ST):
			NOISE_STRIP.append(NOISE_DATA_3[y])
		for y in range(VMM3_EN,64):
			NOISE_STRIP.append(NOISE_DATA_3[y])

	if (VMM4_S == 'N') & (VMM4_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_4[y])
	else:
		VMM4_ST = int(VMM4_S) - 1
		VMM4_EN = int(VMM4_E) + 1
		for y in range(0,VMM4_ST):
			NOISE_STRIP.append(NOISE_DATA_4[y])
		for y in range(VMM4_EN,64):
			NOISE_STRIP.append(NOISE_DATA_4[y])

	if (VMM5_S == 'N') & (VMM5_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_5[y])
	else:
		VMM5_ST = int(VMM5_S) - 1
		VMM5_EN = int(VMM5_E) + 1
		for y in range(0,VMM5_ST):
			NOISE_STRIP.append(NOISE_DATA_5[y])
		for y in range(VMM5_EN,64):
			NOISE_STRIP.append(NOISE_DATA_5[y])

	if (VMM6_S == 'N') & (VMM6_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_6[y])
	else:
		VMM6_ST = int(VMM6_S) - 1
		VMM6_EN = int(VMM6_E) + 1
		for y in range(0,VMM6_ST):
			NOISE_STRIP.append(NOISE_DATA_6[y])
		for y in range(VMM6_EN,64):
			NOISE_STRIP.append(NOISE_DATA_6[y])

	if (VMM7_S == 'N') & (VMM7_E == 'N'):
		for y in range(0,64):
			NOISE_STRIP.append(NOISE_DATA_7[y])
	else:
		VMM7_ST = int(VMM7_S) - 1
		VMM7_EN = int(VMM7_E) + 1
		for y in range(0,VMM7_ST):
			NOISE_STRIP.append(NOISE_DATA_7[y])
		for y in range(VMM1_EN,64):
			NOISE_STRIP.append(NOISE_DATA_7[y])



	'''
	for y in range(0,VMM0_ST):
		NOISE_PAD.append(NOISE_DATA_1[y])
	for y in range(VMM0_EN,64):
		NOISE_PAD.append(NOISE_DATA_1[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_2[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_3[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_4[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_5[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_6[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_7[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_8[y])

	#For pFEB PAD, add VMMB and VMMC into series
	#for y in range(0,64):
	#	NOISE_PAD.append(NOISE_DATA_2[y])
	#for y in range(0,64):
	#	NOISE_PAD.append(NOISE_DATA_3[y])
	'''

	#Plot data on graphs

	if reverse_reg == 1:
		NOISE_STRIP.reverse()
		print('list reversed')
	else:
		print('no reverse')
	
	counter = []

	lower_limit = 0.6
	higher_limit = 1.5
	lower_limit_arr_x = []
	higher_limit_arr_x = []
	lower_limit_arr_y = []
	higher_limit_arr_y = []

	for y in range(0,len(NOISE_STRIP)):
		if NOISE_STRIP[y] > higher_limit:
			higher_limit_arr_x.append(y)
			higher_limit_arr_y.append(NOISE_STRIP[y])
		elif NOISE_STRIP[y] < lower_limit:
			lower_limit_arr_x.append(y)
			lower_limit_arr_y.append(NOISE_STRIP[y])
		counter.append(y)
		y += 1

	print lower_limit_arr_x
	print higher_limit_arr_x

	data_temp = file1.split("_")
	noise_title = data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+"_"+data_temp[11]+"_"+data_temp[12]
	noise_title_name = noise_title+".png"

	plt.plot(counter, NOISE_STRIP,'bx-',markersize = 4)
	plt.plot(lower_limit_arr_x, lower_limit_arr_y,'ro',markersize = 8)
	plt.plot(higher_limit_arr_x, higher_limit_arr_y,'ro',markersize = 8)
	for xy in zip(higher_limit_arr_x, higher_limit_arr_y):                                       
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=12)
	for xy in zip(lower_limit_arr_x, lower_limit_arr_y):                                       
		plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data',fontsize=12)
	#plt.plot(counter, NOISE_DATA_2,'bx',markersize = 5)
	#plt.plot(counter, NOISE_DATA_3,'gx',markersize = 5)
	#plt.plot(counter, NOISE_DATA_4,'mx',markersize = 5)
	#plt.legend('VMMA',loc='upper right')
	#plt.title('Noise Plot')
	plt.title(noise_title)
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.legend(loc='upper right')
	plt.ylim(0,3)
	plt.axvline(x=63.5, color='black')
	plt.axvline(x=123.5, color='black')
	plt.axvline(x=191.5, color='black')
	plt.axvline(x=255.5, color='black')
	plt.axvline(x=319.5, color='black')
	plt.axvline(x=383.5, color='black')
	plt.axvline(x=447.5, color='black')
	plt.axvline(x=512, color='black')
	#plt.savefig(noise_title_name)
	plt.show()


def Noise_plot_pFEB(file1, VMM1_S, VMM1_E, VMM2_S, VMM2_E, reverse_reg):
	#ARRAY_Data = np.loadtxt('0_pFEB009_VMM3_xxx.dat',dtype='double')
	ARRAY_Data_1 = np.loadtxt(file1,dtype='double')
	Total_No_1 = len(ARRAY_Data_1)
	print Total_No_1

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	NOISE_DATA_1 = []
	NOISE_DATA_2 = []
	NOISE_PAD_1 = []
	NOISE_PAD_2 = []
	
	#sweep though all the list, group channel's data


	for y in range(0,64):
		NOISE_DATA_1.append(ARRAY_Data_1[y][2])
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_2.append(ARRAY_Data_1[y][3])
		y += 1


	print NOISE_DATA_1
	print NOISE_DATA_2

	#For pFEB PAD, add VMM1 to VMM2 into series(TBD: Need to know if it is going reversingly)

	if (VMM1_S == 'N') & (VMM1_E == 'N'):
		for y in range(0,64):
			NOISE_PAD_1.append(NOISE_DATA_1[y])
	else:
		VMM1_ST = int(VMM1_S) - 1
		VMM1_EN = int(VMM1_E) + 1
		for y in range(0,VMM1_ST):
			NOISE_PAD_1.append(NOISE_DATA_1[y])
		for y in range(VMM1_EN,64):
			NOISE_PAD_1.append(NOISE_DATA_1[y])

	if (VMM2_S == 'N') & (VMM2_E == 'N'):
		for y in range(0,64):
			NOISE_PAD_2.append(NOISE_DATA_2[y])
	else:
		VMM2_ST = int(VMM2_S) - 1
		VMM2_EN = int(VMM2_E) + 1
		for y in range(0,VMM2_ST):
			NOISE_PAD_2.append(NOISE_DATA_2[y])
		for y in range(VMM2_EN,64):
			NOISE_PAD_2.append(NOISE_DATA_2[y])

	#print len(NOISE_PAD)
	'''
	for y in range(0,VMM0_ST):
		NOISE_PAD.append(NOISE_DATA_1[y])
	for y in range(VMM0_EN,64):
		NOISE_PAD.append(NOISE_DATA_1[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_2[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_3[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_4[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_5[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_6[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_7[y])
	for y in range(0,64):
		NOISE_PAD.append(NOISE_DATA_8[y])

	#For pFEB PAD, add VMMB and VMMC into series
	#for y in range(0,64):
	#	NOISE_PAD.append(NOISE_DATA_2[y])
	#for y in range(0,64):
	#	NOISE_PAD.append(NOISE_DATA_3[y])
	'''

	#Plot data on graphs

	if reverse_reg == 1:
		NOISE_PAD_1.reverse()
		print('list reversed')
	else:
		print('no reverse')
	
	counter_1 = []
	counter_2 = []

	y = 0
	for y in range(0,64):
		counter_1.append(y)
		y += 1


	for y in range(64,128):
		counter_2.append(y)
		y += 1

	print counter_1
	print counter_2

	data_temp = file1.split("_")
	noise_title = data_temp[6]+"_"+data_temp[7]+"_"+data_temp[8]+"_"+data_temp[9]+"_"+data_temp[10]+"_"+data_temp[11]+"_"+data_temp[12]
	print data_temp
	noise_title_name = noise_title+".png"

	plt.plot(counter_1, NOISE_PAD_1,'bx-',markersize = 4, label = 'VMMB')
	plt.plot(counter_2, NOISE_PAD_2,'rx-',markersize = 4, label = 'VMMC')
	#plt.plot(counter, NOISE_DATA_2,'bx',markersize = 5)
	#plt.plot(counter, NOISE_DATA_3,'gx',markersize = 5)
	#plt.plot(counter, NOISE_DATA_4,'mx',markersize = 5)
	#plt.legend('VMMA',loc='upper right')
	#plt.title('Noise Plot for pFEB')
	plt.title(noise_title)
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.legend(loc='upper right')
	plt.savefig(noise_title_name)
	#plt.show()



