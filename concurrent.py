import os
import sys
from multiprocessing import Process

def script1():
    os.system("/root/Downloads/scripts/recv_dgramsocket.py")     
def script2():
    os.system("/root/Downloads/scripts/main.py") 

if __name__ == '__main__':
    p = Process(target=script1)
    q = Process(target=script2)
    p.start()
    q.start()
    p.join()
    q.join()
