#!/usr/bin/python

import xml.sax

class MovieHandler( xml.sax.ContentHandler ):
   def __init__(self):
      self.CurrentData = ""
      self.sp = ""
      self.sdpk = ""
      self.sbmx = ""
      self.sbft = ""
      self.sbfp = ""
      self.sbfm = ""
      self.slg = ""
      self.smch = ""
      self.scmx = ""
      self.sfa = ""
      self.sfam = ""
      self.spt = ""
      self.sfm = ""
      self.sg = ""
      self.sng = ""
      self.stot = ""
      self.sttt = ""
      self.ssh = ""
      self.stc = ""
      self.sdt = ""
      self.sdp = ""
      self.sc10b = ""
      self.sc8b = ""
      self.sc6b = ""
      self.s8b = ""
      self.s6b = ""
      self.s10b = ""
      self.sdcks = ""
      self.sdcka = ""
      self.sdck6b = ""
      self.sdrv = ""
      self.stpp = ""
      self.res = ""
      self.slvs = ""
      self.s32 = ""
      self.stcr = ""
      self.ssart = ""
      self.srec = ""
      self.stlc = ""
      self.sbip = ""
      self.srat = ""
      self.sfrst = ""
      self.slvsbc = ""
      self.slvstp = ""
      self.slvstk = ""
      self.slvsdt = ""
      self.slvsart = ""
      self.slvstki = ""
      self.slvsena = ""
      self.slvs6b = ""
      self.sL0enaV = ""
      self.slh = ""
      self.slxh = ""
      self.stgc = ""
      self.nu = ""
      self.reset = ""
      self.sc = ""

   # Call when an element starts
   def startElement(self, tag, attributes):
      self.CurrentData = tag
      if tag == "reg":
         print "*****reg*****"
         title = attributes["addr"]
         print "Address:", title

   # Call when an elements ends
   def endElement(self, tag):
      if self.CurrentData == "sp":
         print "{sp} Charge polarity, [0] negative, [1] positive:", self.sp
      elif self.CurrentData == "sdpk":
         print "{sdpk} disable-at-peak:", self.sdpk
      elif self.CurrentData == "sbmx":
         print "{sbmx} routes analog monitor to PDO output:", self.sbmx
      elif self.CurrentData == "sbft":
         print "{sbft} analog output buffers, [1] enable TDO:", self.sbft
      elif self.CurrentData == "sbfp":
         print "{sbfp} analog output buffers, [1] enable PDO:", self.sbfp
      elif self.CurrentData == "sbfm":
         print "{sbfm} analog output buffers, [1] enable MO:", self.sbfm
      elif self.CurrentData == "slg":
         print "{slg} leakage current disable ([0] enabled):", self.slg
      elif self.CurrentData == "smch":
         print "{smch} channel monitor (Channel 0 to 63):", self.smch
      elif self.CurrentData == "scmx":
         print "{scmx} monitor multiplexing; [0] Common monitor, [1] Channel monitor:", self.scmx
      elif self.CurrentData == "sfa":
         print "{sfa} ART enable [1]:", self.sfa
      elif self.CurrentData == "sfam":
         print "{sfam} ART mode (sfam [0] timing at threshold, [1] timing at peak):", self.sfam
      elif self.CurrentData == "spt":
         print "{spt} peaktime (200 [00], 100 [01], 50 [10], 25 [11] ns ):", self.spt
      elif self.CurrentData == "sfm":
         print "{sfm} enables full-mirror (AC) and high-leakage operation (enables SLH):", self.sfm
      elif self.CurrentData == "sg":
         print "{sg} gain (0.5 [000], 1 [001], 3 [010], 4.5 [011], 6 [100], 9 [101], 12 [110], 16 [111] mV/fC):", self.sg
      elif self.CurrentData == "sng":
         print "{sng} neighbor (channel and chip) triggering enable:", self.sng
      elif self.CurrentData == "stot":
         print "{stot} stot[00,01,10,11]: TtP,ToT,PtP,PtT:", self.stot
      elif self.CurrentData == "sttt":
         print "{sttt} enables direct-output logic (both timing and s6b):", self.sttt
      elif self.CurrentData == "ssh":
         print "{ssh} enables sub-hysteresis discrimination:", self.ssh
      elif self.CurrentData == "stc":
         print "{stc} TAC slope adjustment (60 [00], 100 [01], 350[10], 650 [11] ns ):", self.stc
      elif self.CurrentData == "sdt":
         print "{sdt} coarse threshold DAC:", self.sdt
      elif self.CurrentData == "sdp":
         print "{sdp} test pulse DAC:", self.sdp
      elif self.CurrentData == "sc10b":
         print "{sc10b} 10-bit ADC conv. time (increase subtracts 60 ns):", self.sc10b
      elif self.CurrentData == "sc8b":
         print "{sc8b} 8-bit ADC conv. time (increase subtracts 60 ns):", self.sc8b
      elif self.CurrentData == "sc6b":
         print "{sc6b} 6-bit ADC conv. time:", self.sc6b
      elif self.CurrentData == "s8b":
         print "{s8b} 8-bit ADC conversion mode:", self.s8b
      elif self.CurrentData == "s6b":
         print "{s6b} enables 6-bit ADC (requires sttt enabled):", self.s6b
      elif self.CurrentData == "s10b":
         print "{s10b} enables high resolution ADCs (10/8-bit ADC enable):", self.s10b
      elif self.CurrentData == "sdcks":
         print "{sdcks} enables high resolution ADCs (10/8-bit ADC enable):", self.sdcks
      elif self.CurrentData == "sdcka":
         print "{sdcka} dual clock edge serialized ART enable:", self.sdcka
      elif self.CurrentData == "sdck6b":
         print "{sdck6b} dual clock edge serialized 6-bit enable:", self.sdck6b
      elif self.CurrentData == "sdrv":
         print "{sdrv} tristates analog outputs with token, used in analog mode:", self.sdrv
      elif self.CurrentData == "stpp":
         print "{stpp} timing outputs control 2:", self.stpp
      elif self.CurrentData == "res":
         print "{res}:", self.res
      elif self.CurrentData == "slvs":
         print "{slvs} enables direct output IOs:", self.slvs
      elif self.CurrentData == "s32":
         print "{s32} skips channels 16-47 and makes 15 and 48 neighbors:", self.s32
      elif self.CurrentData == "stcr":
         print "{stcr} enables auto-reset (at the end of the ramp, if no stop occurs):", self.stcr
      elif self.CurrentData == "ssart":
         print "{ssart} enables ART flag synchronization (trail to next trail):", self.ssart
      elif self.CurrentData == "srec":
         print "{srec} enables fast recovery from high charge:", self.srec
      elif self.CurrentData == "stlc":
         print "{stlc} enables mild tail cancellation (when enabled, overrides sbip):", self.stlc
      elif self.CurrentData == "sbip":
         print "{sbip} enables bipolar shape:", self.sbip
      elif self.CurrentData == "srat":
         print "{srat} enables timing ramp at threshold:", self.srat
      elif self.CurrentData == "sfrst":
         print "{sfrst} enables fast reset at 6-b completion:", self.sfrst
      elif self.CurrentData == "slvsbc":
         print "{slvsbc} enable slvs 100 Ohm termination on ckbc:", self.slvsbc
      elif self.CurrentData == "slvstp":
         print "{slvstp} enable slvs 100 Ohm termination on cktp:", self.slvstp
      elif self.CurrentData == "slvstk":
         print "{slvstk} enable slvs 100 Ohm termination on cktk:", self.slvstk
      elif self.CurrentData == "slvsdt":
         print "{slvsdt} enable slvs 100 Ohm termination on ckdt:", self.slvsdt
      elif self.CurrentData == "slvsart":
         print "{slvsart} enable slvs 100 Ohm termination on ckart:", self.slvsart
      elif self.CurrentData == "slvstki":
         print "{slvstki} enable slvs 100 Ohm termination on cktki:", self.slvstki
      elif self.CurrentData == "slvsena":
         print "{slvsena} enable slvs 100 Ohm termination on ckena:", self.slvsena
      elif self.CurrentData == "slvs6b":
         print "{slvs6b} enable slvs 100 Ohm termination on ck6b:", self.slvs6b
      elif self.CurrentData == "sL0enaV":
         print "{sL0enaV} disable mixed signal functions when L0 enabled:", self.sL0enaV
      elif self.CurrentData == "slh":
         print "{slh} :", self.slh
      elif self.CurrentData == "slxh":
         print "{slxh} :", self.slxh
      elif self.CurrentData == "stgc":
         print "{stgc} :", self.stgc
      elif self.CurrentData == "nu":
         print "{nu} :", self.nu
      elif self.CurrentData == "reset":
         print "{reset} :", self.reset
      elif self.CurrentData == "sc":
         print "{sc} large sensor capacitance mode ([0] <~200 pF , [1] >~200 pF ):", self.sc
      elif self.CurrentData == "sl":
         print "{sl} leakage current disable [0=enabled]:", self.sl
      elif self.CurrentData == "st":
         print "{st} 300 fF test capacitor [1=enabled]:", self.st
      elif self.CurrentData == "sth":
         print "{sth} multiplies test capacitor by 10:", self.sth
      elif self.CurrentData == "sm":
         print "{sm} mask enable [1=enabled]:", self.sm
      elif self.CurrentData == "smx":
         print "{smx} channel monitor mode ( [0] analog output, [1]trimmed threshold))):", self.smx
      elif self.CurrentData == "sd":
         print "{sd} trim threshold DAC, 1 mV step ([0:0] trim 0 V ,[1:1] trim -29 mV ):", self.sd
      elif self.CurrentData == "sz10b":
         print "{sz10b}  10-bit ADC offset subtraction:", self.sz10b
      elif self.CurrentData == "sz8b":
         print "{sz8b}  8-bit ADC offset subtraction:", self.sz8b
      elif self.CurrentData == "sz6b":
         print "{sz8b}  6-bit ADC offset subtraction:", self.sz6b
      self.CurrentData = ""

   # Call when a character is read
   def characters(self, content):
      if self.CurrentData == "sp":
         self.sp = content
      elif self.CurrentData == "sdpk":
         self.sdpk = content
      elif self.CurrentData == "sbmx":
         self.sbmx = content
      elif self.CurrentData == "sbft":
         self.sbft = content
      elif self.CurrentData == "sbfp":
         self.sbfp = content
      elif self.CurrentData == "sbfm":
         self.sbfm = content
      elif self.CurrentData == "slg":
         self.slg = content
      elif self.CurrentData == "smch":
         self.smch = content
      elif self.CurrentData == "scmx":
         self.scmx = content
      elif self.CurrentData == "sfa":
         self.sfa = content
      elif self.CurrentData == "sfam":
         self.sfam = content
      elif self.CurrentData == "spt":
         self.spt = content
      elif self.CurrentData == "sfm":
         self.sfm = content
      elif self.CurrentData == "sg":
         self.sg = content
      elif self.CurrentData == "sng":
         self.sng = content
      elif self.CurrentData == "stot":
         self.stot = content
      elif self.CurrentData == "sttt":
         self.sttt = content
      elif self.CurrentData == "ssh":
         self.ssh = content
      elif self.CurrentData == "stc":
         self.stc = content
      elif self.CurrentData == "sdt":
         self.sdt = content
      elif self.CurrentData == "sdp":
         self.sdp = content
      elif self.CurrentData == "sc10b":
         self.sc10b = content
      elif self.CurrentData == "sc8b":
         self.sc8b = content
      elif self.CurrentData == "sc6b":
         self.sc6b = content
      elif self.CurrentData == "s8b":
         self.s8b = content
      elif self.CurrentData == "s6b":
         self.s6b = content
      elif self.CurrentData == "s10b":
         self.s10b = content
      elif self.CurrentData == "sdcks":
         self.sdcks = content
      elif self.CurrentData == "sdcka":
         self.sdcka = content
      elif self.CurrentData == "sdck6b":
         self.sdck6b = content
      elif self.CurrentData == "sdrv":
         self.sdrv = content
      elif self.CurrentData == "stpp":
         self.stpp = content
      elif self.CurrentData == "res":
         self.res = content
      elif self.CurrentData == "slvs":
         self.slvs = content
      elif self.CurrentData == "s32":
         self.s32 = content
      elif self.CurrentData == "stcr":
         self.stcr = content
      elif self.CurrentData == "ssart":
         self.ssart = content
      elif self.CurrentData == "srec":
         self.srec = content
      elif self.CurrentData == "stlc":
         self.stlc = content
      elif self.CurrentData == "sbip":
         self.sbip = content
      elif self.CurrentData == "srat":
         self.srat = content
      elif self.CurrentData == "sfrst":
         self.sfrst = content
      elif self.CurrentData == "slvsbc":
         self.slvsbc = content
      elif self.CurrentData == "slvstp":
         self.slvstp = content
      elif self.CurrentData == "slvstk":
         self.slvstk = content
      elif self.CurrentData == "slvsdt":
         self.slvsdt = content
      elif self.CurrentData == "slvsart":
         self.slvsart = content
      elif self.CurrentData == "slvstki":
         self.slvstki = content
      elif self.CurrentData == "slvsena":
         self.slvsena = content
      elif self.CurrentData == "slvs6b":
         self.slvs6b = content
      elif self.CurrentData == "sL0enaV":
         self.sL0enaV = content
      elif self.CurrentData == "slh":
         self.slh = content
      elif self.CurrentData == "slxh":
         self.slxh = content
      elif self.CurrentData == "stgc":
         self.stgc = content
      elif self.CurrentData == "nu":
         self.nu = content
      elif self.CurrentData == "reset":
         self.reset = content
      elif self.CurrentData == "sc":
         self.sc = content
      elif self.CurrentData == "sl":
         self.sl = content
      elif self.CurrentData == "st":
         self.st = content
      elif self.CurrentData == "sth":
         self.sth = content
      elif self.CurrentData == "sm":
         self.sm = content
      elif self.CurrentData == "smx":
         self.smx = content
      elif self.CurrentData == "sd":
         self.sd = content
      elif self.CurrentData == "sz10b":
         self.sz10b = content
      elif self.CurrentData == "sz8b":
         self.sz8b = content
      elif self.CurrentData == "sz6b":
         self.sz6b = content
  
if ( __name__ == "__main__"):
   
   # create an XMLReader
   parser = xml.sax.make_parser()
   # turn off namepsaces
   parser.setFeature(xml.sax.handler.feature_namespaces, 0)

   # override the default ContextHandler
   Handler = MovieHandler()
   parser.setContentHandler( Handler )
   
   parser.parse("vmm.xml")