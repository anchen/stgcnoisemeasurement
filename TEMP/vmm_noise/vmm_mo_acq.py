#!/usr/bin/env python

#-------------------------------------------------------
# PROJECT NAME: VMM_MO_ACQ
#
# DATE: 13 April 2017
# AUTHOR: Liang Guan (liang.cuts@gmail.com)
#
# DESCRIPTION:
# This script is used to control Tek MSO56 scope for taking
# basic measurements of the signal amplitude, baseline, threshold 
# from VMM. It will automatically detect signal input and adjust 
# the time scale, vertical scale, trigger mode etc. for carrying
# out the measurement

import os
import time
import collections
import numpy as np
from tekscope_utils import *

def setup_scope(scope,scope_channel,**kwargs):

	# Define Variables

	#xscale for one division in unit:second
	xscale=100.0E-9
	xdelay_mode='ON'
	xdelay_time=100.0E-9

	#yscale for one division in unit:Volt
	yscale=0.2
        #Impedance Setting (50/1.0E6)
	term=1.0E6
	coupling='DC'
	#Vetical Offset
	offset=0.0
        #y-postion for division
	position=-4.0
        #Label mark shown
	label='\"VMM3_MO\"'

	# Initial Horizontal Setup
	scope.set_horizontal_parameters(xscale=xscale,delay_mode=xdelay_mode,delay_time=xdelay_time)
	xparams = scope.get_horizontal_parameters()
	#print "=======X Settings===========\n",'\n'.join(str(p) for p in xparams) 

	# Initial Channel(Veritical) Setup
	scope.set_channel_parameters(channel=scope_channel,option='all',scale=yscale,termination=term,\
					coupling=coupling,offset=offset,position=position,label=label)
	yparams = scope.get_channel_parameters(scope_channel)
	#print "=======Y Settings===========\n",'\n'.join(str(p) for p in yparams) 

	# Initial Trigger Setup 
	scope.setup_trigger(trigtype='Edge',channel=scope_channel,level=0.005,slope='RISE',coupling='DC',trigmode='AUTO')

def setup_scope_special(scope,**kwargs):

	# Define Variables
	xscale=200.0E-9
	xdelay_mode='OFF'
	xdelay_time=100.0E-9

	yscale=0.1
	term=1.0E6
	coupling='DC'
	offset=0.0
	position=-4.0

	# Initial Horizontal Setup
	scope.set_horizontal_parameters(xscale=xscale,delay_mode=xdelay_mode,delay_time=xdelay_time)
	xparams = scope.get_horizontal_parameters()
	#print "=======X Settings===========\n",'\n'.join(str(p) for p in xparams) 

	# Initial Channel(Veritical) Setup
	#CH1
	scope.set_channel_parameters(channel=1,option='all',scale=1.0,termination=50.0,\
					coupling='DC',offset=0.0,position=2.0,label="\"Coincidence\"")
	#yparams = scope.get_channel_parameters(scope_channel)
	#CH2
	scope.set_channel_parameters(channel=2,option='all',scale=0.05,termination=1.0E6,\
					coupling='AC',offset=0.0,position=-4.0,label='\"sTGC_Pad\"')
	#CH3
	scope.set_channel_parameters(channel=3,option='all',scale=1.0,termination=50.0,\
					coupling='DC',offset=0.0,position=-1.0,label='\"SCINT_x\"')
	#print "=======Y Settings===========\n",'\n'.join(str(p) for p in yparams) 

	# Initial Trigger Setup 
	scope.setup_trigger(trigtype='Edge',channel=1,level=0.8,slope='RISE',coupling='DC')

def measure_dclevel(scope,scope_channel,meas_id,**kwargs):

	yscale=None
	yposition=None
	if kwargs.get('scale') is None:	yscale=0.05
	else:	yscale=kwargs.get('scale')

	if kwargs.get('position') is None: yposition=-4.0
	else:	yposition=kwargs.get('position')

	#create measurement
	#scope.create_measurement(channel=scope_channel,meas_id=meas_id,mtype='LOW')
	scope.create_measurement(channel=scope_channel,meas_id=meas_id,mtype='MEAN')
	#print "***", scope.query_measurement_setup(meas_id)
	scope.set_channel_parameters(channel=scope_channel,coupling='DC',scale=yscale,position=yposition)
	scope.reset_measurements()
	time.sleep(0.7)

	result=scope.query_measurement_results(meas_id=meas_id,fetch='mean')
	return float(result[0][1])


def measure_vmm_pulser(scope,scope_channel,meas_id,**kwargs):

	yscale=None
	trig_level=None
	if kwargs.get('scale') is None:
		yscale=0.5
	else:
		yscale=kwargs.get('scale')

	if kwargs.get('trig_level') is None:
		trig_lvl=0.05
	else:
		trig_lvl=kwargs.get('trig_level')


	#create measurement
	scope.create_measurement(channel=scope_channel,meas_id=meas_id,mtype='Amplitude')
	print "***", scope.query_measurement_setup(meas_id)
	scope.setup_trigger(trigtype='Edge',channel=scope_channel,level=trig_lvl,slope='RISE',coupling='DC')
	scope.set_channel_parameters(channel=scope_channel,coupling='AC',scale=yscale, offset=0.0, position=-4.5)
	
	scope.reset_measurements()
	time.sleep(0.7)

	result=scope.query_measurement_results(meas_id=meas_id,fetch='mean')
	return float(result[0][1])

def is_deadchannel(scope,scope_channel,**kwargs):

	# change scope channel to AC couple, 100mV/div, vert position -4.5
	#scope.set_channel_parameters(channel=scope_channel,coupling='AC',scale=0.1,position=-4.5)


	# create a "maximum" measurement
	scope.create_measurement(channel=1,meas_id=1,mtype='AMPlITUDE')
	scope.create_measurement(channel=2,meas_id=2,mtype='AMPlITUDE')
	scope.create_measurement(channel=3,meas_id=3,mtype='AMPlITUDE')
	scope.create_measurement(channel=4,meas_id=4,mtype='AMPlITUDE')
	#print "***", scope.query_measurement_setup(meas_id)

	# adjust trigger and scope scales
	yscale=0.1
	scope.setup_trigger(trigtype='Edge',channel=1,trigmode='Auto',level=0.20,slope='RISE',coupling='DC')
	time.sleep(0.8)
	scope.set_horizontal_parameters(xscale=100.0E-9)
	scope.set_channel_parameters(channel=1,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	scope.set_channel_parameters(channel=2,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	scope.set_channel_parameters(channel=3,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	scope.set_channel_parameters(channel=4,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	
	# adjust trigger (default trigger on channel 1)
	scope.setup_trigger(trigtype='Edge',channel=1,trigmode='Auto',level=0.25,slope='RISE',coupling='DC')

	# reset measurement
	scope.reset_measurements()
	time.sleep(0.7)

	result = []

	query_meas_0 = scope.query_measurement_results(meas_id=1,fetch='mean')
	query_meas_1 = scope.query_measurement_results(meas_id=2,fetch='mean')
	query_meas_2 = scope.query_measurement_results(meas_id=3,fetch='mean')
	query_meas_3 = scope.query_measurement_results(meas_id=4,fetch='mean')	
	# switch trigger channel to avoid triggering on dead channel and make false judement to other channels
	# --switch to triggering on channel 2 if all channels show low pulse amplitude (not trigggered)
 	if (float(query_meas_0[0][1])<0.09) and (float(query_meas_1[0][1])<0.09) and (float(query_meas_2[0][1])<0.09) and (float(query_meas_3[0][1])<0.09):
		del result[:]
		scope.setup_trigger(trigtype='Edge',channel=2,trigmode='Auto',level=0.22,slope='RISE',coupling='DC')
		time.sleep(1.0)
		query_meas_0 = scope.query_measurement_results(meas_id=1,fetch='mean')
		query_meas_1 = scope.query_measurement_results(meas_id=2,fetch='mean')
		query_meas_2 = scope.query_measurement_results(meas_id=3,fetch='mean')
		query_meas_3 = scope.query_measurement_results(meas_id=4,fetch='mean')
		# -- switch to triggering on channel 3
 		if (float(query_meas_0[0][1])<0.09) and (float(query_meas_1[0][1])<0.09) and (float(query_meas_2[0][1])<0.09) and (float(query_meas_3[0][1])<0.09):
			del result[:]
			scope.setup_trigger(trigtype='Edge',channel=3,trigmode='Auto',level=0.24,slope='RISE',coupling='DC')
			time.sleep(1.0)
			query_meas_0 = scope.query_measurement_results(meas_id=1,fetch='mean')
			query_meas_1 = scope.query_measurement_results(meas_id=2,fetch='mean')
			query_meas_2 = scope.query_measurement_results(meas_id=3,fetch='mean')
			query_meas_3 = scope.query_measurement_results(meas_id=4,fetch='mean')
			# -- switch to triggering on channnel 4
 			if (float(query_meas_0[0][1])<0.09) and (float(query_meas_1[0][1])<0.09) and (float(query_meas_2[0][1])<0.09) and (float(query_meas_3[0][1])<0.09):
				del result[:]
				scope.setup_trigger(trigtype='Edge',channel=4,trigmode='Auto',level=0.26,slope='RISE',coupling='DC')
				time.sleep(1.0)
				query_meas_0 = scope.query_measurement_results(meas_id=1,fetch='mean')
				query_meas_1 = scope.query_measurement_results(meas_id=2,fetch='mean')
				query_meas_2 = scope.query_measurement_results(meas_id=3,fetch='mean')
				query_meas_3 = scope.query_measurement_results(meas_id=4,fetch='mean')
				# -- all four channels dead (can not triggered) return results anyway ...
				result.append(float(query_meas_0[0][1])*1000.0)
				result.append(float(query_meas_1[0][1])*1000.0)
				result.append(float(query_meas_2[0][1])*1000.0)
				result.append(float(query_meas_3[0][1])*1000.0)
			else:
				result.append(float(query_meas_0[0][1])*1000.0)
				result.append(float(query_meas_1[0][1])*1000.0)
				result.append(float(query_meas_2[0][1])*1000.0)
				result.append(float(query_meas_3[0][1])*1000.0)
		else:
			result.append(float(query_meas_0[0][1])*1000.0)
			result.append(float(query_meas_1[0][1])*1000.0)
			result.append(float(query_meas_2[0][1])*1000.0)
			result.append(float(query_meas_3[0][1])*1000.0)
	else:
		result.append(float(query_meas_0[0][1])*1000.0)
		result.append(float(query_meas_1[0][1])*1000.0)
		result.append(float(query_meas_2[0][1])*1000.0)
		result.append(float(query_meas_3[0][1])*1000.0)

	if float(query_meas_0[0][1]) <0.09:
		print "peak:"+str(float(query_meas_0[0][1]))+"<0.09 V."+" No pulse found. Channel DEAD"
	else:
		print "peak:"+str(float(query_meas_0[0][1]))+">0.09 V."+" Pulse found. Channel Alive"

	if float(query_meas_1[0][1]) <0.09:
		print "peak:"+str(float(query_meas_1[0][1]))+"<0.09 V."+" No pulse found. Channel DEAD"
	else:
		print "peak:"+str(float(query_meas_1[0][1]))+">0.09 V."+" Pulse found. Channel Alive"

	if float(query_meas_2[0][1]) <0.09:
		print "peak:"+str(float(query_meas_2[0][1]))+"<0.09 V."+" No pulse found. Channel DEAD"
	else:
		print "peak:"+str(float(query_meas_2[0][1]))+">0.09 V."+" Pulse found. Channel Alive"

	if float(query_meas_3[0][1]) <0.09:
		print "peak:"+str(float(query_meas_3[0][1]))+"<0.09 V."+" No pulse found. Channel DEAD"
	else:
		print "peak:"+str(float(query_meas_3[0][1]))+">0.09 V."+" Pulse found. Channel Alive"
	
	return result




def measure_amplitude(scope,scope_channel,meas_id,**kwargs):

	#create measurement
	scope.create_measurement(channel=scope_channel,meas_id=meas_id,mtype='AMPLitude')
	#print "***", scope.query_measurement_setup(meas_id)

	#change scope channel to AC couple
	scope.set_channel_parameters(channel=scope_channel,coupling='AC',scale=1.0)
	#yparams = scope.get_channel_parameters(scope_channel)
	#print "=======Y Settings===========\n",'\n'.join(str(p) for p in yparams) 

	# adjust Vertical Range and trigger level 
	it=0
	step=0.01
	yscale=1.0
	while True: # -- clear event register errors
		time.sleep(0.3)
		reply=scope.detect_error()
		if (reply[0]=='0'):
			print "event buffer cleared"
			break

	#-- best initial guess	
	rslt=scope.query_measurement_results(meas_id=1,fetch='mean')[0][1]
	print "initial measured amplitude is",rslt
	yscale=float(rslt)/5.0
	print "I think the scale should be ",yscale
	time.sleep(0.1)
	scope.set_channel_parameters(channel=scope_channel,scale=yscale)
	scope.setup_trigger(trigtype='Edge',channel=scope_channel,trigmode='Normal',level=float(rslt)*0.5,slope='RISE',coupling='DC')

	while True: #-- adjust range

		# making measurement to detect error
		time.sleep(0.4) # NEED TO KEEP THIS TIME TO GUARANTEE ERROR REGISTERY
		rslt=scope.query_measurement_results(meas_id=meas_id,fetch='value')
		amp = float(rslt[0][1])
		print 'it:',it,'amplitude:',amp,'scale:',yscale,'fullrange:',yscale*10
		#time.sleep(1.0) # NEED TO KEEP THIS TIME TO GUARANTEE ERROR REGISTERY
		err=scope.detect_error()
		#print err

		# check if the range is appropriate
		if err[0]=='0'and amp!=9.91e+37 and amp > yscale*6.0:
			print '----amp:',str(amp),' is greater than 50% of fullrange:',str(yscale*5.0)
			print "adjustment finalized!"
			break

		elif err[0]!='0' or amp==9.91e+37 or amp>yscale*8.5: # positive cropping
			yscale = yscale + step
			scope.set_channel_parameters(channel=scope_channel,scale=yscale)
			scope.setup_trigger(trigmode='Edge',channel=scope_channel,level=amp*0.5,slope='RISE',coupling='DC')
			print "yscale increased to:",str(yscale)

		elif amp < yscale*6.0: # range too large, deduce y-scale
			if (yscale-step)<0.020:
			#if (yscale-step)<0.010:
				print "approaching minium V/div. can not adjust anymore!"
				break			
			else:
				yscale = yscale - step
				print "yscale decreased to:",str(yscale)
				scope.set_channel_parameters(channel=scope_channel,scale=yscale)
				scope.setup_trigger(trigtype='Edge',channel=scope_channel,trigmode='Normal',level=amp*0.5,slope='RISE',coupling='DC')
		it=it+1

	# reset measurement statisics and get final measurement value
	scope.reset_measurements()
	time.sleep(0.2)
	for x in range (1):
		time.sleep(1)
		#print x
	result=scope.query_measurement_results(meas_id=meas_id,fetch='mean')
	if kwargs.get('save_screen')=='yes' and kwargs.get('filename') is not None:
		scope.save_screen(filename=kwargs.get('filename'))
		#bashcmd='display '+kwargs.get('filename')
		#os.system(bashcmd)

	return float(result[0][1])


def measure_noise_rms(scope,scope_channel,meas_id,**kwargs):
#def measure_noise_rms(scope,scope_channel,**kwargs):

	yscale=None
	trig_level=None
	if kwargs.get('scale') is None:
		yscale=0.005
	else:
		yscale=kwargs.get('scale')

	if kwargs.get('trig_level') is None:
		trig_lvl=0.001
	else:
		trig_lvl=kwargs.get('trig_level')


	#create measurement
	scope.create_measurement(channel=1,meas_id=1,mtype='ACRMS')
	scope.create_measurement(channel=2,meas_id=2,mtype='ACRMS')
	scope.create_measurement(channel=3,meas_id=3,mtype='ACRMS')
	scope.create_measurement(channel=4,meas_id=4,mtype='ACRMS')
	#print "***", scope.query_measurement_setup(meas_id)
	scope.setup_trigger(trigtype='Edge',channel=2,level=trig_lvl,slope='RISE',coupling='DC')
	scope.set_horizontal_parameters(xscale=20.0E-6)
	scope.set_channel_parameters(channel=1,coupling='AC',termination='1.0E6',scale=yscale, offset=0.0, position=0.0)
	scope.set_channel_parameters(channel=2,coupling='AC',termination='1.0E6',scale=yscale, offset=0.0, position=0.0)
	scope.set_channel_parameters(channel=3,coupling='AC',termination='1.0E6',scale=yscale, offset=0.0, position=0.0)
	scope.set_channel_parameters(channel=4,coupling='AC',termination='1.0E6',scale=yscale, offset=0.0, position=0.0)
	
	scope.reset_measurements()
	time.sleep(1.5)

	'''
	result=scope.query_measurement_results(meas_id=meas_id,fetch='mean')
	return float(result[0][1])*1000.0
	'''
	
	result = []
	x=0
	
	result_0 = scope.query_measurement_results(meas_id=1,fetch='mean')
	print result_0
	result_1 = scope.query_measurement_results(meas_id=2,fetch='mean')
	print result_1
	result_2 = scope.query_measurement_results(meas_id=3,fetch='mean')
	print result_2
	result_3 = scope.query_measurement_results(meas_id=4,fetch='mean')
	print result_3

	result.append(float(result_0[0][1])*1000.0)
	result.append(float(result_1[0][1])*1000.0)
	result.append(float(result_2[0][1])*1000.0)
	result.append(float(result_3[0][1])*1000.0)

	return result




def measure_dc_mean(scope,scope_channel,meas_id,**kwargs):

	yscale=None
	trig_level=None
	if kwargs.get('scale') is None:
		yscale=0.050
	else:
		yscale=kwargs.get('scale')

	if kwargs.get('trig_level') is None:
		trig_lvl=0.001
	else:
		trig_lvl=kwargs.get('trig_level')

	#create measurement
	scope.create_measurement(channel=1,meas_id=1,mtype='MEAN')
	scope.create_measurement(channel=2,meas_id=2,mtype='MEAN')
	scope.create_measurement(channel=3,meas_id=3,mtype='MEAN')
	scope.create_measurement(channel=4,meas_id=4,mtype='MEAN')
	#print "***", scope.query_measurement_setup(meas_id)
	scope.setup_trigger(trigtype='Edge',channel=2,level=trig_lvl,slope='RISE',coupling='DC')
	scope.set_horizontal_parameters(xscale=20.0E-6)
	scope.set_channel_parameters(channel=1,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	scope.set_channel_parameters(channel=2,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	scope.set_channel_parameters(channel=3,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	scope.set_channel_parameters(channel=4,coupling='DC',termination='1.0E6',scale=yscale, offset=0.0, position=-4.0)
	
	scope.reset_measurements()
	time.sleep(1.3)
	
	result = []
	
	result_0 = scope.query_measurement_results(meas_id=1,fetch='mean')
	print result_0
	result_1 = scope.query_measurement_results(meas_id=2,fetch='mean')
	print result_1
	result_2 = scope.query_measurement_results(meas_id=3,fetch='mean')
	print result_2
	result_3 = scope.query_measurement_results(meas_id=4,fetch='mean')
	print result_3

	result.append(float(result_0[0][1])*1000.0)
	result.append(float(result_1[0][1])*1000.0)
	result.append(float(result_2[0][1])*1000.0)
	result.append(float(result_3[0][1])*1000.0)

	return result
