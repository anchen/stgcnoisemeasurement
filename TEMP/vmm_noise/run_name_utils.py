#############################################
# Module name: run_name_utils.py 
# Description utils to construct new run names 
# Author: Liang Guan
##############################################
import os
from datetime import date

def query_date():
	today = date.today()
	return today


def search_last_run_number(**kwargs):

	fdir=''
	files=[]

	if kwargs.get('fdir') is None: fdir=fdir
	else:   fdir="./data/"

	last_run_num=0
	for root,dirs,files in os.walk(fdir):
		for filename in files:
			split_file_name_list=filename.split("_")
			if (len(split_file_name_list)<3):
				print "filename_too_short",str(filename)
				pass
			else:
				if (split_file_name_list[2].isdigit()==True):
					current_run_num=int(split_file_name_list[2])
					if (current_run_num>last_run_num):
						last_run_num=current_run_num	
				

	print "the last run number is:"+str(last_run_num)
	return last_run_num

if __name__ == "__main__":
	lr=search_last_run_number(fdir='./data/')
	print "function return:",lr
