#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import unicode_literals
import os
import psutil

from datetime import date
from run_name_utils import *
from tekscope_utils import *
from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket

xm=XmlModifier()
sp=SendPacket()


#######################################################################################################
# Simply instructions
# I. introduction to submodules:
# 1) tekscope_untils.py: contains the core setup_scope(scope,scope_channel,**kwargs) class of commands to control 
#			  			 tektronix scopes. Includes basic functions to adjust vertical/horizontal scales, trigger, 
#						 get current settings etc.
#
# 2) vmm_mo_acq.py: contains a class of functions to adjust the scope to take amplitude, baseline,
#		    		or noise RMS measurement from VMM MO output 
#
# 3) vmm_measlib.py: contains classes, funnctions with several loops to iterate over vmm channels
#		     		 for vmm_mo measurements. It also saves data into a text file
#
#II. how to use the script:
# 1) Find IP address of the scope: the scope needs to be connected the same network where the control PC 
# is connected. Find out the current ip address of the scope by pushing "utility" button on scope buttom  
# then utility page (I/O)-> Ethernet network settings -> Change instrument settings.
# 
# 2) Change IP, file name, scope_chan:
# Change the IP address accordingly in the is_dead_channel.py and change the file_name as well to save your new 
# measurements. Note default file write setting is "a", which is append. If one wanta to overwrite the file 
# of the smae name, change "a" to "w" in the f=open(filename,'a'). 
# 
# 3) Change board_id, vmm_id:
# One needs to modify the target_info.py to update the FEB board_id and VMM_id paramters.
# Note board_id starts from 0. board_id 0 corresponding to J1 on mSAS_FMC board. vmm_id is a bit mask.
# Therefore, vmm_id = 6 (binary is 00000110) refers to the configuration of both second and third VMM
# on the FEB.
#
# 4) execute noise.py
# Finally, you should be able to execute the script to perform some basic measurements over all VMM channels
# using noise.py. However, one should be aware that the calibration or measurement is done by first 
# sending vmm configuration bits to the VMM via KC705 and GBT-SCA and then taking scope measurements (readback results).
# vmm.xml, the vmm configuration file, will be modified during the run. Please save another copy as a backup 
# in case the run needs to be force stopped during the loop and the vmm.xml is modified in the midway. 
#=========================================


#==============
# Change Log
#=============


if __name__ == "__main__":

	#====================
	# VMM Calibraton Loop
	#====================
	start=time.time()

	#== setup scope
	scope=scope('192.168.0.100')
	scope_chan=1
	setup_scope(scope,scope_channel=1)
	setup_scope(scope,scope_channel=2)
	setup_scope(scope,scope_channel=3)
	setup_scope(scope,scope_channel=4)
	#sp.send('sca','InitSCA',target)
	#sp.send('sca_gpio','ScaGPIO',target)
	#sp.send('sca_adc','ScaADC',target)
	#sp.send('vmm','CfgASIC',target)

	#== loops to measure mo output and do calibration via scope
	
	DATAFOLDERDIR='./data/'
	FEB = 'sFEB'
	SCAID= 'xxx'
	OPTION=4
	if(OPTION==1):
		GLOBAL_VMM_GAIN=1
		GLOBAL_VMM_PT=25
	elif(OPTION==2):
		GLOBAL_VMM_GAIN=1
		GLOBAL_VMM_PT=50
	elif(OPTION==3):
		GLOBAL_VMM_GAIN=3
		GLOBAL_VMM_PT=25
	elif(OPTION==4):
		GLOBAL_VMM_GAIN=3
		GLOBAL_VMM_PT=50


	RUN_TYPE='noise'
	VMM_CONNECTED='0_1_2_3'
	#VMM_CONNECTED='4_5_6_7'
	NXT_RUN_NUM=search_last_run_number(fdir=DATAFOLDERDIR)+1
	today=date.today()

	file_name=DATAFOLDERDIR+"run_"+RUN_TYPE+"_"+str(NXT_RUN_NUM)+"_"+str(today)+"_"+str(FEB)+"_"+str(SCAID)+"_"+str(GLOBAL_VMM_GAIN)+'_'+str(GLOBAL_VMM_PT)+"_P"+"_VMM_"+VMM_CONNECTED+".dat"
	f=open(file_name,'a')
	#f.close()

	noise_level_scan(scope,file_name,vmm_pt=GLOBAL_VMM_PT,vmm_gain=GLOBAL_VMM_GAIN)
	

	stop=time.time()
	total_run_time=stop-start
	print "RUN finished in "+str(total_run_time)+" seconds"
	

 	
