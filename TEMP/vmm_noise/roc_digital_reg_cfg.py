#!/usr/bin/env python

import socket
import binascii
from time import sleep
from target_info import *

cmd_dict={"InitSCA":0x0100,
	  "CfgASIC":0x0200,
	  "RstASIC":0x0300,
    	  "DoNothing":0xFFFF}

class SendPacket():

	def __init__(self):
		self.host_ip="192.168.0.16"
		self.host_port=7201
		self.general_packet_header= binascii.a2b_hex('decafbad') 

	def send(self,chip,cmd_string,target):
		# First Cfg Word [28:26]=board id [25:24]=aisc type [23:16]=VMMID Bit Mask [15:0] CMD
		idWord_bin = (bin(target(chip).board_id)[2:].zfill(3))+ \
				(bin(target(chip).asic_type)[2:].zfill(2))+ \
				(bin(target(chip).asic_id)[2:].zfill(8))

		idWord_hex = hex(int(idWord_bin,2))[2:].zfill(4)
		cmd_hex = hex(cmd_dict[cmd_string])[2:].zfill(4)

		command_word_32b = binascii.a2b_hex(idWord_hex+cmd_hex)

		print "~~~~~~",binascii.b2a_hex(command_word_32b)
		payload =binascii.a2b_hex(target(chip).cmd_content)
		
		message = self.general_packet_header + command_word_32b+payload
		roc_digital_head='decafbad0e000200' # for pFEBv2.3 J2

                #roc reg 0-63, 8 bit data
                                
                dreg0='C5' # First_L1_ID Parity(should be even 1'b1) ROC_ID_FOR_NULL_EVENTS (6'bxxxxxx) 
                dreg1='ff' # speed of subROC 0xFF means 80Mbps for all four lanes
                dreg2='04' # sROC 0 connected to VMM capture  (VMMA on pFEB) 
                dreg3='08' # sROC 1 connected to VMM capture  (VMMB on pFEB)
                dreg4='01' # sROC 2 connected to VMM capture  (VMMC on pFEB)
                dreg5='00' # sROC 3 connected to VMM capture
                dreg6='ff' # sending of EOP and NULL event headers enabled for all sROCs
                dreg7='BF' # BYPASS TIMEOUT TTC STart bit [1:0] SROC ENA [3:0]
                dreg8='ff' # All VMM Capture Enabled
                dreg9='ff' # Timeout threshold one count is 50 ns
                dreg10='84' # TX_CSEL bit mask [3:0] BCID OFFSET [11:8] 
                dreg11='85'
                dreg12='0D'
                dreg13='E5'
                dreg14='ff'
                dreg15='00'
                dreg16='00'
                dreg17='00'
                dreg18='00'
                dreg19='00'
                dreg20='ff'
                dreg21='07'
                dreg22='6C'
                dreg23='07'
                dreg24='08'
                dreg25='00'
                dreg26='00'
                dreg27='00'
                dreg28='00'
                dreg29='00'
                dreg30='00'
                dreg31='ff' # max. number of back-to-back packets after which two commas will be inserted
                dreg32='00'
                dreg33='00'
                dreg34='00'
                dreg35='00'
                dreg36='00'
                dreg37='00'
                dreg38='00'
                dreg39='00'
                dreg40='00'
                dreg41='00'
                dreg42='00'
                dreg43='00'
                dreg44='00'
                dreg45='00'
                dreg46='00'
                dreg47='00'
                dreg48='00'
                dreg49='00'
                dreg50='00'
                dreg51='00'
                dreg52='00'
                dreg53='00'
                dreg54='00'
                dreg55='00'
                dreg56='00'
                dreg57='00'
                dreg58='00'
                dreg59='00'
                dreg60='00'
                dreg61='00'
                dreg62='00'
                dreg63='00'

                rocdigconfigbit = ''.join([roc_digital_head,dreg0,dreg1,dreg2,dreg3,dreg4,dreg5,dreg6,dreg7,dreg8,dreg9,dreg10,dreg11,dreg12,dreg13,dreg14,dreg15,dreg16,dreg17,dreg18,dreg19,dreg20,dreg21,dreg22,dreg23,dreg24,dreg25,dreg26,dreg27,dreg28,dreg29,dreg30,dreg31,dreg32,dreg33,dreg34,dreg35,dreg36,dreg37,dreg38,dreg39,dreg40,dreg41,dreg42,dreg43,dreg44,dreg45,dreg46,dreg47,dreg48,dreg49,dreg50,dreg51,dreg52,dreg53,dreg54,dreg55,dreg56,dreg57,dreg58,dreg59,dreg60,dreg61,dreg62,dreg63])

                #rocanaconfigbit=rocanaconfigbit.replace(' ','')

                #rocanaconfigbit=roc_analog_head+reg0+reg1
                mes_vec = [ binascii.a2b_hex(rocdigconfigbit)]
		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sock.bind((self.host_ip, self.host_port))

		
		for i in range(1):
			#print "My IP:     " + self.host_ip
			#print "My Port:   " + str(self.host_port)
			#print "Dest IP:   " + target(chip).guest_ip
			#print "Dest Port: " + str(target(chip).guest_port)
			print "============================"
			print "Payload:   " + binascii.b2a_hex(mes_vec[i])
			sock.sendto(mes_vec[i], (target(chip).guest_ip, target(chip).guest_port))
			sleep(0.2)


if __name__ == "__main__":
	sp=SendPacket()
	#sp.send('','InitSCA',target)
	#sp.send('vmm','CfgASIC',target)
	sp.send('roc','CfgASIC',target)
	#sp.send('vmm','RstASIC',target)
	#sp.send('tds','RstASIC',target)



