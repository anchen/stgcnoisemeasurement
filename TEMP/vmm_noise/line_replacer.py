# This file is used to modify pFEB/sFEB in "target_info.py" by tk GUI

import re

#filename = "target_info.py"

#with open("target_info.py", 'r+') as f:
#    text = f.read()
#    text = re.sub('\t\tself.board_type = 1', '\t\tself.board_type = 0', text)
#    f.seek(0)
#    f.write(text)
#    f.truncate()
#    f.close()


#print '\t\tself.board_type = 0'

def line_replacer_board_type_0():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_type = 1', '\t\tself.board_type = 0', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def line_replacer_board_type_1():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_type = 0', '\t\tself.board_type = 1', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()
