import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

'''
if len(sys.argv)!=2:
  print("Usage: python TEST.py targetDir")
  sys.exit(0)

filelist = glob( sys.argv[1] + "/*.txt")
'''
def Noise_plot(file_name):
	#ARRAY_Data = np.loadtxt('0_pFEB009_VMM3_xxx.dat',dtype='double')
	ARRAY_Data = np.loadtxt(file_name,dtype='double')
	Total_No = len(ARRAY_Data)
	print Total_No

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	NOISE_DATA_1 = []
	NOISE_DATA_2 = []
	NOISE_DATA_3 = []
	NOISE_DATA_4 = []
	
	#sweep though all the list, group channel's data
	
	for y in range(0,64):
		NOISE_DATA_1.append(ARRAY_Data[y][1])
		y += 1

	for y in range(0,64):
		NOISE_DATA_2.append(ARRAY_Data[y][2])
		y += 1
	
	for y in range(0,64):
		NOISE_DATA_3.append(ARRAY_Data[y][3])
		y += 1

	for y in range(0,64):
		NOISE_DATA_4.append(ARRAY_Data[y][4])
		y += 1

	print NOISE_DATA_1
	print NOISE_DATA_2
	print NOISE_DATA_3
	print NOISE_DATA_4

	#Plot data on graphs

	counter = []

	for y in range(0,length):
		counter.append(y)
		y += 1

	plt.plot(counter, NOISE_DATA_1,'kx',markersize = 5)
	plt.plot(counter, NOISE_DATA_2,'bx',markersize = 5)
	plt.plot(counter, NOISE_DATA_3,'gx',markersize = 5)
	plt.plot(counter, NOISE_DATA_4,'mx',markersize = 5)
	plt.legend(('VMMA', 'VMMB', 'VMMC', 'VMMD'),loc='upper right')
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.show()

