### Saved Raw Data Title format ###
# RunID+TestDate + p/sFEB+ SCA ID + Wedge Serial + Quad info +Layer + Gain + PT + polarity + VMMtoscope mapping
# File Format: run_00002_17052019_pFEB_1045_20MNIWSAP00001_QS1P_L1_3_50_P_0_1_2_3.dat
# File Format: run_00002_17052019_pFEB_1045_00000000000000_QS0T_L0_3_50_N_2_3_4.dat (On test Bench)
# runid+_+nowdate+_+board_type_N+_+scaid+_+monthly+_+Size_ChamberNo+Position+_+Layer+_+vmm_gain_temp_t+_+vmm_pt_temp+_+board_type_N_t+_+vmmmaping+".dat"

from __future__ import absolute_import
from __future__ import unicode_literals

import os
import psutil
import glob
import time

#from Tkinter import *
import Tkinter as tk
import ttk
import ScrolledText as tkst
import tkFileDialog as filedialog
from Cham_DECODER_funct import *
from line_replacer import *
from BoardID_replacer import *
from DECODER_SCAID_funct import *
#from tekscope_utils_4034 import *
from tekscope_utils import *
from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket
from Noise_process_funct import *
from VMMconfig_replacer import *
from datetime import datetime
from roc_analog_reg_cfg import *
from roc_digital_reg_cfg import *
from sendttc import *

#Use for running shell script
import subprocess


# User selection tool for selecting target directory for plots
def c_open_target_dir():
  global target_dir
  target_dir = filedialog.askdirectory()
  logtext.insert(tk.END,"Target directory: "+target_dir+"\n\n")
  print(target_dir)
  

def show_entry_fields():
   print("First Name: %s\nLast Name: %s" % (e1.get(), e2.get()))


def set_all_config():
  global FEB_serial
  global FEB_MTF

  logtext.insert(tk.END,"Setting all configurations...\n\n")

  success_all = True
  success_all *= set_test_station()

  #comment_tab = e_comment.get()
  Cpi_Pad = e_Cpi.get()
  Rp_Strip = e_Rp.get()


  success_all *= set_FEB_wedge()

  success_all *= set_minidaq()
  
  if success_all:
    logtext.insert(tk.END,"All configurations successfully set."+"\n\n")
    runID_gen()
    show_file_name()

def set_test_station(success=True):
  global scope_type_N
  global location_type
  global location_type_T
  global shifter1
  global shifter2
  global shifter3

  scope_type_N = str((v_SCOPE_D.state())[0][0])
  var_BoardID = str((v_BID_D.state())[0][0])
  location_type = str((v_LOC_D.state())[0][0])

  if (scope_type_N == 'None'):
    logtext.insert(tk.END,"ERROR: no scope selected."+"\n")
    success *= False
  if (var_BoardID == 'None'):
    logtext.insert(tk.END,"ERROR: no MINISAS port selected."+"\n")
    success *= False
  if (location_type == 'None'):
    logtext.insert(tk.END,"ERROR: no location selected."+"\n")
    success *= False
  if success:
    location_type_T = location_type[0]
    var_BoardID = int(var_BoardID[1:])-1

    if var_BoardID == 0:
      BoardID_replacer_board_type_0()
    elif var_BoardID == 1:
      BoardID_replacer_board_type_1()
    elif var_BoardID == 2:
      BoardID_replacer_board_type_2()
    elif var_BoardID == 3:
      BoardID_replacer_board_type_3()
    elif var_BoardID == 4:
      BoardID_replacer_board_type_4()
    elif var_BoardID == 5:
      BoardID_replacer_board_type_5()
    elif var_BoardID == 6:
      BoardID_replacer_board_type_6()
    elif var_BoardID == 7:
      BoardID_replacer_board_type_7()

    shifter1 = e_shifter1.get()
    shifter2 = e_shifter2.get()
    shifter3 = e_shifter3.get()

    print(scope_type_N)
    print(location_type)
    print(location_type_T)
    print(var_BoardID)
    print(shifter1)
    print(shifter2)
    print(shifter3)

    logtext.insert(tk.END,"Scope selected: "+scope_type_N+"\n")
    logtext.insert(tk.END,"MINISAS port selected: J"+str(var_BoardID+1)+"\n")
    logtext.insert(tk.END,"Location selected: "+location_type+"\n")
    if (shifter1 != ''):
      logtext.insert(tk.END,"Shifter 1: "+shifter1+"\n")
      if (shifter2 != ''):
        logtext.insert(tk.END,"Shifter 2: "+shifter2+"\n")
        if (shifter3 != ''):
          logtext.insert(tk.END,"Shifter 3: "+shifter3+"\n")
    else:
      logtext.insert(tk.END,"WARNING: no shifters listed.\n")
      
    success = True

  logtext.insert(tk.END,"\n")
  return success


def set_FEB_wedge(success=True):
  global FEB_serial
  global FEB_MTF

  FEB_serial = e_FEB_serial.get()
  FEB_MTF = e_FEB_MTF.get()

  if (FEB_serial ==''):
    logtext.insert(tk.END,"ERROR: missing FEB serial."+"\n")
    success *= False
  if (FEB_MTF ==''):
    logtext.insert(tk.END,"ERROR: missing FEB MTF."+"\n")
    success *= False
  if success:
    print("FEB_serial: %s" % (FEB_serial))
    print("FEB_MTF: %s" % (FEB_MTF))

    logtext.insert(tk.END,"FEB serial: "+FEB_serial+"\n")
    logtext.insert(tk.END,"FEB MTF: "+FEB_MTF+"\n")

    success = True

  success *= id_decode(ents)
  success *= show_Wedge_LOC()
  
  logtext.insert(tk.END,"\n")
  return success


def set_minidaq(success=True):
  success *= set_vmm()
  success *= allstates_map()

  logtext.insert(tk.END,"\n")
  return success


def set_vmm(success=True):
  global vmm_gain_temp
  global vmm_gain_temp_t
  global vmm_pt_temp
  global vmm_pt_temp_N
  global vmm_POLAR_temp
  global vmm_POLAR_temp_N
  global scaid

  scaid = e3.get()
  if (scaid == ""):
    logtext.insert(tk.END,"ERROR: no SCA ID found."+"\n")
    success *= False

  vmm_gain_temp = (var_GAIN_D.state())[0][0]
  vmm_pt_temp = (var_PT_D.state())[0][0]
  vmm_POLAR_temp_N = (var_POLAR_D.state())[0][0]
  print(vmm_gain_temp)
  print(vmm_pt_temp)
  print(vmm_POLAR_temp_N)
  

  if (vmm_gain_temp == 'None'):
    logtext.insert(tk.END,"ERROR: no VMM gain selected."+"\n")
    success *= False
  if (vmm_pt_temp == 'None'):
    logtext.insert(tk.END,"ERROR: no VMM PT selected."+"\n")
    success *= False
  if (vmm_POLAR_temp_N == 'None'):
    logtext.insert(tk.END,"ERROR: no VMM polarity selected."+"\n")
    success *= False
  if success:
    vmm_gain_temp = float(vmm_gain_temp)
    vmm_gain_temp_t = str(int(vmm_gain_temp))
    vmm_pt_temp = long(vmm_pt_temp)
    vmm_pt_temp_N = str(int(vmm_pt_temp))
    vmm_POLAR_temp_N = str(vmm_POLAR_temp_N)[0]

    if (vmm_POLAR_temp_N == 'P'):
      vmm_POLAR_temp = 0
    elif (vmm_POLAR_temp_N == 'N'):
      vmm_POLAR_temp = 1
    else:
      vmm_POLAR_temp = None

    print(vmm_gain_temp)
    print(vmm_gain_temp_t)
    print(vmm_pt_temp)
    print(vmm_pt_temp_N)
    print(vmm_POLAR_temp)
    print(vmm_POLAR_temp_N)
    logtext.insert(tk.END,"VMM gain: "+vmm_gain_temp_t+" mV/fC\n")
    logtext.insert(tk.END,"VMM PT: "+vmm_pt_temp_N+" ns\n")
    logtext.insert(tk.END,"VMM polarity: "+vmm_POLAR_temp_N+"\n")

  return success


def id_decode(entries, success=True):
  global Position
  global monthly
  global S_L_Wedge
  global small_or_large
  global A_C_Wedge
  global P_C_Wedge
  global Wedge_Serial_Number
  global A_or_C

  success = True

  monthly = (str(entries['*Chamber Serial*'].get()))
  if (monthly == ''):
    logtext.insert(tk.END,"ERROR: missing chamber serial."+"\n")
    success *= False
  else:
    try:
      cham_code = cham_decode(monthly)
    except:
      logtext.insert(tk.END,"ERROR: incorrect chamber serial format."+"\n")
      success *= False
    else:
      S_L_Wedge = cham_code[0]
      A_C_Wedge = cham_code[1]
      P_C_Wedge = cham_code[2]
      Wedge_Serial_Number = cham_code[3]
      entries['Large/Small'].delete(0,tk.END)
      entries['Large/Small'].insert(0, cham_code[0] )
      entries['Side A/C'].delete(0,tk.END)
      entries['Side A/C'].insert(0, cham_code[1] )
      entries['Pivot/Confirm'].delete(0,tk.END)
      entries['Pivot/Confirm'].insert(0, cham_code[2] )
      entries['Serial Number'].delete(0,tk.END)
      entries['Serial Number'].insert(0, cham_code[3] )
      print("Chamber Serial: %s" % str(monthly))

      if cham_code[2] == "Pivot":
        Position = "P"
      elif cham_code[2] == "Confirm":
        Position = "C"
      elif cham_code[2] == "Bench":
        Position = "T"

      if S_L_Wedge == "QS":
        small_or_large = "Small"
      elif S_L_Wedge == "QL":
        small_or_large = "Large"
      else:
        small_or_large = "N/A"

      if A_C_Wedge == "Side A":
        A_or_C = "A"
      elif A_C_Wedge == "Side C":
        A_or_C = "C"
      else:
        A_or_C = "N/A"

      logtext.insert(tk.END,"Wedge size: "+small_or_large+"\n")
      logtext.insert(tk.END,"Wedge side: "+A_or_C+"\n")
      logtext.insert(tk.END,"Wedge type: "+cham_code[2]+"\n")
      logtext.insert(tk.END,"Wedge serial: "+cham_code[3]+"\n")
      success = True

  return success


def makeform(root, fields, line_count=0):
   entries = {}
   for field in fields:
      lab = tk.Label(root, text=field+':',font=20)
      if ((field[0] == '*') & (field[-1] == '*')):
        ent = tk.Entry(root, highlightbackground='red')
      else:
        ent = tk.Entry(root)
      lab.grid(row=line_count, column=0)
      ent.grid(row=line_count, column=1)
      entries[field] = ent
      line_count = line_count + 1
   return entries


def show_Wedge_LOC(success=True): 
  global Wedge_LOC
  global Size_ChamberNo_Position
  global board_type_N
  global board_type_N_t
  global Layer
  global Layer_T

  code_wedge_list = []
  Wedge_LOC = e_Wedge_LOC.get()

  if (Wedge_LOC == ''):
    logtext.insert(tk.END,"ERROR: missing location on wedge."+"\n")
    success *= False
  if success:
    code_wedge_list = Wedge_LOC.split("-")
    if len(code_wedge_list) == 3:
      Size_ChamberNo_Position = code_wedge_list[0]
      if code_wedge_list[1] == "Pad":
        line_replacer_board_type_0()
        board_type_N = "pFEB"
        board_type_N_t = "P"
      elif code_wedge_list[1] == "Strip":
        line_replacer_board_type_1()
        board_type_N = "sFEB"
        board_type_N_t = "P"
      print("Measurement location: %s" % (Wedge_LOC))
      if code_wedge_list[2] == "Layer1":
        Layer = "L1"
        Layer_T = "1"
      elif code_wedge_list[2] == "Layer2":
        Layer = "L2"
        Layer_T = "2"
      elif code_wedge_list[2] == "Layer3":
        Layer = "L3"
        Layer_T = "3"
      elif code_wedge_list[2] == "Layer4":
        Layer = "L4"
        Layer_T = "4"
      else:
        Layer = "L0"
        Layer_T = "0"
    else:
      Size_ChamberNo_Position = "QS0T"
      board_type_N = "pFEB"
      board_type_N_t = "P"
      Layer = "L0"
      Layer_T = "0"

    success = True

  return success


def paraprocess_SCA():
  subprocess.call("./test_SCA.sh")


def SCAID_insert():
  global scaid
  e3.insert(0, SCAID_DECODER() )
  os.remove('test.dat')
  scaid = e3.get()


def paraprocess_ROC():
  spa.send('roc','CfgASIC',target)
  time.sleep(2)
  spd.send('roc','CfgASIC',target)
  print "ROC configuation done"


# Dropdown menu template
class Dropdown_Menu(tk.Frame):
  def __init__(self, parent=None, optionlist=[], picks=[], side=tk.LEFT, anchor='w'):
    tk.Frame.__init__(self, parent)
    self.optionlist = optionlist
    self.picks = picks
    self.side = side
    self.anchor = anchor

    self.tkvar_map = []
    self.vars = []
    for i in range(len(self.picks)):
      self.tkvar_map.append(tk.StringVar(parent))
      self.tkvar_map[i].set('None')
      popupMenu = tk.OptionMenu(self, self.tkvar_map[i], 'None', *self.optionlist)
      popupMenu.pack(side=self.side, anchor=self.anchor, expand=tk.YES)
      self.vars.append(self.tkvar_map[i])
  def state(self):
    #print(len(self.picks))
    #print([self.tkvar_map[i].get() for i in range(len(self.picks))])
    return map((lambda tkvar_map: [self.tkvar_map[i].get() for i in range(len(self.picks))]), self.vars)
  def set_option(self, settings, parent=None):
    for i in range(len(settings)):
      self.tkvar_map[i].set(settings[i])
      #popupMenu = tk.OptionMenu(self, self.tkvar_map, 'None', *self.optionlist)
      #popupMenu.pack(side=self.side, anchor=self.anchor, expand=tk.YES)
      


# Maps oscilloscope to VMM states
def allstates_map(success=True): 
  global total_scanned_channel
  global scope_channel_string
  global scope_channel_string_no
  global scope_channel_string_N

  list_vmmmap = list(map1.state()[0])
  #list_vmmmap = [i for i in list_vmmmap if not 'N' in i]
  print(list_vmmmap)
  if all(item == 'None' for item in list_vmmmap):
    logtext.insert(tk.END,"ERROR: no scope to VMM mapping provided.\n")
    success *= False
  else:
    list_scope_channel_no = []
    list_scope_channel = []
    for x in range(0,8):
      print x
      if list_vmmmap[x] != 'None':
        list_scope_channel.append(int(list_vmmmap[x]))
        list_scope_channel_no.append(x+1)

    total_scanned_channel = len(list_scope_channel_no)
    print total_scanned_channel
    print list_scope_channel_no
    print list_scope_channel

    scope_channel_string = str(list_scope_channel[0])
    scope_channel_string_no = str(list_scope_channel_no[0])
    for i in range(1,total_scanned_channel):
      scope_channel_string += ','+str(list_scope_channel[i])
      scope_channel_string_no += ','+str(list_scope_channel_no[i])

    print scope_channel_string
    print scope_channel_string_no

    list_scope_channel_N = []
    for x in range(0,len(list_scope_channel)):
      if (list_scope_channel[x] != -1):
        list_scope_channel_N.append(list_scope_channel[x])

    print list_scope_channel_N

    total_scanned_channel_N = len(list_scope_channel_N)

    scope_channel_string_N = str(list_scope_channel_N[0])
    for i in range(1,total_scanned_channel_N):
      scope_channel_string_N += '_'+str(list_scope_channel_N[i])

    print scope_channel_string_N
    logtext.insert(tk.END,"Scope to VMM mapping string: "+scope_channel_string_N+"\n")

  return success


# Display name of generated data file
def show_file_name():
  global vmm_pt_temp_N
  global vmm_gain_temp_t
  global runid
  global board_type_N
  global scaid
  global monthly
  #global Size_ChamberNo
  #global Position
  global Size_ChamberNo_Position
  global Layer
  global vmm_POLAR_temp_N
  global scope_channel_string_N
  global file_name
  global FEB_MTF
  global FEB_serial
  global location_type_T
  #print Size_ChamberNo

  # moved to set_all_config
  #FEB_serial = e_FEB_serial.get()
  #FEB_MTF = e_FEB_MTF.get()
  #print("FEB_serial: %s" % (FEB_serial))

  #if (runid != "") & (nowdate != "") & (board_type_N != "") & (scaid != "") & (monthly != "") & (Size_ChamberNo_Position != "") & (Layer != "")& (vmm_gain_temp_t != "") & (vmm_pt_temp_N != "") & (vmm_POLAR_temp_N != "") & (scope_channel_string_N != "") & (location_type_T != ""):
  file_name="run_"+runid+"_"+nowdate+"_"+board_type_N+"_"+scaid+"_"+monthly+"_"+Size_ChamberNo_Position+"_"+Layer+"_"+vmm_gain_temp_t+"_"+vmm_pt_temp_N+"_"+vmm_POLAR_temp_N+"_VMM_"+scope_channel_string_N+"_"+location_type_T+".dat"


  print file_name
  logtext.insert(tk.END,"File name: "+file_name+"\n")


# Generate a run ID
def runID_gen():
  global runid
  datfiles = []
  runid_temp = []
  for file in glob("*.dat"):
    datfiles.append(file)
  print datfiles
  if len(datfiles) == 0:
    f=open('run_00000_99999999_tFEB_00000_00000000000000_L0_0_00_T_VMM_0.dat','a')
    f.write("**This is a dummy file for further file generation**\n")
    f.close()
    runid_temp = [0]
  else:
    for x in range(0,len(datfiles)):
      data_temp = datfiles[x].split("_")
      print data_temp[1]
      runid_temp.append(int(data_temp[1]))
  print runid_temp
  print max(runid_temp)
  runid = str(int(max(runid_temp)) + 1)

  print runid
  logtext.insert(tk.END,"Run ID: "+runid+"\n")


# Noise RMS measurement for 4 VMMs
def main_noise_measurement_4():
  global vmm_pt_temp
  global vmm_gain_temp
  global runid
  global board_type_N
  global scaid
  global monthly
  #global Size_ChamberNo
  global Position
  global Layer
  global vmm_POLAR_temp
  global scope_channel_string_N
  global file_name
  global target_dir
  global S_L_Wedge
  global A_C_Wedge
  global P_C_Wedge
  global Wedge_Serial_Number
  global scope_type_N
  global FEB_serial
  global vmm_POLAR_temp_N
  global FEB_MTF
  global vmm_gain_temp_t
  global vmm_pt_temp_N
  print vmm_pt_temp
  print vmm_gain_temp
  global Size_ChamberNo_Position
  global total_scanned_channel
  global scope_channel_string
  global scope_channel_string_no
  global location_type
  global small_or_large
  global A_or_C
  global Layer_T
  print Layer
  global shifter1
  global shifter2
  global shifter3

  try:
    target_dir
  except NameError:
    target_dir = os.getcwd()
    print(target_dir)

  Size_ChamberNo_Position_list = list(Size_ChamberNo_Position)
  QType = Size_ChamberNo_Position_list[0] + Size_ChamberNo_Position_list[1]
  QIndex = Size_ChamberNo_Position_list[2]

  # now done in set_all_config() method
  comment_tab = e_comment.get()
  #shifter1 = e_shifter1.get()
  #shifter2 = e_shifter2.get()
  #shifter3 = e_shifter3.get()
  Cpi_Pad = e_Cpi.get()
  Rp_Strip = e_Rp.get()

  start=time.time()

  #== setup scope
  scope_N=scope('192.168.0.100')
  scope_chan=1
  setup_scope(scope_N,scope_channel=1)
  setup_scope(scope_N,scope_channel=2)
  setup_scope(scope_N,scope_channel=3)
  setup_scope(scope_N,scope_channel=4)
  DATAFOLDERDIR='./data/'
  full_file_name = os.path.join(target_dir,file_name)
  f=open(full_file_name,'a')

  f.write("************************\n")
  f.write("**Measurement Unix Time:"+str(int(time.time()))+"**\n")
  f.write("**Measurement Date:"+nowdate+"**\n")

  # ??? (TODO: optimize later)
  if (shifter1 != "") & (shifter2 == "") & (shifter3 == ""):
    f.write("**Measurement Shifter1:"+shifter1+"**\n")
    f.write("**Measurement Shifter2:"+""+"**\n")
    f.write("**Measurement Shifter3:"+""+"**\n")
  elif (shifter1 != "") & (shifter2 != "") & (shifter3 == ""):
    f.write("**Measurement Shifter1:"+shifter1+"**\n")
    f.write("**Measurement Shifter2:"+shifter2+"**\n")
    f.write("**Measurement Shifter3:"+""+"**\n")
  elif (shifter1 != "") & (shifter2 != "") & (shifter3 != ""):
    f.write("**Measurement Shifter1:"+shifter1+"**\n")
    f.write("**Measurement Shifter2:"+shifter2+"**\n")
    f.write("**Measurement Shifter3:"+shifter3+"**\n")
  else:
    print("No of channels")
    f.write("**Measurement Shifter1:"+""+"**\n")
    f.write("**Measurement Shifter2:"+""+"**\n")
    f.write("**Measurement Shifter3:"+""+"**\n")

  if location_type != "":
    f.write("**Measurement Location:"+location_type+"**\n")
  else:
    showerror("Error", "No measurment location inserted")

  f.write("**FEB Serial Number:"+FEB_serial+"**\n")
  f.write("**FEB MTF:"+FEB_MTF+"**\n")  
  f.write("**FEB Type:"+board_type_N+"**\n")

  if (Rp_Strip == "") & (Cpi_Pad != ""):
    f.write("**Cpi if pFEB(pF):"+Cpi_Pad+"**\n")
  elif (Rp_Strip != "") & (Cpi_Pad == ""):
    f.write("**Rpull-up if sFEB(kOhm):"+Rp_Strip+"**\n")
  else:
    print("Neither Cpi if pFEB(pF) nor Rpull-up if sFEB(kOhm) input provide. please check")

  f.write("**VMM Gain(mV/fC):"+vmm_gain_temp_t+"**\n")
  f.write("**VMM Peaking Time (ns):"+vmm_pt_temp_N+"**\n")
  f.write("**VMM SFM:"+"1"+"**\n")

  f.write("**Wedge MTF:"+monthly+"**\n")
  f.write("**Wedge Size:"+small_or_large+"**\n")
  f.write("**Wedge for ATLAS Side A or C:"+A_or_C+"**\n")
  f.write("**Wedge Type:"+P_C_Wedge+"**\n")
  f.write("**Wedge Serial Number:"+Wedge_Serial_Number+"**\n")
  f.write("**Quadruplet Type:"+QType+"**\n")
  f.write("**Quadruplet Index:"+QIndex+"**\n")
  f.write("**Quadruplet Layer:"+Layer_T+"**\n")

  f.write("**GFZ Adapter Board Type:"+"**\n")
  f.write("**Scope Type:"+ scope_type_N+"**\n")
  f.write("**Scope Channels Connected:"+str(total_scanned_channel)+"**\n")
  f.write("**Scope Channel Recorded:"+scope_channel_string_no+"**\n")
  f.write("**FEB VMM Number Mapped:"+scope_channel_string+"**\n")
  f.write("************************\n\n\n\n")
  
  if comment_tab == "":
    f.write("############COMMENT############\n")
    f.write("###\n")
    f.write("##############################\n\n\n\n")
  elif comment_tab != "":
    f.write("############COMMENT############\n")
    f.write("###"+comment_tab+"\n")
    f.write("##############################\n\n\n\n")
  else:
    print("No comment inputed, please check")       

  f.close()

  noise_level_scan_4(scope_N,file_name,vmm_pt=vmm_pt_temp,vmm_gain=vmm_gain_temp,vmm_polarity=vmm_POLAR_temp)
  logtext.insert(tk.END,"Output directory: "+target_dir+"\n")

  stop=time.time()
  total_run_time=stop-start
  print "Noise Measurement Run finished in "+str(total_run_time)+" seconds"
  logtext.insert(tk.END,"Noise Measurement Run finished in "+str(total_run_time)+" seconds.\n\n")


# Noise RMS measurement for 6 VMMs
def main_noise_measurement_6():
  global vmm_pt_temp
  global vmm_gain_temp
  global runid
  global board_type_N
  global scaid
  global monthly
  #global Size_ChamberNo
  global Position
  global Layer
  global vmm_POLAR_temp
  global scope_channel_string_N
  global file_name
  global target_dir
  global S_L_Wedge
  global A_C_Wedge
  global P_C_Wedge
  global Wedge_Serial_Number
  global scope_type_N
  global FEB_serial
  global vmm_POLAR_temp_N
  global FEB_MTF
  global vmm_gain_temp_t
  global vmm_pt_temp_N
  print vmm_pt_temp
  print vmm_gain_temp
  global Size_ChamberNo_Position
  global Cpi_Pad
  global Rp_Strip
  global shifter1
  global shifter2
  global shifter3
  global total_scanned_channel
  global scope_channel_string
  global scope_channel_string_no
  global location_type
  global small_or_large
  global Layer_T
  print Layer
  start=time.time()

  try:
    target_dir
  except NameError:
    target_dir = os.getcwd()
    print(target_dir)

  #== setup scope
  scope_N=scope('192.168.0.100')
  scope_chan=1
  setup_scope(scope_N,scope_channel=1)
  setup_scope(scope_N,scope_channel=2)
  setup_scope(scope_N,scope_channel=3)
  setup_scope(scope_N,scope_channel=4)
  setup_scope(scope_N,scope_channel=5)
  setup_scope(scope_N,scope_channel=6)
  DATAFOLDERDIR='./data/'
  full_file_name = os.path.join(target_dir,file_name)
  f=open(full_file_name,'a')


  f.write("************************\n")
  f.write("**Measurement Date:"+nowdate+"**\n")

  if (shifter1 != "") & (shifter2 == "") & (shifter3 == ""):
    f.write("**Measurement Shifter1:"+shifter1+"**\n")
    f.write("**Measurement Shifter2:"+" "+"**\n")
    f.write("**Measurement Shifter3:"+" "+"**\n")
  elif (shifter1 != "") & (shifter2 != "") & (shifter3 == ""):
    f.write("**Measurement Shifter1:"+shifter1+"**\n")
    f.write("**Measurement Shifter2:"+shifter2+"**\n")
    f.write("**Measurement Shifter3:"+" "+"**\n")
  elif (shifter1 != "") & (shifter2 != "") & (shifter3 != ""):
    f.write("**Measurement Shifter1:"+shifter1+"**\n")
    f.write("**Measurement Shifter2:"+shifter2+"**\n")
    f.write("**Measurement Shifter3:"+shifter3+"**\n")
  else:
    print("No of channels")
    f.write("**Measurement Shifter1:"+" "+"**\n")
    f.write("**Measurement Shifter2:"+" "+"**\n")
    f.write("**Measurement Shifter3:"+" "+"**\n")

  f.write("**Measurement Location:"+location_type+"**\n")
  f.write("**FEB Serial Number:"+FEB_serial+"**\n")
  f.write("**FEB MTF:"+FEB_MTF+"**\n")  
  f.write("**FEB Type:"+board_type_N+"**\n")

  if (Rp_Strip == "") & (Cpi_Pad != ""):
    f.write("**Cpi if pFEB(pF):"+Cpi_Pad+"**\n")
  elif (Rp_Strip != "") & (Cpi_Pad == ""):
    f.write("**Rpull-up if sFEB(kOhm):"+Rp_Strip+"**\n")
  else:
    print("Neither Cpi if pFEB(pF) nor Rpull-up if sFEB(kOhm) inputed, please check")

  f.write("**VMM Gain(mV/fC):"+vmm_gain_temp_t+"**\n")
  f.write("**VMM Peaking Time(ns):"+vmm_pt_temp_N+"**\n")
  f.write("**VMM SFM:"+"1"+"**\n")

  f.write("**Wedge MTF:"+monthly+"**\n")
  f.write("**Wedge Size:"+small_or_large+"**\n")
  f.write("**Wedge for Side ATLAS A or C:"+A_C_Wedge+"**\n")
  f.write("**Wedge Type:"+P_C_Wedge+"**\n")
  f.write("**Wedge Serial Number:"+Wedge_Serial_Number+"**\n")
  f.write("**Quadruplet Type:"+Size_ChamberNo_Position+"**\n")
  f.write("**Quadruplet Layer:"+Layer_T+"**\n")

  f.write("**GFZ Adapter Board Type:"+' '+"**\n")
  f.write("**Scope Type:"+ scope_type_N+" **\n")
  f.write("**Number of Scope Channel(s) Connected:"+str(total_scanned_channel)+"**\n")
  f.write("**Scope Channel(s) Recorded:"+scope_channel_string_no+"**\n")
  f.write("**FEB VMM Number Mapped:"+scope_channel_string+"**\n")
  f.write("************************\n\n\n\n")

  f.write("############COMMENT############\n")
  f.write("###\n")
  f.write("##############################\n\n\n\n")

  f.close()

  noise_level_scan_6(scope_N,file_name,vmm_pt=vmm_pt_temp,vmm_gain=vmm_gain_temp,vmm_polarity=vmm_POLAR_temp)
  logtext.insert(tk.END,"Output directory: "+target_dir+"\n")

  stop=time.time()
  total_run_time=stop-start
  print "Noise Measurement Run finished in "+str(total_run_time)+" seconds"
  logtext.insert(tk.END,"Noise Measurement Run finished in "+str(total_run_time)+" seconds.\n\n")




# ==== MAIN SCRIPT ====


xm=XmlModifier()
sp=SendPacket()
spa=SendPacket_Ana()
spd=SendPacket_Dig()
spttc=SendPacket_TTC()
master = tk.Tk()
#master.geometry('1280x800')

style = ttk.Style(master)
style.theme_use("clam")

default_color = master.cget('bg')
#temp = tk.Label(master, text='')
default_padx = 120

ttk.Style().configure("TNotebook", background=default_color)
ttk.Style().map("TNotebook.Tab", background=[("selected",default_color)])
ttk.Style().configure('TFrame', background=default_color)

#vmm_pt_temp = 50
#vmm_gain_temp = 1.0

nowdate = datetime.today().strftime('%d%m%Y')

master.title("sTGC Wedge Noise Measurement")

# == GUI title ==
tk.Label(master, text="MINIDAQ IP: 192.168.0.16",font=20, fg="red").grid(row=0, column=0)
tk.Label(master, text='sTGC Wedge Noise Measurement', font=20).grid(row=0, column=1)
tk.Label(master, text="OSCILLOSCOPE IP: 192.168.0.100",font=20, fg="red").grid(row=0, column=2)

# == Tab Configuration ==
frame = tk.Frame(master)
frame.pack()
frame.grid(row=2, columnspan=3, sticky='ew')

tab_parent = ttk.Notebook(frame)
tab1 = ttk.Frame(tab_parent)
tab2 = ttk.Frame(tab_parent)
tab3 = ttk.Frame(tab_parent)
tab_parent.add(tab1, text='Testing Station Info')
tab_parent.add(tab2, text='FEB/Wedge Info')
tab_parent.add(tab3, text='MINIDAQ Initialize/Configuration')
tab_parent.pack(expand=1, fill='both')

# == Testing Station Setup ==
tk.Label(tab1, text='Testing Station Info', font=20, fg='blue').grid(row=9, column=0, columnspan=2, padx=default_padx*2)
tk.Message(tab1, text = "").grid(row=10)

# oscilloscope model
tk.Label(tab1, text='Oscilloscope Model:', justify=tk.LEFT, font=20).grid(row=11, column=0)
v_SCOPE_D = Dropdown_Menu(parent=tab1, optionlist=['MSO56','DPO4034','WR8104'], picks=['Oscilloscope'])
v_SCOPE_D.grid(row=11, column=1)

# MINIDAQ BoardID
tk.Label(tab1, text='MINISAS Connected to MINIDAQ:', justify=tk.LEFT, font=20).grid(row=12, column=0)
v_BID_D = Dropdown_Menu(parent=tab1, optionlist=['J1','J2','J3','J4','J5','J6','J7','J8'], picks=['BoardID'])
v_BID_D.grid(row=12, column=1)

# wedge location
tk.Label(tab1, text='Wedge Testing Location:', justify=tk.LEFT, font=20).grid(row=13, column=0)
v_LOC_D = Dropdown_Menu(parent=tab1, optionlist=['CLEANROOM','GASROOM','FENCEAREA','OTHER'], picks=['Location'])
v_LOC_D.grid(row=13, column=1)

def cleanroom_default():
  v_SCOPE_D.set_option(['MSO56'], parent=tab1)
  v_BID_D.set_option(['J1'], parent=tab1)
  v_LOC_D.set_option(['CLEANROOM'], parent=tab1)
  return
tk.Button(tab1, text="Cleanroom Default", command=cleanroom_default, font=20).grid(row=14, column=1)
tk.Message(tab1, text = "").grid(row=15)

# shifters
tk.Label(tab1, text='Shifter1:', justify=tk.LEFT, font=20).grid(row=20, column=0)
e_shifter1 = tk.Entry(tab1)
e_shifter1.grid(row=20, column=1)

tk.Label(tab1, text='Shifter2:', justify=tk.LEFT, font=20).grid(row=21, column=0)
e_shifter2 = tk.Entry(tab1)
e_shifter2.grid(row=21, column=1)

tk.Label(tab1, text='Shifter3:', justify=tk.LEFT, font=20).grid(row=22, column=0)
e_shifter3 = tk.Entry(tab1)
e_shifter3.grid(row=22, column=1)

tk.Message(tab1, text = "").grid(row=23)

#tk.Button(tab1, text="Set Testing Station Info", command=set_test_station, font=20).grid(row=24, column=1)

# == FEB/Wedge Setup ==
tk.Label(tab2, text='FEB/Wedge Info', font=20, fg='blue').grid(row=19, column=0, columnspan=2, padx=default_padx*2)
tk.Message(tab2, text = "").grid(row=20)

# FEB Info
tk.Label(tab2, text='*FEB serial*:', justify=tk.LEFT, font=20).grid(row=21, column=0)
e_FEB_serial = tk.Entry(tab2,highlightbackground='red')
e_FEB_serial.grid(row=21, column=1)

tk.Label(tab2, text='FEB MTF:', justify=tk.LEFT, font=20).grid(row=22, column=0)
e_FEB_MTF = tk.Entry(tab2)
e_FEB_MTF.grid(row=22, column=1)

tk.Message(tab2, text = "").grid(row=23)

# Chamber Info
fields = ('*Chamber Serial*', 'Large/Small', 'Side A/C', 'Pivot/Confirm', 'Serial Number')
ents = makeform(tab2, fields, line_count=26)  
#b1 = tk.Button(tab2, text='Chamber Decode', command=(lambda e=ents: id_decode(e)),font=20)
#b1.grid(row=31, column=0, columnspan=2)

tk.Message(tab2, text = "").grid(row=32)

# Quadraplate Info
tk.Label(tab2, text='*Adapter Location*:', justify=tk.LEFT, font=20).grid(row=33, column=0)
e_Wedge_LOC = tk.Entry(tab2, highlightbackground="red")
e_Wedge_LOC.grid(row=33, column=1)

#tk.Button(tab2, text='Wedge Location Decode', command=show_Wedge_LOC,font=20).grid(row=34, column=0, columnspan=2)

#tk.Message(tab2, text = "").grid(row=35)

# == MINIDAQ Setup ==
tk.Label(tab3, text='MINIDAQ Initialize/Configuration', font=20, fg='blue').grid(row=29, column=0, columnspan=2, padx=default_padx*2)
tk.Message(tab3, text = "").grid(row=30)

tk.Button(tab3, text='Initialize ROC (Wait 5 sec)', command=paraprocess_ROC,font=20).grid(row=31, column=0)
tk.Button(tab3, text='Initialize SCA', command=paraprocess_SCA,font=20).grid(row=32, column=0)
tk.Button(tab3, text='Read SCA ID', command=SCAID_insert,font=20).grid(row=33, column=0)
e3 = tk.Entry(tab3)
e3.grid(row=33, column=1)

#tk.Label(tab3, text="SCAID:",font=20).grid(row=32, column=0)
tk.Message(tab3, text = "").grid(row=34)

# VMM Gain
tk.Label(tab3, text='VMM Gain (mV/fC):', justify=tk.LEFT, padx=20, font=20).grid(row=35, column=0)
var_GAIN_D = Dropdown_Menu(parent=tab3, optionlist=['1.0','3.0'], picks=['Gain'])
var_GAIN_D.grid(row=35, column=1)

# VMM PT
tk.Label(tab3, text='VMM PT (ns):', justify=tk.LEFT, padx=20, font=20).grid(row=36, column=0)
var_PT_D = Dropdown_Menu(parent=tab3, optionlist=['25','50'], picks=['PT'])
var_PT_D.grid(row=36, column=1)

# VMM Polarity
tk.Label(tab3, text='VMM Polarity:', justify=tk.LEFT, padx=20, font=20).grid(row=37, column=0)
var_POLAR_D = Dropdown_Menu(parent=tab3, optionlist=['Positive (Pad/Strip)','Negative (Wire)'], picks=['Polarity'])
var_POLAR_D.grid(row=37, column=1)

def vmm_pad_default():
  var_GAIN_D.set_option(['3.0'], parent=tab3)
  var_PT_D.set_option(['50'], parent=tab3)
  var_POLAR_D.set_option(['Positive (Pad/Strip)'], parent=tab3)
  return
tk.Button(tab3, text="VMM Pad Default", command=vmm_pad_default, font=20).grid(row=38, column=1)

def vmm_strip_default():
  var_GAIN_D.set_option(['1.0'], parent=tab3)
  var_PT_D.set_option(['50'], parent=tab3)
  var_POLAR_D.set_option(['Positive (Pad/Strip)'], parent=tab3)
  return
tk.Button(tab3, text="VMM Strip Default", command=vmm_strip_default, font=20).grid(row=39, column=1)

#tk.Button(tab3, text='Set VMM', command=set_vmm, font=20).grid(row=40, column=1)

tk.Message(tab3, text = "").grid(row=41)

# Cpi and Rp input
tk.Label(tab3, text='Cpi in pF (pFEB):', justify=tk.LEFT, font=20).grid(row=50, column=0)
e_Cpi = tk.Entry(tab3)
e_Cpi.grid(row=50, column=1)

tk.Label(tab3, text='Rp in kOhm (sFEB):', justify=tk.LEFT, font=20).grid(row=51, column=0)
e_Rp = tk.Entry(tab3)
e_Rp.grid(row=51, column=1)

tk.Message(tab3, text = "").grid(row=52)

# VMM to oscilloscope channel mapping
tk.Label(tab3, text='Scope to VVM Channel Mapping:\n(None: unused scope channel)\n(-1: unconnected scope channel)', justify=tk.LEFT, font=20).grid(row=60, column=0)
#tk.Button(tab3, text='Generate VMMID', command=allstates_map, font=20).grid(row=60, column=1)
map1 = Dropdown_Menu(parent=tab3, optionlist=['-1','0','1','2','3','4','5','6','7'], picks=['CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8'])
map1.grid(row=61, column=0, columnspan=2)

def map_pad_default():
  map1.set_option(['None','1','2','None','None','None','None','None'], parent=tab3)
  return
tk.Button(tab3, text="N 1 2 N N N N N", command=map_pad_default, font=20).grid(row=62, column=0)

def map_strip_0123_default():
  map1.set_option(['0','1','2','3','None','None','None','None'], parent=tab3)
  return
tk.Button(tab3, text="0 1 2 3 N N N N", command=map_strip_0123_default, font=20).grid(row=62, column=1)

def map_strip_4567_default():
  map1.set_option(['4','5','6','7','None','None','None','None'], parent=tab3)
  return
tk.Button(tab3, text="4 5 6 7 N N N N", command=map_strip_4567_default, font=20).grid(row=63, column=1)


tk.Message(tab3, text = "").grid(row=99)

# == Scan Options ==
tk.Label(master, text='Run Auto-Scan', font=20, fg='blue').grid(row=39, column=0, columnspan=2)
tk.Message(master, text = "").grid(row=40)

# Comment box
tk.Label(master, text='Comments:', justify=tk.LEFT, font=20).grid(row=41, column=0)
e_comment = tk.Entry(master)
e_comment.grid(row=41, column=1, sticky='ew')

# target directory
tk.Button(master, text="Set Target Directory", command=c_open_target_dir, font=20).grid(row=41, column=2)

# File handling
#tk.Button(master, text='Generate RUNID', command=runID_gen, font=20).grid(row=42, column=0)
#tk.Button(master, text='Show File Name', command=show_file_name, font=20).grid(row=42, column=1) 

tk.Message(master, text = "").grid(row=43)

# Scan Noise
tk.Button(master, text='Set All Configurations', command=set_all_config, font=20).grid(row=44, column=0) 
tk.Button(master, text='Scan Noise (4 Channels)', command=main_noise_measurement_4, font=20).grid(row=44, column=1)
tk.Button(master, text='Scan Noise (6 Channels)', command=main_noise_measurement_6, font=20).grid(row=44, column=2)


# == Log output textbox ==
#tk.Message(master, text = "").grid(row=90) # spacer
#ttk.Separator(master,orient=tk.VERTICAL).grid(row=1, column=3, rowspan=4, sticky='ew')
tk.Label(master, text="GUI Log:", font=20).grid(row=1, column=3, columnspan=2, sticky='ew')
logtext = tkst.ScrolledText(master, bd=2, highlightbackground="black")
logtext.grid(row=2, column=3, columnspan=2, sticky='ewns')


# == debug == TODO: change to exit
def debugging():
  v_SCOPE_D.set_option(['MSO56'], parent=tab1)
  v_BID_D.set_option(['J1'], parent=tab1)
  v_LOC_D.set_option(['CLEANROOM'], parent=tab1)
  return
tk.Button(master, text="Debug", command=debugging, font=20).grid(row=94, column=0)

tk.Button(master, text='Quit', command=master.quit, font=20).grid(row=94, column=2)

master.mainloop( )



