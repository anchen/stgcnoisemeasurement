# 15 Apr 2019 Bobby Tse
# This program is intended to decode the Chamber ID scanned from QR code, check if the code is really used on wedge
# & return the decoded code in the format ["QS/QL","Side A/C","P/C"]
# e.g. 20MNIWSAP00001
# [0][1] "20" shows it is in ALTAS project
# [2]    "M" stands for "MUON"
# [3]    "N" stands for "NSW"
# [4]    "I" stands for 
# [5]    "W" stands for "Wedge"
# [6]    "S" stands for "Small" {Options: "S"/"L"}
# [7]    "A" stands for "Side A" {Options: "A"/"C"}
# [8]    "P" stands for "Pivot" {Options: "P"/"C"}
# [9]    "0" stands for
# [10][11][12][13] "0001" indicates the number/serial of wedge


import sys
import math

def cham_decode(Cham_Serial):
	
	code = Cham_Serial

	print (code)
	#code = "20MNIWSAP00001"
	
	code_list = list(code)
	
	code_seg = code_list[0]+code_list[1]
	code_serial = code_list[10]+code_list[11]+code_list[12]+code_list[13]
	code_re_list = []
	
	if code_seg == "20":
		print ("It is under ALTAS")
	elif code_seg == "00":
		print ("It is tested on bench")
	else:
		raise Exception("It is not in ALTAS, please check")
	
	if code_list[2] == "M":
		print ("It is ALTAS MUON")
	elif code_list[2] == "0":
		print ("It is tested on bench")
	else:
		raise Exception("It is not ALTAS MUON, please check")
	
	if code_list[3] == "N":
		print ("It is ALTAS MUON NSW")
	elif code_list[3] == "0":
		print ("It is tested on bench")
	else:
		raise Exception("It is not ALTAS MUON NSW, please check")
	
	if code_list[5] == "W":
		print ("Wedge")
	elif code_list[5] == "0":
		print ("It is tested on bench")
	else:
		raise Exception("It is not Wedge, please check")
	
	if code_list[6] == "S":
		code_re_list.append("QS")
		print ("Small Wedge")
	elif code_list[6] == "L":
		code_re_list.append("QL")
		print ("Large Wedge")
	elif code_list[6] == "0":
		code_re_list.append("Bench")
		print ("It is tested on bench")
	else:
		raise Exception("It is not Wedge, please check")
	
	if code_list[7] == "A":
		code_re_list.append("Side A")
		print ("Side A")
	elif code_list[7] == "C":
		code_re_list.append("Side C")
		print ("Side C")
	elif code_list[7] == "0":
		code_re_list.append("Bench")
		print ("It is tested on bench")
	else:
		raise Exception("It is not Side A or Side C, please check")
	
	if code_list[8] == "P":
		code_re_list.append("Pivot")
		print ("Pivot")
	elif code_list[8] == "C":
		code_re_list.append("Confirm")
		print ("Confirm")
	elif code_list[8] == "0":
		code_re_list.append("Bench")
		print ("It is tested on bench")
	else:
		raise Exception("It is not Pivot or Confirm, please check")
	
	code_re_list.append(code_serial)

	print (code_re_list)

	return code_re_list








