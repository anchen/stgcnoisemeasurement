# This file is used to modify BoardID for MINISAS connection between MINIDAQ & FEB in "target_info.py" by tk GUI

import re

def BoardID_replacer_board_type_0():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        #text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 0', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_1():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 1', text)
        #text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 1', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 1', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 1', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 1', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 1', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 1', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_2():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 2', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 2', text)
        #text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 2', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 2', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 2', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 2', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 2', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_3():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 3', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 3', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 3', text)
        #text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 3', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 3', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 3', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 3', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_4():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 4', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 4', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 4', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 4', text)
        #text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 4', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 4', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 4', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_5():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 5', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 5', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 5', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 5', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 5', text)
        #text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 5', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 5', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_6():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 6', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 6', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 6', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 6', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 6', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 6', text)
        #text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 0', text)
        text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 6', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()

def BoardID_replacer_board_type_7():
    with open("target_info.py", 'r+') as f:
        text = f.read()
        text = re.sub('\t\tself.board_id = 0', '\t\tself.board_id = 7', text)
        text = re.sub('\t\tself.board_id = 1', '\t\tself.board_id = 7', text)
        text = re.sub('\t\tself.board_id = 2', '\t\tself.board_id = 7', text)
        text = re.sub('\t\tself.board_id = 3', '\t\tself.board_id = 7', text)
        text = re.sub('\t\tself.board_id = 4', '\t\tself.board_id = 7', text)
        text = re.sub('\t\tself.board_id = 5', '\t\tself.board_id = 7', text)
        text = re.sub('\t\tself.board_id = 6', '\t\tself.board_id = 7', text)
        #text = re.sub('\t\tself.board_id = 7', '\t\tself.board_id = 0', text)
        f.seek(0)
        f.write(text)
        f.truncate()
        f.close()
