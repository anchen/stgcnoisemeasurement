### Saved Raw Data Title format ###
# RunID+TestDate + p/sFEB+ SCA ID + Wedge Serial + Quad info +Layer + Gain + PT + polarity + VMMtoscope mapping
# File Format: run_00002_17052019_pFEB_1045_20MNIWSAP00001_QS1P_L1_3_50_P_0_1_2_3.dat
# File Format: run_00002_17052019_pFEB_1045_00000000000000_QS0T_L0_3_50_N_2_3_4.dat (On test Bench)
# runid+_+nowdate+_+board_type_N+_+scaid+_+monthly+_+Size_ChamberNo+Position+_+Layer+_+vmm_gain_temp_t+_+vmm_pt_temp+_+board_type_N_t+_+vmmmaping+".dat"

from __future__ import absolute_import
from __future__ import unicode_literals

import os
import psutil
import glob
import time

#from Tkinter import *
import Tkinter as tk
import ttk
from Cham_DECODER_funct import *
from line_replacer import *
from BoardID_replacer import *
from DECODER_SCAID_funct import *
#from tekscope_utils_4034 import *
from tekscope_utils import *
from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket
from Noise_process_funct import *
from VMMconfig_replacer import *
from datetime import datetime
from roc_analog_reg_cfg import *
from roc_digital_reg_cfg import *
from sendttc import *

#Use for running shell script
import subprocess


def show_entry_fields():
   print("First Name: %s\nLast Name: %s" % (e1.get(), e2.get()))


def set_setup():
  global scope_type_N
  global location_type
  global location_type_T

  scope_type_N = str((v_SCOPE_D.state())[0])
  location_type = str((v_LOC_D.state())[0])
  location_type_T = location_type[0]
  var_BoardID = int((v_BID_D.state())[0][1:])-1

  if var_BoardID == 0:
    BoardID_replacer_board_type_0()
  elif var_BoardID == 1:
    BoardID_replacer_board_type_1()
  elif var_BoardID == 2:
    BoardID_replacer_board_type_2()
  elif var_BoardID == 3:
    BoardID_replacer_board_type_3()
  elif var_BoardID == 4:
    BoardID_replacer_board_type_4()
  elif var_BoardID == 5:
    BoardID_replacer_board_type_5()
  elif var_BoardID == 6:
    BoardID_replacer_board_type_6()
  elif var_BoardID == 7:
    BoardID_replacer_board_type_7()

  print(scope_type_N)
  print(location_type)
  print(location_type_T)
  print(var_BoardID)


def set_minidaq():
  global vmm_gain_temp
  global vmm_gain_temp_t
  global vmm_pt_temp
  global vmm_pt_temp_N
  global vmm_POLAR_temp
  global vmm_POLAR_temp_N

  vmm_gain_temp = float((var_GAIN_D.state())[0])
  vmm_gain_temp_t = str(int(vmm_gain_temp))
  vmm_pt_temp = long((var_PT_D.state())[0])
  vmm_pt_temp_N = str(int(vmm_pt_temp))
  vmm_POLAR_temp_N = str((var_POLAR_D.state())[0])[0]

  if (vmm_POLAR_temp_N == 'P'):
    vmm_POLAR_temp = 0
  elif (vmm_POLAR_temp_N == 'N'):
    vmm_POLAR_temp = 1
  else:
    vmm_POLAR_temp = None

  print(vmm_gain_temp)
  print(vmm_gain_temp_t)
  print(vmm_pt_temp)
  print(vmm_pt_temp_N)
  print(vmm_POLAR_temp)
  print(vmm_POLAR_temp_N)


def show_FEB_serial(): 
	global FEB_serial
	FEB_serial = e_FEB_serial.get()
	print("FEB_serial: %s" % (FEB_serial))


def id_decode(entries):
    global Position
    global monthly
    global S_L_Wedge
    global small_or_large
    global A_C_Wedge
    global P_C_Wedge
    global Wedge_Serial_Number
    global A_or_C
    monthly = (str(entries['*Chamber Serial*'].get()))
    cham_code = cham_decode(monthly)
    S_L_Wedge = cham_code[0]
    A_C_Wedge = cham_code[1]
    P_C_Wedge = cham_code[2]
    Wedge_Serial_Number = cham_code[3]
    entries['Large/Small'].delete(0,tk.END)
    entries['Large/Small'].insert(0, cham_code[0] )
    entries['Side A/C'].delete(0,tk.END)
    entries['Side A/C'].insert(0, cham_code[1] )
    entries['Pivot/Confirm'].delete(0,tk.END)
    entries['Pivot/Confirm'].insert(0, cham_code[2] )
    entries['Serial Number'].delete(0,tk.END)
    entries['Serial Number'].insert(0, cham_code[3] )
    print("Chamber Serial: %s" % str(monthly))

    if cham_code[2] == "Pivot":
        Position = "P"
    elif cham_code[2] == "Confirm":
        Position = "C"
    elif cham_code[2] == "Bench":
        Position = "T"

    if S_L_Wedge == "QS":
        small_or_large = "Small"
    elif S_L_Wedge == "QL":
        small_or_large = "Large"
    else:
        small_or_large = "N/A"

    if A_C_Wedge == "Side A":
        A_or_C = "A"
    elif A_C_Wedge == "Side C":
        A_or_C = "C"
    else:
        A_or_C = "N/A"


def makeform(root, fields, line_count=0):
   entries = {}
   for field in fields:
      lab = tk.Label(master, text=field+':',font=20)
      if ((field[0] == '*') & (field[-1] == '*')):
        ent = tk.Entry(master, highlightbackground='red')
      else:
        ent = tk.Entry(master)
      lab.grid(row=line_count, column=0)
      ent.grid(row=line_count, column=1)
      entries[field] = ent
      line_count = line_count + 1
   return entries


def show_Wedge_LOC(): 
	global Wedge_LOC
	global Size_ChamberNo_Position
	global board_type_N
	global board_type_N_t
	global Layer
	global Layer_T
	code_wedge_list = []
	Wedge_LOC = e_Wedge_LOC.get()
	code_wedge_list = Wedge_LOC.split("-")
	if len(code_wedge_list) == 3:
		Size_ChamberNo_Position = code_wedge_list[0]
		if code_wedge_list[1] == "Pad":
			line_replacer_board_type_0()
			board_type_N = "pFEB"
			board_type_N_t = "P"
		elif code_wedge_list[1] == "Strip":
			line_replacer_board_type_1()
			board_type_N = "sFEB"
			board_type_N_t = "P"
		print("Measurement location: %s" % (Wedge_LOC))
		if code_wedge_list[2] == "Layer1":
			Layer = "L1"
			Layer_T = "1"
		elif code_wedge_list[2] == "Layer2":
			Layer = "L2"
			Layer_T = "2"
		elif code_wedge_list[2] == "Layer3":
			Layer = "L3"
			Layer_T = "3"
		elif code_wedge_list[2] == "Layer4":
			Layer = "L4"
			Layer_T = "4"
		else:
			Layer = "L0"
			Layer_T = "0"
	else:
		Size_ChamberNo_Position = "QS0T"
		board_type_N = "pFEB"
		board_type_N_t = "P"
		Layer = "L0"
		Layer_T = "0"


def paraprocess_SCA():
    subprocess.call("./test_SCA.sh")


def SCAID_insert():
    global scaid
    e3.insert(0, SCAID_DECODER() )
    os.remove('test.dat')
    scaid = e3.get()


def paraprocess_ROC():
	spa.send('roc','CfgASIC',target)
	time.sleep(2)
	spd.send('roc','CfgASIC',target)
	print "ROC configuation done"


def ShowChoice_VMM_GAIN():
	global vmm_gain_temp
	global vmm_gain_temp_t
	if var_GAIN.get() == 0:
		vmm_gain_temp = 1.0
		vmm_gain_temp_t = "1"
	elif var_GAIN.get() == 1:
		vmm_gain_temp = 3.0
		vmm_gain_temp_t = "3"


# TODO: description
def ShowChoice_VMM_PT():
	global vmm_pt_temp
	global vmm_pt_temp_N
	if var_PT.get() == 0:
		vmm_pt_temp = 25
		vmm_pt_temp_N = "25"
	elif var_PT.get() == 1:
		vmm_pt_temp = 50
		vmm_pt_temp_N = "50"


# TODO: description
def ShowChoice_VMM_POLAR():
	global vmm_POLAR_temp
	global vmm_POLAR_temp_N
	if var_POLAR.get() == 0:
		vmm_POLAR_temp = 0
		vmm_POLAR_temp_N = "P"
	elif var_POLAR.get() == 1:
		vmm_POLAR_temp = 1
		vmm_POLAR_temp_N = "N"


# Dropdown menu for VMM number
class Dropdown_N(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('N')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'N','-1','0','1','2','3','4','5','6','7')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# Dropdown menu for oscilloscope model
class Dropdown_Osc(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('None')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'None','MSO56','DPO4034','WR8104')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# Dropdown menu for BoardID
class Dropdown_BID(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('None')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'None','J1','J2','J3','J4','J5','J6','J7','J8')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# Dropdown menu for location
class Dropdown_LOC(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('None')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'None','CLEANROOM','GASROOM','FENCEAREA','OTHER')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# Dropdown menu for VMM Gain
class Dropdown_Gain(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('None')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'None','1.0','3.0')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# Dropdown menu for VMM PT
class Dropdown_PT(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('None')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'None','25','50')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# Dropdown menu for VMM Polarity
class Dropdown_Pol(tk.Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor='w'):
		tk.Frame.__init__(self, parent)
		self.vars = []
		for pick in picks:
			tkvar_map = tk.StringVar(master)
			tkvar_map.set('None')
			popupMenu = tk.OptionMenu(self, tkvar_map, 'None','Positive (Pad/Strip)','Negative (Wire)')
			popupMenu.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(tkvar_map)
	def state(self):
		return map((lambda tkvar_map: tkvar_map.get()), self.vars)


# TODO: description
def allstates_map(): 
	global total_scanned_channel
	global scope_channel_string
	global scope_channel_string_no
	global scope_channel_string_N

	list_vmmmap = list(map1.state())
	#list_vmmmap = [i for i in list_vmmmap if not 'N' in i]
	print(list_vmmmap)

	list_scope_channel_no = []
 	list_scope_channel = []
	for x in range(0,8):
		print x
		if list_vmmmap[x] != 'N':
			list_scope_channel.append(int(list_vmmmap[x]))
			list_scope_channel_no.append(x+1)


	total_scanned_channel = len(list_scope_channel_no)
	print total_scanned_channel
	print list_scope_channel_no
	print list_scope_channel

	if total_scanned_channel == 1:
		scope_channel_string = "{}".format(list_scope_channel[0])
		scope_channel_string_no = "{}".format(list_scope_channel_no[0])
	elif total_scanned_channel == 2:
		scope_channel_string = "{},{}".format(list_scope_channel[0], list_scope_channel[1])
		scope_channel_string_no = "{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1])
	elif total_scanned_channel == 3:
		scope_channel_string = "{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2])
		scope_channel_string_no = "{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2])
	elif total_scanned_channel == 4:
		scope_channel_string = "{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3])
 		scope_channel_string_no = "{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3])
	elif total_scanned_channel == 5:
		scope_channel_string = "{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4])
		scope_channel_string_no = "{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4])
	elif total_scanned_channel == 6:
		scope_channel_string = "{},{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4], list_scope_channel[5])
		scope_channel_string_no = "{},{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4], list_scope_channel_no[5])
	elif total_scanned_channel == 7:
		scope_channel_string = "{},{},{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4], list_scope_channel[5], list_scope_channel[6])
		scope_channel_string_no = "{},{},{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4], list_scope_channel_no[5], list_scope_channel_no[6])
	elif total_scanned_channel == 8:
		scope_channel_string = "{},{},{},{},{},{},{},{}".format(list_scope_channel[0], list_scope_channel[1], list_scope_channel[2], list_scope_channel[3], list_scope_channel[4], list_scope_channel[5], list_scope_channel[6], list_scope_channel[7])
		scope_channel_string_no = "{},{},{},{},{},{},{},{}".format(list_scope_channel_no[0], list_scope_channel_no[1], list_scope_channel_no[2], list_scope_channel_no[3], list_scope_channel_no[4], list_scope_channel_no[5], list_scope_channel_no[6], list_scope_channel_no[7])

	print scope_channel_string
	print scope_channel_string_no

	list_scope_channel_N = []
	for x in range(0,len(list_scope_channel)):
		if (list_scope_channel[x] != -1):
			list_scope_channel_N.append(list_scope_channel[x])

	print list_scope_channel_N

	total_scanned_channel_N = len(list_scope_channel_N)

	if total_scanned_channel_N == 1:
		scope_channel_string_N = "{}".format(list_scope_channel_N[0])
	elif total_scanned_channel_N == 2:
		scope_channel_string_N = "{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1])
	elif total_scanned_channel_N == 3:
		scope_channel_string_N = "{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2])
	elif total_scanned_channel_N == 4:
		scope_channel_string_N = "{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3])
	elif total_scanned_channel_N == 5:
		scope_channel_string_N = "{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4])
	elif total_scanned_channel_N == 6:
		scope_channel_string_N = "{}_{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4], list_scope_channel_N[5])
	elif total_scanned_channel_N == 7:
		scope_channel_string_N = "{}_{}_{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4], list_scope_channel_N[5], list_scope_channel_N[6])
	elif total_scanned_channel_N == 8:
		scope_channel_string_N = "{}_{}_{}_{}_{}_{}_{}_{}".format(list_scope_channel_N[0], list_scope_channel_N[1], list_scope_channel_N[2], list_scope_channel_N[3], list_scope_channel_N[4], list_scope_channel_N[5], list_scope_channel_N[6], list_scope_channel_N[7])

	print scope_channel_string_N


# Display name of generated data file
def show_file_name():
	global vmm_pt_temp_N
	global vmm_gain_temp_t
	global runid
	global board_type_N
	global scaid
	global monthly
	#global Size_ChamberNo
	#global Position
	global Size_ChamberNo_Position
	global Layer
	global vmm_POLAR_temp_N
	global scope_channel_string_N
	global file_name
	global FEB_MTF
	global FEB_serial
	global location_type_T
	#print Size_ChamberNo

	FEB_serial = e_FEB_serial.get()
	FEB_MTF = e_FEB_MTF.get()
	print("FEB_serial: %s" % (FEB_serial))

	#if (runid != "") & (nowdate != "") & (board_type_N != "") & (scaid != "") & (monthly != "") & (Size_ChamberNo_Position != "") & (Layer != "")& (vmm_gain_temp_t != "") & (vmm_pt_temp_N != "") & (vmm_POLAR_temp_N != "") & (scope_channel_string_N != "") & (location_type_T != ""):
	file_name="run_"+runid+"_"+nowdate+"_"+board_type_N+"_"+scaid+"_"+monthly+"_"+Size_ChamberNo_Position+"_"+Layer+"_"+vmm_gain_temp_t+"_"+vmm_pt_temp_N+"_"+vmm_POLAR_temp_N+"_VMM_"+scope_channel_string_N+"_"+location_type_T+".dat"


	print file_name

# Generate a run ID
def runID_gen():
	global runid
	datfiles = []
	runid_temp = []
	for file in glob("*.dat"):
		datfiles.append(file)
	print datfiles
	if len(datfiles) == 0:
		f=open('run_00000_99999999_tFEB_00000_00000000000000_L0_0_00_T_VMM_0.dat','a')
		f.write("**This is a dummy file for further file generation**\n")
		f.close()
		runid_temp = [0]
	else:
		for x in range(0,len(datfiles)):
			data_temp = datfiles[x].split("_")
			print data_temp[1]
			runid_temp.append(int(data_temp[1]))
	print runid_temp
	print max(runid_temp)
	runid = str(int(max(runid_temp)) + 1)
	print runid


# Noise RMS measurement for 4 VMMs
def main_noise_measurement_4():
	global vmm_pt_temp
	global vmm_gain_temp
	global runid
	global board_type_N
	global scaid
	global monthly
	#global Size_ChamberNo
	global Position
	global Layer
	global vmm_POLAR_temp
	global scope_channel_string_N
	global file_name
	global S_L_Wedge
	global A_C_Wedge
	global P_C_Wedge
	global Wedge_Serial_Number
	global scope_type_N
	global FEB_serial
	global vmm_POLAR_temp_N
	global FEB_MTF
	global vmm_gain_temp_t
	global vmm_pt_temp_N
	print vmm_pt_temp
	print vmm_gain_temp
	global Size_ChamberNo_Position
	global total_scanned_channel
	global scope_channel_string
	global scope_channel_string_no
	global location_type
	global small_or_large
	global A_or_C
	global Layer_T
	print Layer

	Size_ChamberNo_Position_list = list(Size_ChamberNo_Position)
	QType = Size_ChamberNo_Position_list[0] + Size_ChamberNo_Position_list[1]
	QIndex = Size_ChamberNo_Position_list[2]

	comment_tab = e_comment.get()
	shifter1 = e_shifter1.get()
	shifter2 = e_shifter2.get()
	shifter3 = e_shifter3.get()
	Cpi_Pad = e_Cpi.get()
	Rp_Strip = e_Rp.get()

	start=time.time()

	#== setup scope
	scope_N=scope('192.168.0.100')
	scope_chan=1
	setup_scope(scope_N,scope_channel=1)
	setup_scope(scope_N,scope_channel=2)
	setup_scope(scope_N,scope_channel=3)
	setup_scope(scope_N,scope_channel=4)
	DATAFOLDERDIR='./data/'
	f=open(file_name,'a')

	f.write("************************\n")
	f.write("**Measurement Unix Time:"+str(int(time.time()))+"**\n")
	f.write("**Measurement Date:"+nowdate+"**\n")

	if (shifter1 != "") & (shifter2 == "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+""+"**\n")
		f.write("**Measurement Shifter3:"+""+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+""+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 != ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+shifter3+"**\n")
	else:
		print("No of channels")
		f.write("**Measurement Shifter1:"+""+"**\n")
		f.write("**Measurement Shifter2:"+""+"**\n")
		f.write("**Measurement Shifter3:"+""+"**\n")

	if location_type != "":
		f.write("**Measurement Location:"+location_type+"**\n")
	else:
		showerror("Error", "No measurment location inserted")

	f.write("**FEB Serial Number:"+FEB_serial+"**\n")
	f.write("**FEB MTF:"+FEB_MTF+"**\n")	
	f.write("**FEB Type:"+board_type_N+"**\n")

	if (Rp_Strip == "") & (Cpi_Pad != ""):
		f.write("**Cpi if pFEB(pF):"+Cpi_Pad+"**\n")
	elif (Rp_Strip != "") & (Cpi_Pad == ""):
		f.write("**Rpull-up if sFEB(kOhm):"+Rp_Strip+"**\n")
	else:
		print("Neither Cpi if pFEB(pF) nor Rpull-up if sFEB(kOhm) input provide. please check")

	f.write("**VMM Gain(mV/fC):"+vmm_gain_temp_t+"**\n")
	f.write("**VMM Peaking Time (ns):"+vmm_pt_temp_N+"**\n")
	f.write("**VMM SFM:"+"1"+"**\n")

	f.write("**Wedge MTF:"+monthly+"**\n")
	f.write("**Wedge Size:"+small_or_large+"**\n")
	f.write("**Wedge for ATLAS Side A or C:"+A_or_C+"**\n")
	f.write("**Wedge Type:"+P_C_Wedge+"**\n")
	f.write("**Wedge Serial Number:"+Wedge_Serial_Number+"**\n")
	f.write("**Quadruplet Type:"+QType+"**\n")
	f.write("**Quadruplet Index:"+QIndex+"**\n")
	f.write("**Quadruplet Layer:"+Layer_T+"**\n")

	f.write("**GFZ Adapter Board Type:"+"**\n")
	f.write("**Scope Type:"+ scope_type_N+"**\n")
	f.write("**Scope Channels Connected:"+str(total_scanned_channel)+"**\n")
	f.write("**Scope Channel Recorded:"+scope_channel_string_no+"**\n")
	f.write("**FEB VMM Number Mapped:"+scope_channel_string+"**\n")
	f.write("************************\n\n\n\n")
	
	if comment_tab == "":
		f.write("############COMMENT############\n")
		f.write("###\n")
		f.write("##############################\n\n\n\n")
	elif comment_tab != "":
		f.write("############COMMENT############\n")
		f.write("###"+comment_tab+"\n")
		f.write("##############################\n\n\n\n")
	else:
		print("No comment inputed, please check")				

	f.close()

	noise_level_scan_4(scope_N,file_name,vmm_pt=vmm_pt_temp,vmm_gain=vmm_gain_temp,vmm_polarity=vmm_POLAR_temp)

	stop=time.time()
	total_run_time=stop-start
	print "Noise Measurement Run finished in "+str(total_run_time)+" seconds"


# Noise RMS measurement for 6 VMMs
def main_noise_measurement_6():
	global vmm_pt_temp
	global vmm_gain_temp
	global runid
	global board_type_N
	global scaid
	global monthly
	#global Size_ChamberNo
	global Position
	global Layer
	global vmm_POLAR_temp
	global scope_channel_string_N
	global file_name
	global S_L_Wedge
	global A_C_Wedge
	global P_C_Wedge
	global Wedge_Serial_Number
	global scope_type_N
	global FEB_serial
	global vmm_POLAR_temp_N
	global FEB_MTF
	global vmm_gain_temp_t
	global vmm_pt_temp_N
	print vmm_pt_temp
	print vmm_gain_temp
	global Size_ChamberNo_Position
	global Cpi_Pad
	global Rp_Strip
	global shifter1
	global shifter2
	global shifter3
	global total_scanned_channel
	global scope_channel_string
	global scope_channel_string_no
	global location_type
	global small_or_large
	global Layer_T
	print Layer
	start=time.time()

	#== setup scope
	scope_N=scope('192.168.0.100')
	scope_chan=1
	setup_scope(scope_N,scope_channel=1)
	setup_scope(scope_N,scope_channel=2)
	setup_scope(scope_N,scope_channel=3)
	setup_scope(scope_N,scope_channel=4)
	setup_scope(scope_N,scope_channel=5)
	setup_scope(scope_N,scope_channel=6)
	DATAFOLDERDIR='./data/'
	f=open(file_name,'a')


	f.write("************************\n")
	f.write("**Measurement Date:"+nowdate+"**\n")

	if (shifter1 != "") & (shifter2 == "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+" "+"**\n")
		f.write("**Measurement Shifter3:"+" "+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 == ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+" "+"**\n")
	elif (shifter1 != "") & (shifter2 != "") & (shifter3 != ""):
		f.write("**Measurement Shifter1:"+shifter1+"**\n")
		f.write("**Measurement Shifter2:"+shifter2+"**\n")
		f.write("**Measurement Shifter3:"+shifter3+"**\n")
	else:
		print("No of channels")
		f.write("**Measurement Shifter1:"+" "+"**\n")
		f.write("**Measurement Shifter2:"+" "+"**\n")
		f.write("**Measurement Shifter3:"+" "+"**\n")

	f.write("**Measurement Location:"+location_type+"**\n")
	f.write("**FEB Serial Number:"+FEB_serial+"**\n")
	f.write("**FEB MTF:"+FEB_MTF+"**\n")	
	f.write("**FEB Type:"+board_type_N+"**\n")

	if (Rp_Strip == "") & (Cpi_Pad != ""):
		f.write("**Cpi if pFEB(pF):"+Cpi_Pad+"**\n")
	elif (Rp_Strip != "") & (Cpi_Pad == ""):
		f.write("**Rpull-up if sFEB(kOhm):"+Rp_Strip+"**\n")
	else:
		print("Neither Cpi if pFEB(pF) nor Rpull-up if sFEB(kOhm) inputed, please check")

	f.write("**VMM Gain(mV/fC):"+vmm_gain_temp_t+"**\n")
	f.write("**VMM Peaking Time(ns):"+vmm_pt_temp_N+"**\n")
	f.write("**VMM SFM:"+"1"+"**\n")

	f.write("**Wedge MTF:"+monthly+"**\n")
	f.write("**Wedge Size:"+small_or_large+"**\n")
	f.write("**Wedge for Side ATLAS A or C:"+A_C_Wedge+"**\n")
	f.write("**Wedge Type:"+P_C_Wedge+"**\n")
	f.write("**Wedge Serial Number:"+Wedge_Serial_Number+"**\n")
	f.write("**Quadruplet Type:"+Size_ChamberNo_Position+"**\n")
	f.write("**Quadruplet Layer:"+Layer_T+"**\n")

	f.write("**GFZ Adapter Board Type:"+' '+"**\n")
	f.write("**Scope Type:"+ scope_type_N+" **\n")
	f.write("**Number of Scope Channel(s) Connected:"+str(total_scanned_channel)+"**\n")
	f.write("**Scope Channel(s) Recorded:"+scope_channel_string_no+"**\n")
	f.write("**FEB VMM Number Mapped:"+scope_channel_string+"**\n")
	f.write("************************\n\n\n\n")

	f.write("############COMMENT############\n")
	f.write("###\n")
	f.write("##############################\n\n\n\n")

	f.close()

	noise_level_scan_6(scope_N,file_name,vmm_pt=vmm_pt_temp,vmm_gain=vmm_gain_temp,vmm_polarity=vmm_POLAR_temp)

	stop=time.time()
	total_run_time=stop-start
	print "Noise Measurement Run finished in "+str(total_run_time)+" seconds"




# ==== MAIN SCRIPT ====


xm=XmlModifier()
sp=SendPacket()
spa=SendPacket_Ana()
spd=SendPacket_Dig()
spttc=SendPacket_TTC()
master = tk.Tk()

style = ttk.Style(master)
style.theme_use("clam")

vmm_pt_temp = 50
vmm_gain_temp = 1.0

nowdate = datetime.today().strftime('%d%m%Y')

master.title("sTGC Wedge Noise Auto Measurement")

# == GUI title ==
tk.Label(master, text='sTGC Wedge Noise Measurement Plot', font=20).grid(row=0, column=0)
tk.Label(master, text="MINIDAQ IP: 192.168.0.16",font=20, fg="red").grid(row=0, column=1)
tk.Label(master, text="OSCILLOSCOPE IP: 192.168.0.100",font=20, fg="red").grid(row=0, column=2)
ttk.Separator(master,orient=tk.HORIZONTAL).grid(row=1, columnspan=3, sticky='ew')

# debug TODO: change to exit
def debugging():
  print("LOC: "+(v_LOC.state())[0])
  print("LOC_T: "+(v_LOC.state())[0][0])
  print(type((v_LOC.state())[0][0]))

tk.Button(master, text="debug", command=debugging, font=20).grid(row=0, column=3)

# == Testing Station Setup ==
tk.Label(master, text='1. Testing Station Info', font=20, fg='blue').grid(row=10, column=1)

# oscilloscope model
tk.Label(master, text='Oscilloscope Model:', justify=tk.LEFT, padx=20,font=20).grid(row=11, column=0)
v_SCOPE_D = Dropdown_Osc(master, ['Oscilloscope'])
v_SCOPE_D.grid(row=12, column=0)

# MINIDAQ BoardID
tk.Label(master, text='MINISAS Number Connected to MINIDAQ:', justify=tk.LEFT, padx=20,font=20).grid(row=11, column=1)
v_BID_D = Dropdown_BID(master, ['BoardID'])
v_BID_D.grid(row=12, column=1)

# wedge location
tk.Label(master, text='Wedge Testing Location:', justify=tk.LEFT, padx=20,font=20).grid(row=11, column=2)
v_LOC_D = Dropdown_LOC(master, ['Location'])
v_LOC_D.grid(row=12, column=2)

tk.Button(master, text="Set Setup", command=set_setup, font=20).grid(row=12, column=3)

# shifters
tk.Label(master, text='Shifter1:', justify=tk.LEFT, padx=20, font=20).grid(row=13, column=0)
tk.Label(master, text='Shifter2:', justify=tk.LEFT, padx=20, font=20).grid(row=13, column=1)
tk.Label(master, text='Shifter3:', justify=tk.LEFT, padx=20, font=20).grid(row=13, column=2)

e_shifter1 = tk.Entry(master)
e_shifter1.grid(row=14, column=0)
e_shifter2 = tk.Entry(master)
e_shifter2.grid(row=14, column=1)
e_shifter3 = tk.Entry(master)
e_shifter3.grid(row=14, column=2)

tk.Message(master, text = "").grid(row=15)

# == FEB/Wedge Setup ==
tk.Label(master, text='2. FEB/Wedge Info', font=20, fg='blue').grid(row=20, column=1)

# FEB Info
tk.Label(master, text='*FEB serial*:', justify=tk.LEFT, font=20).grid(row=21, column=0)
tk.Label(master, text='FEB MTF:', justify=tk.LEFT, font=20).grid(row=22, column=0)

e_FEB_serial = tk.Entry(master,highlightbackground='red')
e_FEB_serial.grid(row=21, column=1)
e_FEB_MTF = tk.Entry(master)
e_FEB_MTF.grid(row=22, column=1)

# Chamber Info
fields = ('*Chamber Serial*', 'Large/Small', 'Side A/C', 'Pivot/Confirm', 'Serial Number')
ents = makeform(master, fields, line_count=23)  
b1 = tk.Button(master, text='Chamber Decode', command=(lambda e=ents: id_decode(e)),font=20)
b1.grid(row=23, column=2)

# Quadraplate Info
tk.Label(master, text='*Quadraplate Serial*:', justify=tk.LEFT, font=20).grid(row=28, column=0)
e_Wedge_LOC = tk.Entry(master,highlightbackground="red")
e_Wedge_LOC.grid(row=28, column=1)

tk.Button(master, text='Quadraplate Decode', command=show_Wedge_LOC,font=20).grid(row=28, column=2)

tk.Message(master, text = "").grid(row=29)

# == MINIDAQ Setup ==
tk.Label(master, text='3. MINIDAQ Initialize/Configuration', font=20, fg='blue').grid(row=30, column=1)

tk.Button(master, text='Initialize SCA', command=paraprocess_SCA,font=20).grid(row=31, column=0)
tk.Button(master, text='Read SCA ID', command=SCAID_insert,font=20).grid(row=31, column=1)
tk.Button(master, text='Initialize ROC (Wait 5 sec)', command=paraprocess_ROC,font=20).grid(row=31, column=2)

tk.Label(master, text="SCAID:",font=20).grid(row=32, column=0)

e3 = tk.Entry(master)
e3.grid(row=32, column=1)

# VMM Gain
tk.Label(master, text='VMM Gain (mV/fC):', justify=tk.LEFT, padx=20, font=20).grid(row=34, column=0)
var_GAIN_D = Dropdown_Gain(master, ['Gain'])
var_GAIN_D.grid(row=35, column=0)

# VMM PT
tk.Label(master, text='VMM PT (ns):', justify=tk.LEFT, padx=20, font=20).grid(row=34, column=1)
var_PT_D = Dropdown_PT(master, ['PT'])
var_PT_D.grid(row=35, column=1)

# VMM Polarity
tk.Label(master, text='VMM Polarity:', justify=tk.LEFT, padx=20, font=20).grid(row=34, column=2)
var_POLAR_D = Dropdown_Pol(master, ['Gain'])
var_POLAR_D.grid(row=35, column=2)

tk.Button(master, text='Set MINIDAQ', command=set_minidaq, font=20).grid(row=35, column=3)

# Cpi and Rp input
tk.Label(master, text='Cpi in pF (pFEB):', justify=tk.LEFT, font=20).grid(row=36, column=0)
tk.Label(master, text='Rp in kOhm (sFEB):', justify=tk.LEFT, font=20).grid(row=36, column=1)

e_Cpi = tk.Entry(master)
e_Cpi.grid(row=37, column=0)
e_Rp = tk.Entry(master)
e_Rp.grid(row=37, column=1)

# VMM to oscilloscope channel mapping
tk.Label(master, text='Scope to VVM Channel Mapping:\n(N: unused scope channel)\n(-1: unconnected scope channel)', justify=tk.LEFT, font=20).grid(row=38, column=0)
map1 = Dropdown_N(master, ['CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8'])
map1.grid(row=38, column=1)

tk.Button(master, text='Generate VMMID', command=allstates_map, font=20).grid(row=38, column=2, pady=4)

tk.Message(master, text = "").grid(row=39)

# == Scan Options ==
tk.Label(master, text='4. Run Scan', font=20, fg='blue').grid(row=40, column=1)

# Comment box
tk.Label(master, text='Comment:', justify=tk.LEFT, font=20).grid(row=41, column=0)

e_comment = tk.Entry(master)
e_comment.grid(row=41, column=1, sticky='ew')

# File handling
tk.Button(master, text='Generate RUNID', command=runID_gen, font=20).grid(row=42, column=0, pady=4)
tk.Button(master, text='Show file name', command=show_file_name, font=20).grid(row=42, column=1, pady=4) 

# Scan Noise
tk.Button(master, text='Scan Noise (4 Channels)', command=main_noise_measurement_4, font=20).grid(row=42, column=2)
tk.Button(master, text='Scan Noise (6 Channels)', command=main_noise_measurement_6, font=20).grid(row=42, column=3)



#tk.Button(master, text='Quit', command=master.quit, font=20).grid(row=45, column=4)

master.mainloop( )



