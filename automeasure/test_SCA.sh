#!/bin/bash

#       This program runs "main.py" and "recv_dgramsocket.py" at the same time and kill all process when main.py finished
#	Usage: bash test.sh
# History
# 2018/02/11	BTse	First release


{ python SCA_init.py; echo one; kill -2 0; } &
# "$!" Expands to the process ID of the most recently executed background (asynchronous) command
FOO=$!
{ python recv_dgramsocket.py; echo two; }

wait $FOO
sleep 2

#Run decoder for plotting/fitting and fitting SCA ADC values versus scope (~30 seconds)
#python DECODER.py

#Scan baseline, save to a new txt file

#Plot all channels baseline data in histogram(apply cut)

#Scan null trim thershold with a specific set thershold

#Calculate required trimmed values for each channel

#Put the trimmed values back to each channel and scan the thersold again

#Repeat the process until getting a good value

echo done

