#!/usr/bin/env python
# v2 for miniDAQ2

import socket
import binascii
from time import sleep
from target_info import *

class SendPacket():

	def __init__(self):
		self.host_ip="192.168.0.16"
		self.host_port=6007
		#self.host_port=7201
		self.dest_ip="192.168.0.1"
		self.dest_port=6008
		self.general_packet_header= binascii.a2b_hex('decafbad') 
		

	def send(self):

		cmd_hex = "00000400"

		cmd = binascii.a2b_hex(cmd_hex)

		print "~~~~~~",cmd
		reg_string = (#"000000AF00000000" #FPGA Global RESET (write 0xAA will reset once)
			     #+"000000AB00000001" #TRIGGER MODE (1:EXT. 0:INT.)
			     #+"0000000F00000000" #DAQ_ENA
			     #+"000000CD00000000" #eFIFO ENABLE (elink 31-0) CKBC_Phase (BD3-0) Least 7 bit valid for each board
			     "000000B058494365" #CKBC_Phase (BD3-0) Least 7 bit valid for each board
			     +"000000B148516649" #CKBC_Phase (BD7-4) Least 7 bit valid for each board
			     #+"000000E000000000" #TTC_STARTBIT (3bits, 0-7)
			     #+"000000E20000001F" #GATE_Trigger_nBC (in unit of BC, 16 bits, Max 0.2ms)
			     +"000000E400000000" #TTC-bit-SCA_RST
			     +"000000E500000000" #TTC-bit-ECR (Event Counter Reset)
			     +"000000E600000000" #TTC-bit-BCR (Bunch Counter Reset)
			     +"000000E700000000" #TTC-bit-Soft_RST (VMM/ROC Soft Reset)
			     +"000000E800000000" #TTC-bit-EC0R 
			     +"000000E900000000" #TTC-bit-L0A
			     +"000000EA00000000" #TTC-bit-L1A
			     +"000000EB00000001" #TTC-VMMTP
			     #+"000000EC00000000" #L0-Latency
			     #+"000000ED00000000" #L1-Latency
			     +"000000EE000000FF" #TTC bit pulse or level
			     #+"000000EF00000000" #TTC test pattern (OCR, VMMTP, L0, L1 sequence)
			     #+"000000C30000FFFF" #CKTP_NMAX (16 bits, 0xFFFF==infinite)
			     #+"000000C80000FFFF" #VMM TP SPACING (in unit of BC, 24 bits, minium value: 0xFFFF)
			     #+"000000D000000000" #Query Elink Status. Return 32 elink channel lock status 
			     #+"000000D100000005" #BD0 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D201060601" #BD1 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D301060601" #BD2 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D401060601" #BD3 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D501060601" #BD4 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D601060601" #BD5 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D701060601" #BD6 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D801060601" #BD7 Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000DA00000000" #Query All, return 32b EVID, 32b elink_lock, 32b eFIFO1_dcount, ...eFIFO31,eFIFO30 dcount,elink idly 0... 
			     #+"000000DE00000000" #Query Event ID
			     #+"000000DF00000000" #Query miniDAQ firmware version
			     )
	

		payload = binascii.a2b_hex(reg_string)	
	
		message = self.general_packet_header + cmd +  payload 
		mes_vec = [message]

		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sock.bind((self.host_ip, self.host_port))
		
		for i in range(1):
			print "My IP:     " + self.host_ip
			print "My Port:   " + str(self.host_port)
			print "Dest IP:   " + self.dest_ip
			print "Dest Port: " + str(self.dest_port)
			print "Payload:   " + binascii.b2a_hex(mes_vec[i])
			sock.sendto(mes_vec[i], (self.dest_ip, self.dest_port))
			sleep(0.2)


if __name__ == "__main__":
	sp=SendPacket()
	sp.send()

