import sys
from glob import glob
#from Baseline_process_funct import *
#from diff_funct import *
#from TEST_PDO_funct import *
import textwrap
import numpy as np
import matplotlib.pyplot as plt

'''
if len(sys.argv)!=2:
  print("Usage: python TEST.py targetDir")
  sys.exit(0)

filelist = glob( sys.argv[1] + "/*.txt")
'''
#fo = open("test.dat", "rw+")
#line_raw = fo.readlines()
line_raw = [line.rstrip('\n') for line in open('test.dat')]
#print "Read Line: %s" % (line_raw)

list_processed = []
for x in range(1,len(line_raw)):
	count = []
	#count.append(line_raw[x].split)
	count = textwrap.wrap(line_raw[x],8)
	#print count
	if len(count) > 20:
		del count[0:3]
		del count[-3:]
		list_processed.append(count)

#Convert all the hex value in list into dec

z = 0
length = 50
list_dec = [[] for _ in range(length)]

for z in range(0,length):
	for y in range(1,len(list_processed[z])):
		dec_value = int(list_processed[z][y],16)
		if dec_value <= 4096:
			list_dec[z].append(dec_value/4.096)
z = 15

print list_dec[z]

Data_Mean = np.mean(list_dec[z])
Data_Median = np.median(list_dec[z])
Data_Max = np.max(list_dec[z])
Data_Min = np.min(list_dec[z])
        
His_Range_Min = float(Data_Min - 5)
His_Range_Max = float(Data_Max + 5)

plt.hist(list_dec[z], bins=np.arange(His_Range_Min, His_Range_Max + 0.2, 0.2), range=(His_Range_Min,His_Range_Max), histtype='bar', rwidth=None, color=None, label=None)

plt.xlabel("ADC_Value")
plt.ylabel("Count")
#plt.title("Histogram of ADC value for channel %s of SCA %s VMM %s" % (channel_no, SCA_no, VMM_no))
plt.text(350, 3200, r'$\mu=100,\ \sigma=15$')
plt.grid(True)

plt.savefig("PDO_SCA%s_VMM%s_CH%s.png" % (1, 1, 1))
plt.gcf().clear()

'''
list_dec_N = Baseline_HIS_Plot(list_dec,length,"1030","D")

list_avg = []
z = 0
for z in range(0,length):
	list_avg.append(np.mean(list_dec_N[z]))
	z += 1

print list_avg
print len(list_avg)

PDO_DATA = Baseline_process('0_pFEB009_VMM3_xxx.dat',length)
print PDO_DATA

difference_osc_ADC(PDO_DATA,list_avg,length)
'''

