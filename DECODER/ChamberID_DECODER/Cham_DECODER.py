# This program is intended to decode the Chamber ID scanned from QR code
# e.g. 20MNIWSAP00001
# [0][1] "20" shows it is in ALTAS project
# [2]    "M" stands for "MUON"
# [3]    "N" stands for "NSW"
# [4]    "I" stands for 
# [5]    "W" stands for "Wedge"
# [6]    "S" stands for "Small" {Options: "S"/"L"}
# [7]    "A" stands for "Side A" {Options: "A"/"C"}
# [8]    "P" stands for "Pivot" {Options: "P"/"C"}
# [9]    "0" stands for
# [10][11][12][13] "0001" indicates the number/serial of wedge

import sys
import math

code = "20MNIWSAP00001"

code_list = list(code)

code_seg = code_list[0]+code_list[1]
code_serial = code_list[10]+code_list[11]+code_list[12]+code_list[13]
code_re_list = []
code_re_list.append(code)

if code_seg == "20":
	print "It is under ALTAS"
else:
	raise Exception("It is not in ALTAS, please check")

if code_list[2] == "M":
	print "It is ALTAS MUON"
else:
	raise Exception("It is not ALTAS MUON, please check")

if code_list[3] == "N":
	print "It is ALTAS MUON NSW"
else:
	raise Exception("It is not ALTAS MUON NSW, please check")

if code_list[5] == "W":
	print "Wedge"
else:
	raise Exception("It is not Wedge, please check")

if code_list[6] == "S":
	code_re_list.append("QS")
	print "Small Wedge"
elif code_list[6] == "L":
	code_re_list.append("QL")
	print "Large Wedge"
else:
	raise Exception("It is not Wedge, please check")

if code_list[7] == "A":
	code_re_list.append("Side A")
	print "Side A"
elif code_list[7] == "C":
	code_re_list.append("Side C")
	print "Side C"
else:
	raise Exception("It is not Side A or Side C, please check")

if code_list[8] == "P":
	code_re_list.append("Pivot")
	print "Pivot"
elif code_list[8] == "Confirm":
	code_re_list.append("Confirm")
	print "Confirm"
else:
	raise Exception("It is not Pivot or Confirm, please check")

code_re_list.append(code_serial)

print code_re_list








