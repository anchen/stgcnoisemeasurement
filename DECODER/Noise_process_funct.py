import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

'''
if len(sys.argv)!=2:
  print("Usage: python TEST.py targetDir")
  sys.exit(0)

filelist = glob( sys.argv[1] + "/*.txt")
'''
def Noise_plot(file_name):
	#ARRAY_Data = np.loadtxt('0_pFEB009_VMM3_xxx.dat',dtype='double')
	ARRAY_Data = np.loadtxt(file_name,dtype='double')
	Total_No = len(ARRAY_Data)
	print Total_No

	x=0
	y=0
	z=0
	length=64

	#Generate all lists for all 64 channels

	PDO_DATA_1 = []
	PDO_DATA_2 = []
	PDO_DATA_3 = []
	PDO_DATA_4 = []
	
	#sweep though all the list, group channel's data
	
	for y in range(0,64):
		PDO_DATA_1.append(ARRAY_Data[y][1])
		y += 1

	for y in range(0,64):
		PDO_DATA_2.append(ARRAY_Data[y][2])
		y += 1
	
	for y in range(0,64):
		PDO_DATA_3.append(ARRAY_Data[y][3])
		y += 1

	for y in range(0,64):
		PDO_DATA_4.append(ARRAY_Data[y][4])
		y += 1

	print PDO_DATA_1
	print PDO_DATA_2
	print PDO_DATA_3
	print PDO_DATA_4

	#Plot data on graphs

	counter = []

	for y in range(0,length):
		counter.append(y)
		y += 1

	plt.plot(counter, PDO_DATA_1,'rx',markersize = 2)
	plt.plot(counter, PDO_DATA_2,'bx',markersize = 2)
	plt.plot(counter, PDO_DATA_3,'gx',markersize = 2)
	plt.plot(counter, PDO_DATA_4,'mx',markersize = 2)
	plt.ylabel('Noise RMS[mV]')
	plt.xlabel('Channel')
	plt.show()

