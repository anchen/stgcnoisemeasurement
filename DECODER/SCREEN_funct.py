import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

def SCREEN_DATA(PDO_DATA,length,SCA_no,VMM_no):
	z=0
	PDO_RANGE_PROCESS = [[] for _ in range(length)]
	for z in range(0,length):
	
		channel_no = z

        	Data_Mean = np.mean(PDO_DATA[z])
        	Data_Median = np.median(PDO_DATA[z])
        	Data_Max = np.max(PDO_DATA[z])
        	Data_Min = np.min(PDO_DATA[z])

        	His_Range_Min = float(Data_Median - 5)
        	His_Range_Max = float(Data_Median + 5)
		
		x=0
		length_temp = len(PDO_DATA[z])
		
		for x in range(0,length_temp):
			if ((PDO_DATA[z][x] < His_Range_Max) and (PDO_DATA[z][x] > His_Range_Min)) == True:
				PDO_RANGE_PROCESS[z].append(PDO_DATA[z][x])
			x += 1
		z += 1 
		
		#Calculate mean for all channels

	return PDO_RANGE_PROCESS



