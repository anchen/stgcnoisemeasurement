import sys
from glob import glob
import textwrap
import numpy as np


def SCAID_DECODER():
	line_raw = [line.rstrip('\n') for line in open('test.dat')]
	
	list_processed = []
	for x in range(1,len(line_raw)):
		count = []
		#count.append(line_raw[x].split)
		count = textwrap.wrap(line_raw[x],8)
		#print count
		if len(count) > 5:
			del count[0:3]
			del count[-3:]
			i = int(count[0], 16)
			output_T = str(i)
	return output_T

