import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from glob import glob
from trendline import *


def difference_osc_ADC(base_osc,base_ADC,length):
	#base_osc = [232.0101, 215.5735, 248.6186, 218.1987, 229.2065, 228.5071, 227.9242, 219.8712, 215.9257, 239.8723, 232.4785, 220.2319, 222.8593, 236.049, 227.2936, 234.1973, 233.0645, 257.0833, 208.3594, 227.852, 224.7255, 234.3849, 235.6152, 226.5516, 249.2129, 228.9262, 232.5087, 221.1334, 233.6352, 236.1525, 243.7906, 241.1529, 226.8973, 223.8873, 252.3801, 228.1351, 217.4685, 250.7763, 238.4191, 212.4346, 227.3625, 247.656, 231.8154, 214.8816, 232.7874, 237.1369, 219.481, 232.923, 229.1724, 244.4041, 257.2896, 224.185, 238.6766, 208.7681, 237.0492, 244.6081, 222.1958, 214.2039, 218.2881, 225.5606, 220.1744, 223.2535, 233.2113, 236.8677]
	
	#base_ADC = [223.58296179646598, 207.39931987626903, 240.3954574742268, 210.67082331730768, 220.53994937818877, 220.06017874672776, 220.5593670685279, 211.54536033163265, 208.48505221419597, 231.72397806186868, 223.8364738503886, 212.1000141677461, 215.28043451997422, 228.11373197115384, 222.62925957914572, 225.28168184673368, 225.1014122596154, 248.84554608982413, 200.31926081730768, 221.54889787946428, 218.3807219692211, 226.40423798680902, 226.73265861742425, 225.01544563137753, 239.30320548052762, 220.88056741301546, 223.90589763208763, 212.34600360576923, 225.46756628787878, 227.30232007575756, 235.07552410489947, 232.59165807423855, 222.27102308417085, 216.361347754397, 244.30870171168343, 221.57342940414506, 210.18041745580808, 241.9421137595663, 229.3427136479592, 203.73351130653265, 220.58674708549222, 239.6264895044192, 223.36251395089283, 206.46324936224488, 224.54645215850516, 227.63277304292927, 210.9757240372475, 224.70250080541237, 220.62174479166666, 235.94657859610552, 250.5569841394472, 215.94114977904042, 230.67117815640702, 201.11632977843917, 229.25182224548968, 236.4066425879397, 213.8298190369898, 206.65698366116752, 210.32079060872397, 227.72308809673368, 211.96619669596353, 215.14511108398438, 225.02825586928935, 226.9454413864213]
	
	
	base_diff = []
	counter = []
	#length = 64
	
	#for Global DAC plotting
	
	for y in range(0,length):
	#for y in range(101,length):
		base_diff.append(base_osc[y] - base_ADC[y])
		#counter.append((y+1)*20)
		counter.append((y+101))
		y += 1
	print base_diff
	print counter
	plt.plot(counter, base_osc,'rx',markersize = 2, label='DAC_scope')
	plt.plot(counter, base_ADC,'bx',markersize = 2, label='DAC_ADC')
	osc_var = trendline(counter, base_osc)
	ADC_var = trendline(counter, base_ADC)
	slope_diff = (osc_var[0] - ADC_var[0])/osc_var[0] * 100
	offset = osc_var[1] - ADC_var[1]
	print("slope difference = " '%f' " percent" % (slope_diff))
	print("offset = " '%f' % (offset))
	plt.ylabel('Voltage[mV]')
	plt.xlabel('Global DAC setting')
	plt.title("Voltage Scan vs Global DAC")
	plt.legend()
	#plt.show()
	plt.grid(True)
	figure = plt.gcf() # get current figure
	figure.set_size_inches(16,9)

	plt.savefig("Global_DAC_SCA%s_VMM%s.png" % (1, 1),dpi = 100)
	plt.gcf().clear()
	
	#for baseline plotting
	'''
	for y in range(0,length):
		base_diff.append(base_osc[y] - base_ADC[y])
		counter.append(y)
		y += 1

	max_y = max(base_osc)
	max_y_modified = max(base_osc) - 3
	diff_osc = int(max(base_osc) - min(base_osc))
	diff_ADC = int(max(base_ADC) - min(base_ADC))
	diff_show = ('SCDIFF = %i \nADCDIFF = %i' % (diff_osc, diff_ADC))
	

	print base_diff
	print counter
	plt.plot(counter, base_osc,'rx',markersize = 2, label='base_scope')
	plt.plot(counter, base_ADC,'bx',markersize = 2, label='base_ADC')
	osc_var = trendline(counter, base_osc)
	ADC_var = trendline(counter, base_ADC)
	slope_diff = (osc_var[0] - ADC_var[0])/osc_var[0] * 100
	offset = osc_var[1] - ADC_var[1]
	print("slope difference = " '%f' % (slope_diff))
	print("offset = " '%f' % (offset))
	plt.ylabel('Baseline[mV]')
	plt.xlabel('Channel')
	plt.title("Baseline Scan")
	plt.text(52, max_y_modified, diff_show)
	plt.legend()
	plt.show()
	'''
