import sys
from glob import glob
from Baseline_process_funct import *
from diff_funct import *
from SCREEN_funct import *
import textwrap
import numpy as np

'''
if len(sys.argv)!=2:
  print("Usage: python TEST.py targetDir")
  sys.exit(0)

filelist = glob( sys.argv[1] + "/*.txt")
'''
#fo = open("test.dat", "rw+")
#line_raw = fo.readlines()
line_raw = [line.rstrip('\n') for line in open('test.dat')]
#print "Read Line: %s" % (line_raw)

list_processed = []
for x in range(1,len(line_raw)):
	count = []
	#count.append(line_raw[x].split)
	count = textwrap.wrap(line_raw[x],8)
	#print count
	if len(count) > 20:
		del count[0:3]
		del count[-3:]
		list_processed.append(count)

#Convert all the hex value in list into dec

z = 0
length = 64
list_dec = [[] for _ in range(length)]

for z in range(0,length):
	for y in range(1,len(list_processed[z])):
		dec_value = int(list_processed[z][y],16)
		if dec_value <= 4096:
			list_dec[z].append(dec_value/4.096)

list_dec_N = SCREEN_DATA(list_dec,length,"1030","D")

list_avg = []
z = 0
for z in range(0,length):
	list_avg.append(np.mean(list_dec_N[z]))
	z += 1

print list_avg
print len(list_avg)

Data_Min = np.min(list_avg)
print Data_Min

DIFF = []
for i in range (64):
	difference_temp = list_avg[i] - Data_Min
	DIFF.append(int(round(difference_temp)))

print DIFF

'''
PDO_DATA = Baseline_process('0_pFEB009_VMM3_xxx.dat',length)
print PDO_DATA

difference_osc_ADC(PDO_DATA,list_avg,length)
'''

