from Tkinter import *
class Checkbar(Frame):
    def __init__(self, parent=None, picks=[], side=LEFT, anchor=W):
       Frame.__init__(self, parent)
       self.vars = []
       for pick in picks:
          var = IntVar()
          chk = Checkbutton(self, text=pick, variable=var)
          chk.pack(side=side, anchor=anchor, expand=YES)
          self.vars.append(var)
    def state(self):
       return map((lambda var: var.get()), self.vars)
#if __name__ == '__main__':
root = Tk()
lng = Checkbar(root, ['VMM0', 'VMM1', 'VMM2', 'VMM3', 'VMM4', 'VMM5', 'VMM6', 'VMM7'])
lng.pack(side=TOP,  fill=X)
lng.config(relief=GROOVE, bd=2)

def allstates(): 
    list_lng = list(lng.state())
    print(list_lng)
    list_lng_N = []
    if list_lng[0] == 1:
        list_lng_N.append("0")
    if list_lng[1] == 1:
        list_lng_N.append("1")
    if list_lng[2] == 1:
        list_lng_N.append("2")
    if list_lng[3] == 1:
        list_lng_N.append("3")
    if list_lng[4] == 1:
        list_lng_N.append("4")
    if list_lng[5] == 1:
        list_lng_N.append("5")
    if list_lng[6] == 1:
        list_lng_N.append("6")
    if list_lng[7] == 1:
        list_lng_N.append("7")
    print list_lng_N
    print len(list_lng_N)
    if len(list_lng_N) == 1:
        string_in_string = "{}".format(list_lng_N[0])
    if len(list_lng_N) == 2:
        string_in_string = "{}_{}".format(list_lng_N[0], list_lng_N[1])
    if len(list_lng_N) == 3:
        string_in_string = "{}_{}_{}".format(list_lng_N[0], list_lng_N[1], list_lng_N[2])
    if len(list_lng_N) == 4:
        string_in_string = "{}_{}_{}_{}".format(list_lng_N[0], list_lng_N[1], list_lng_N[2], list_lng_N[3])
    if len(list_lng_N) == 5:
        string_in_string = "{}_{}_{}_{}_{}".format(list_lng_N[0], list_lng_N[1], list_lng_N[2], list_lng_N[3], list_lng_N[4])
    if len(list_lng_N) == 6:
        string_in_string = "{}_{}_{}_{}_{}_{}".format(list_lng_N[0], list_lng_N[1], list_lng_N[2], list_lng_N[3], list_lng_N[4], list_lng_N[5])
    if len(list_lng_N) == 0:
        string_in_string = "No VMM selected"
        print ("Please select at least one VMM")
    if len(list_lng_N) >= 7:
        string_in_string = "Too many VMM selected"
        print ("Too many VMM selected, oscilliscope does not not have that much channel")
    print(string_in_string)

Button(root, text='Quit', command=root.quit).pack(side=RIGHT)
Button(root, text='Peek', command=allstates).pack(side=RIGHT)
root.mainloop()