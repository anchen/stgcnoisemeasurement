#!/usr/bin/env python

import socket
import binascii
from time import sleep
from daq_dict import *
from target_info import *

class SendPacket_Ana():

	def __init__(self):
		self.host_ip="192.168.0.16"
		self.host_port=7201
		self.general_packet_header= binascii.a2b_hex('decafbad') 

	def send(self,chip,cmd_string,target):
		# First Cfg Word [30:29]= board type [28:26]=board id [25:24]=asic type [23:16]=VMMID Bit Mask [15:0] CMD
		idWord_bin =    (bin(target(chip).board_type)[2:].zfill(3))+ \
				(bin(target(chip).board_id)[2:].zfill(3))+ \
				(bin(target(chip).asic_type)[2:].zfill(2))+ \
				(bin(target(chip).asic_id)[2:].zfill(8))

		idWord_hex = hex(int(idWord_bin,2))[2:].zfill(4)
		cmd_hex = hex(cmd_dict[cmd_string])[2:].zfill(4)

		command_word_32b = binascii.a2b_hex(idWord_hex+cmd_hex)

		print "~~~~~~",binascii.b2a_hex(command_word_32b)
		payload =binascii.a2b_hex(target(chip).cmd_content)
		
		message = self.general_packet_header + command_word_32b+payload
                
                roc_analog_head='decafbad0e010200'  #pFEB v2.3 J3

                #roc reg 0-63, 8 bit per register
                reg0='00'
                reg1='00'
                reg2='00'
                reg3='00'
                reg4='00'
                reg5='00'
                reg6='99'
                reg7='88'
                reg8='ff'
                reg9='00'
                reg10='00'
                reg11='00'
                reg12='00'
                reg13='00'
                reg14='00'
                reg15='f4'
                reg16='00'
                reg17='00'
                reg18='00'
                reg19='00'
                reg20='00'
                reg21='00'
                reg22='99'
                reg23='88'
                reg24='ff'
                reg25='00'
                reg26='00'
                reg27='00'
                reg28='00'
                reg29='00'
                reg30='00'
                reg31='f4'
                reg32='00'
                reg33='00'
                reg34='00'
                reg35='00'
                reg36='00'
                reg37='00'
                reg38='99'
                reg39='88'
                reg40='ff'
                reg41='00'
                reg42='f4'
                reg43='00'
                reg44='00'
                reg45='00'
                reg46='00'
                reg47='f4'
                reg48='99'
                reg49='88'
                reg50='ff'
                reg51='00'
                reg52='00'
                reg53='00'
                reg54='00'
                reg55='80'
                reg56='00'
                reg57='00'
                reg58='00'
                reg59='00'
                reg60='00'
                reg61='FF'
                reg62='FF'
                reg63='FF'
                
                rocanaconfigbit = ''.join([roc_analog_head,reg0,reg1,reg2,reg3,reg4,reg5,reg6,reg7,reg8,reg9,reg10, \
					reg11,reg12,reg13,reg14,reg15,reg16,reg17,reg18,reg19,reg20,reg21,reg22,reg23,reg24,reg25,reg26,reg27,reg28,reg29,\
					reg30,reg31,reg32,reg33,reg34,reg35,reg36,reg37,reg38,reg39,reg40,reg41,reg42,\
					reg43,reg44,reg45,reg46,reg47,reg48,reg49,reg50,reg51,reg52,reg53,reg54,reg55,\
					reg56,reg57,reg58,reg59,reg60,reg61,reg62,reg63])
                
  
                mes_vec = [ binascii.a2b_hex(rocanaconfigbit)]
		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sock.bind((self.host_ip, self.host_port))

		
		for i in range(1):
			#print "My IP:     " + self.host_ip
			#print "My Port:   " + str(self.host_port)
			#print "Dest IP:   " + target(chip).guest_ip
			#print "Dest Port: " + str(target(chip).guest_port)
			#print "Dest Port: " + str(target(chip).guest_port)
			print "======================================"
			print "Payload:   " + binascii.b2a_hex(mes_vec[i])
			sock.sendto(mes_vec[i], (target(chip).guest_ip, target(chip).guest_port))
			#sleep(0.2)


if __name__ == "__main__":
	sp=SendPacket_Ana()
	#sp.send('','InitSCA',target)
	#sp.send('vmm','CfgASIC',target)
	#sp.send('tds','CfgASIC',target)
	sp.send('roc','CfgASIC',target)
	#sp.send('vmm','RstASIC',target)
	#sp.send('tds','RstASIC',target)



