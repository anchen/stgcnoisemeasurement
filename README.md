# sTGC Noise Measurement Python GUI

18 SEP 2019 Created by Bobby Tse.

## Instruction for Noise Measurement Program ##

### Automatic noise measurement data taking ###
For initializing the program, exec ``sudo python2.7 noise_meas_main.py``. A GUI should pop up.
Details as follows:
1. Choose oscilloscope type.
2. Choose MINIDAQ MINISAS adaptor output (J1-J8).
3. Use Barcode reader to scan FEB serial/Wedge serial/Wedge Location.
4. Press ``cham_decode`` & ``Wedge LOC decode`` button once.
5. Press ``INIT SCA`` button once, then ``READ SCA ID`` button once. SCAID will show up. SCAIS will not show up if there is a problem in the connection between FEB and MINIDAQ.
6. Press ``INIT ROC (Wait for 5 seconds)`` button once for ROC configuration.
7. Choose VMM Gain and PT.
8. Choose VMM Polarity. (Function not installed yet, you need to change 'sp' bit in "vmm.xml" manually)
9. Choose the connection between scope to VMM. (Normally for pFEB:VMM1,2, sFEB:VMM0,1,2,3/4,5,6,7/2,3,4,5,6,7)
10. Press ``Generate RUNID`` button once, it will scan all .dat file in the directory and assign a new runid. Please ensure there is only noise measurement output file in the directory.
11. Press ``Show file name`` button once, the file for next measurement will be shown. If error occurs, there should be something missing in the previous steps.
12. Press ``Scan Noise`` depending on how many channels you have been connected to the oscilloscope.
13. The data file will be save in the directory as the same location as ``noise_meas_main.py`` in.

### Automatic noise measurement plotting ###
For plotting of the raw data, exec ``sudo python2.7 noise_meas_plot.py``. A GUI should pop up.
Details as follows:
1. Press ``Open files`` button once, a file browsing GUI will pop up.
2. Choose the raw data files you want to check with. You can choose multiple files, however pFEB and sFEB run should not be mixed. If you want to plot sFEB data, you need to choose 2 files (for 4 channels per run) in a group.
3. For pFEB data files, you need to click on ``Combine and plot data for pFEB``. Result graph(s) will be generated.
4. For sFEB data files, you need to click on ``Combine and plot data for sFEB``. Result graph(s) will be generated.

### Notes ### 
For the y dimension of the graph, you can change it in ``Noise_analysis_plot_funct_mass.py``. Look for line showing ``plt.ylim(*,*)``, the stars are the lower bound and the upper bound of the y-axis of the graph.


